# Project Implementation-phase

Name: Jakub Pronobis
Student Number: R00110611
Class: DNET4

## How to run
NPM version: 3.10.10
NODE version: v7.3.0

Steps
Create DB named dietbook and use script on it in Server/DatabaseCreation folder.
Inside Client folder run npm install and then npm start
Inside server folder run npm install and then npm start
