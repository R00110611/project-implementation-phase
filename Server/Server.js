var express = require('express');
var	app = express();
var	bodyParser = require('body-parser');
var util = require('util');
var multer = require('multer');
var uploadUserImage = multer({ dest: './UserImages'});
var uploadIngredientImage = multer({ dest: './IngredientImages'});
var uploadRecipeeImage= multer({ dest: './RecipeeImages'});
var DatabaseQuery = require('./DatabaseConnection/DatabaseQuery');

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Accept');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

app.use(bodyParser.json());


app.post('/register',function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	var slashes = require('slashes');
	var doesUserExists = 'SELECT EXISTS(SELECT * FROM user WHERE username = \'' + slashes.add(req.body.username) + '\') AS Solution;';

	DatabaseQuery(doesUserExists, function(err, rows, fields) {
  		if(rows[0].Solution == 0) {
  			var post  = {username: slashes.add(req.body.username), emailAddress: slashes.add(req.body.emailAddress), password: slashes.add(req.body.password)};
  			var query = DatabaseQuery('INSERT INTO user SET ?', post, function(err, result) { console.log(err) });


  			res.send(JSON.stringify({ registered: "true" }));
  		} else {
  			res.send(JSON.stringify({ registered: "false" }));
  		}
	});
});


app.post('/login',function(req, res) {
	res.setHeader('Content-Type', 'application/json');
var slashes = require('slashes');

	var AuthenticateUser = 'SELECT EXISTS(SELECT * FROM user WHERE username = \'' + slashes.add(req.body.username) + "\' AND BINARY password ='" + slashes.add(req.body.password) + "') AS AuthenticateUser;";
	DatabaseQuery(AuthenticateUser, function(err, rows, fields) {
		rows[0].AuthenticateUser == 1 ? res.send(JSON.stringify({ AuthenticateUser: true })) : null;

		if(rows[0].AuthenticateUser == 0) {
			var UserExistsQuery = 'SELECT EXISTS(SELECT * FROM user WHERE username = \'' + slashes.add(req.body.username) + "\') AS UserExists;";
			DatabaseQuery(UserExistsQuery, function(err, rows, fields) {
				rows[0].UserExists == 1 ? res.send(JSON.stringify({ AuthenticateUser: false, UserExists: true })) : res.send(JSON.stringify({ AuthenticateUser: false, UserExists: false }));
			})
		}
	});
});


app.post('/retrievePassword',function(req, res) {
	res.setHeader('Content-Type', 'application/json');
var slashes = require('slashes');


	if(req.body.retrieveMethod == "Email") {
		var doesUserExists = 'SELECT EXISTS(SELECT * FROM user WHERE username = \'' + slashes.add(req.body.username) + '\') AS Solution;';



		DatabaseQuery(doesUserExists, function(err, rows, fields) {
	  		if(rows[0].Solution == 1) {

					var getPassword = 'SELECT password AS Solution, emailAddress AS email FROM user WHERE username = \'' + slashes.add(req.body.username) + '\'';


					DatabaseQuery(getPassword, function(err, rows, fields) {

						var email   = require("emailjs/email.js");
						var server  = email.server.connect({
						   user:     "jakub.pronobis@outlook.com",
						   password: "9bZkp7q19f0",
						   host:     "smtp-mail.outlook.com",
						   tls: {ciphers: "SSLv3"}
						});

						var message = {
						   text:    "Password",
						   from:    "you <jakub.pronobis@outlook.com>",
						   to:      rows[0].email,
						   subject: "Your password ",
						   attachment:
						   [
						      {data:"<html> Your password is: <b> " + rows[0].Solution + " </b> </html>", alternative:true}
						   ]
						};

						server.send(message, function(err, message) { console.log(err || message); });


						res.send(JSON.stringify({ success: "true" }));
					});
	  		} else {
	  			res.send(JSON.stringify({ success: "false" }));
	  		}


		});

	}

	if(req.body.retrieveMethod == "Telephone") {
		var doesUserExists = 'SELECT EXISTS(SELECT * FROM user WHERE username = \'' + slashes.add(req.body.username) + '\') AS Solution;';

		DatabaseQuery(doesUserExists, function(err, rows, fields) {
	  		if(rows[0].Solution == 1) {

					var getPassword = 'SELECT password AS Solution, emailAddress AS email FROM user WHERE username = \'' + slashes.add(req.body.username) + '\'';

					DatabaseQuery(getPassword, function(err, rows, fields) {

						var getTelephoneNumber = 'SELECT telephoneNumber AS Solution FROM user WHERE username = \'' + slashes.add(req.body.username) + '\'';
						var password = rows[0].Solution;
						DatabaseQuery(getTelephoneNumber, function(err, rows, fields) {

							if(rows[0].Solution != null && rows[0].Solution != undefined && rows[0].Solution != "") {
							var twilio = require('twilio');
							var client = twilio('ACea28024f87183a4185ef5c09e5d2febb', 'afa38ce293b1fd7758875b088d038694');


							client.sendMessage({
								to: rows[0].Solution,
								from: '353861800335',
								body: 'Your password is: ' + password
						});


			res.send(JSON.stringify({ success: "telephoneTrue" }));
					} else {
						res.send(JSON.stringify({ success: "telephoneNoExist" }));
					}



						});

					})
		} else {
			res.send(JSON.stringify({ success: "telephoneFalse" }));
		}
	});
	}
});




app.post('/getUserProperties', function(req, res, next) {
		var slashes = require('slashes');
	var username = slashes.add(req.body.username);
	var queryString = "select * FROM user WHERE username ='" + slashes.add(req.body.username) + "'";

	DatabaseQuery(queryString, function(err, rows, fields) {
		if (err) throw err;

		var CurrentUsers = {};

		for (var i = 0; i < rows.length; i++) {
			CurrentUsers = {
				Username: rows[i].username,
				Height: rows[i].height,
				Weight: rows[i].weight,
				emailAddress: rows[i].emailAddress,
				Photo: rows[i].Photo,
				RegistrationDate: rows[i].RegistrationDate,
				FirstName: rows[i].FirstName,
				LastName: rows[i].LastName,
				DateOfBirth: rows[i].DateOfBirth,
				Gender: rows[i].Gender,
				TelephoneNumber: rows[i].TelephoneNumber
			}
		}



		var UserProfileProperties = {
			Ingredients: {
				CreatedIngredients: [],
				FavouriteIngredients: []
			}
		}

		if(rows[0].username === "testing1") {
			UserProfileProperties.Ingredients.CreatedIngredients.push({test: "testing1"});
		}

		if(rows[0].username === "testing2") {
			UserProfileProperties.Ingredients.CreatedIngredients.push({test: "testing2"});
		}

		res.send(JSON.stringify({
			response: CurrentUsers || null,
			returnValue: UserProfileProperties
		}));
});

});

app.post('/updateUserProperties',function(req, res) {
	res.setHeader('Content-Type', 'application/json');
	var slashes = require('slashes');

	var username = slashes.add(req.body.username);
	var height = req.body.height;
	var weight = req.body.weight;
	var DateOfBirth = req.body.DateOfBirth;
	var FirstName = slashes.add(req.body.FirstName);
	var LastName = slashes.add(req.body.LastName);
	var Gender = req.body.Gender;
	var TelephoneNumber = req.body.TelephoneNumber;

	if(TelephoneNumber === "" || TelephoneNumber === null || TelephoneNumber === undefined) {
		TelephoneNumber = null;
	} else {
		TelephoneNumber = '\'' + TelephoneNumber + '\'';
	}

	if(Gender === "" || Gender === null || Gender === undefined) {
		Gender = null;
	} else {
		Gender = '\'' + Gender + '\'';
	}

	if(DateOfBirth === "" || DateOfBirth === null || DateOfBirth === undefined) {
		DateOfBirth = null;
	} else {
		DateOfBirth = '\'' + DateOfBirth.substring(0, 10) + '\'';
	}

	if(FirstName === "" || FirstName === null || FirstName === undefined) {
		FirstName = null;
	} else {
		FirstName = '\'' + FirstName + '\'';
	}

	if(LastName === "" || LastName === null || LastName === undefined) {
		LastName = null;
	} else {
		LastName = '\'' + LastName + '\'';
	}

	if(height === "" || height === null || height === undefined) {
		height = null;
	} else {
		height = '\'' + height + '\'';
	}

	if(weight === "" || weight === null || weight === undefined) {
		weight = null;
	} else {
		weight = '\'' + weight + '\'';
	}


	var updateUserProperties = "UPDATE user SET FirstName=" + FirstName + ", LastName=" + LastName + ", height=" + height + ", weight=" + weight + ", DateOfBirth=" + DateOfBirth + ", Gender=" + Gender + ", TelephoneNumber=" + TelephoneNumber + " WHERE username='" + username + "'";

	DatabaseQuery(updateUserProperties, function(err, rows, fields) {
		res.send(JSON.stringify({ response: "true" }));
	});

});


app.post('/SearchForUser', function(req, res, next) {
	var slashes = require('slashes');
	var FirstName = slashes.add(req.body.FirstName);
	var LastName = slashes.add(req.body.LastName);

	var queryString = "select * from user where FirstName like '%" + FirstName + "%' and LastName like '%" + LastName + "%'";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		var Users = [];

		for (var i = 0; i < rows.length; i++) {
			var User = {
				Username: rows[i].username,
				Photo: rows[i].Photo,
				FirstName: rows[i].FirstName,
				MiddleName: rows[i].MiddleName,
				LastName: rows[i].LastName
			}
			Users.push(User);
		}

		res.send(JSON.stringify({
			response: Users || null,
		}));
	});
});

app.get('/UserImages/:id', function(req, res) {
  res.sendFile(__dirname + "/UserImages/" + req.params.id);
});

app.get('/RecipeeIngredientImages/:id', function(req, res) {
  res.sendFile(__dirname + "/RecipeeIngredientImages/" + req.params.id);
});



app.post('/getLastVisitors', function(req, res, next) {
	var ProfileToVisit = req.body.username;
	var LoggedInUser = req.body.LoggedInUser;

	var queryString = "select userID FROM user WHERE username ='" + req.body.username + "'";

	DatabaseQuery(queryString, function(err, rows, fields) {
			if (err) throw err;

			var userID = rows[0].userID;

			var queryString2 = "select userID FROM user WHERE username ='" + LoggedInUser + "'";

			var LoggedInUserID = 0;

			DatabaseQuery(queryString2, function(err, rows, fields) {
					if (err) throw err;
					LoggedInUserID = rows[0].userID;

					var insertQuery = "INSERT into uservisitors (UsernameID, VisitorID) VALUES (" + userID + ", " + LoggedInUserID + ")";

					if(userID != LoggedInUserID) {
						DatabaseQuery(insertQuery, function(err, rows, fields) {


							var query2 = "select u.username, u.photo, uv.TimeAndDate from user u, uservisitors uv WHERE uv.UsernameID = " + userID + " AND u.userID = uv.VisitorID ORDER BY TimeAndDate desc";
							DatabaseQuery(query2, function(err, rows, fields) {
									if (err) throw err;

										var ArrayOfVisitors = [];

										for (var i = 0; i < rows.length; i++) {
												var Visitor = {
													Username: rows[i].username,
													Timestamp: rows[i].TimeAndDate,
													Photo: rows[i].photo
												}
												ArrayOfVisitors.push(Visitor);

										}

										res.send(JSON.stringify({
											ArrayOfVisitors: ArrayOfVisitors || null,
										}));
									});

						});



					} else {
						var query2 = "select u.username, uv.TimeAndDate from user u, uservisitors uv WHERE uv.UsernameID = " + userID + " AND u.userID = uv.VisitorID ORDER BY TimeAndDate desc";
						DatabaseQuery(query2, function(err, rows, fields) {
								if (err) throw err;

									var ArrayOfVisitors = [];

									for (var i = 0; i < rows.length; i++) {
											var Visitor = {
												Username: rows[i].username,
												Timestamp: rows[i].TimeAndDate
											}
											ArrayOfVisitors.push(Visitor);

									}

									res.send(JSON.stringify({
										ArrayOfVisitors: ArrayOfVisitors || null,
									}));
								});
					}
			});

	});
});

app.post('/updateUserPhoto', uploadUserImage.single('photo'), function(req, res, next) {
	res.setHeader('Content-Type', 'application/json');

	var username = req.body.username;
	var photoPath = "UserImages/" + req.file.filename;

	var queryString = "UPDATE user SET Photo ='" + photoPath + "' WHERE username='" + username + "'";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		res.send(JSON.stringify({
			response: req.file.path || null,
		}));
});
});




app.post('/updateUserPhoto2', function(req, res, next) {
	var username = req.body.username;

	var queryString = "UPDATE user SET Photo = null WHERE username='" + username + "'";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		res.send(JSON.stringify({
			response: "Done" || null,
		}));

});
});


app.post('/CreateIngredient', uploadIngredientImage.single('photo'), function(req, res, next) {
		var jsonState = JSON.parse(req.body.obj);

		var post = { Name: jsonState.IngredientName,
			Username: jsonState.username,
			Description: jsonState.IngredientDescription,
			Language: jsonState.IngredientLanguage,
			Photo: req.file != undefined ? req.file.path : null,
			Calories: jsonState.Calories === null || jsonState.Calories === undefined || jsonState.Calories === ""? 0 : jsonState.Calories,
			Amount: jsonState.IngredientAmount != "" ? jsonState.IngredientAmount : 0,
			VitaminA: jsonState.VitaminA != "" ? jsonState.VitaminA : 0,
			VitaminB1: jsonState.VitaminB1 != "" ? jsonState.VitaminB1 : 0,
			VitaminB2: jsonState.VitaminB2 != "" ? jsonState.VitaminB2 : 0,
			VitaminB3: jsonState.VitaminB3 != "" ? jsonState.VitaminB3 : 0,
			VitaminB5: jsonState.VitaminB5 != "" ? jsonState.VitaminB5 : 0,
			VitaminB6: jsonState.VitaminB6 != "" ? jsonState.VitaminB6 : 0,
			VitaminB9: jsonState.VitaminB9 != "" ? jsonState.VitaminB9 : 0,
			VitaminB12: jsonState.VitaminB12 != "" ? jsonState.VitaminB12 : 0,
			VitaminC: jsonState.VitaminC != "" ? jsonState.VitaminC : 0,
			VitaminD: jsonState.VitaminD != "" ? jsonState.VitaminD : 0,
			VitaminE: jsonState.VitaminE != "" ? jsonState.VitaminE : 0,
			VitaminK: jsonState.VitaminK != "" ? jsonState.VitaminK : 0,
			Calcium: jsonState.Calcium != "" ? jsonState.Calcium : 0,
			Chromium: jsonState.Chromium != "" ? jsonState.Chromium : 0,
			Copper: jsonState.Copper != "" ? jsonState.Copper : 0,
			Folate: jsonState.Folate != "" ? jsonState.Folate : 0,
			Iron: jsonState.Iron != "" ? jsonState.Iron : 0,
			Magnesium: jsonState.Magnesium != "" ? jsonState.Magnesium : 0,
			Manganese: jsonState.Manganese != "" ? jsonState.Manganese : 0,
			Phosphorus: jsonState.Phosphorus != "" ? jsonState.Phosphorus : 0,
			Potassium: jsonState.Potassium != "" ? jsonState.Potassium : 0,
			Sodium: jsonState.Sodium != "" ? jsonState.Sodium : 0,
			Selenium: jsonState.Selenium != "" ? jsonState.Selenium : 0,
			Zinc: jsonState.Zinc != "" ? jsonState.Zinc : 0,
			MonounsaturatedFat: jsonState.MonounsaturatedFat != "" ? jsonState.MonounsaturatedFat : 0,
			PolyunsaturatedFat: jsonState.PolyunsaturatedFat != "" ? jsonState.PolyunsaturatedFat : 0,
			SaturatedFat: jsonState.SaturatedFat != "" ? jsonState.SaturatedFat : 0,
			TransFat: jsonState.TransFat != "" ? jsonState.TransFat : 0,
			OtherFats: jsonState.OtherFats != "" ? jsonState.OtherFats : 0,
			Fiber: jsonState.Fiber != "" ? jsonState.Fiber : 0,
			Sugar: jsonState.Sugar != "" ? jsonState.Sugar : 0,
			OtherCarbohydrates: jsonState.OtherCarbohydrates != "" ? jsonState.OtherCarbohydrates : 0,
			Cholesterol: jsonState.Cholesterol != "" ? jsonState.Cholesterol : 0,
			Protein: jsonState.Protein != "" ? jsonState.Protein : 0 };

		DatabaseQuery('INSERT INTO ingredient SET ?', post, function(err, result) {
			console.log(err);
		});

		res.send(JSON.stringify({
			response: "IngredientCreated" || null,
		}));

});




app.get('/IngredientImages/:id', function(req, res) {
  res.sendFile(__dirname + "/IngredientImages/" + req.params.id);
});

app.get('/RecipeeImages/:id', function(req, res) {
  res.sendFile(__dirname + "/RecipeeImages/" + req.params.id);
});

app.post('/SearchForIngredient', function(req,res ) {
	var queryString = 'SELECT * FROM ingredient WHERE Name LIKE "%' + req.body.name + '%"';
	var myIngredients = req.body.myIngredients;

	if(myIngredients === true) {
		queryString = queryString + " AND Username=\"" + req.body.username + "\"";
	}

	if(req.body.IngredientLangauge != 'All') {
		queryString = queryString + " AND Language=\"" + req.body.IngredientLangauge + "\"";
	}

	if(req.body.onlyFavouriteIngredients === true) {
		var queryString1 = "SELECT i.* " + "FROM ingredient i, favouriteingredients fi" + " WHERE i.Name LIKE \"%" + req.body.name + "%\" ";



	if(myIngredients === true) {
		queryString1 = queryString1 + "AND i.Username = '" + req.body.username + "'"
	}

		queryString1 = queryString1 + " AND i.ID = fi.IngredientID AND fi.username = '" + req.body.username + "'";

		if(req.body.IngredientLangauge != 'All') {
			queryString1 = queryString1 + " AND Language=\"" + req.body.IngredientLangauge + "\"";
		}

		queryString = queryString1;

	}


	var arrayOfIngredients = [];

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

    for (var i in rows) {
				var Ingredient = {
					ID: rows[i].ID,
					Name: rows[i].Name,
					Username: rows[i].Username,
					Language: rows[i].Language,
					Description: rows[i].Description,
					Photo: rows[i].Photo,
					Calories: rows[i].Calories,
					Amount: rows[i].Amount,
					VitaminA: rows[i].VitaminA,
					VitaminB1: rows[i].VitaminB1,
					VitaminB2: rows[i].VitaminB2,
					VitaminB3: rows[i].VitaminB3,
					VitaminB5: rows[i].VitaminB5,
					VitaminB6: rows[i].VitaminB6,
					VitaminB9: rows[i].VitaminB9,
					VitaminB12: rows[i].VitaminB12,
					VitaminC: rows[i].VitaminC,
					VitaminD: rows[i].VitaminD,
					VitaminE: rows[i].VitaminE,
					VitaminK: rows[i].VitaminK,
					Calcium: rows[i].Calcium,
					Chromium: rows[i].Chromium,
					Copper: rows[i].Copper,
					Folate: rows[i].Folate,
					Iron: rows[i].Iron,
					Magnesium: rows[i].Magnesium,
					Manganese: rows[i].Manganese,
					Phosphorus: rows[i].Phosphorus,
					Potassium: rows[i].Potassium,
					Sodium: rows[i].Sodium,
					Selenium: rows[i].Selenium,
					Zinc: rows[i].Zinc,
					MonounsaturatedFat: rows[i].MonounsaturatedFat,
					PolyunsaturatedFat: rows[i].PolyunsaturatedFat,
					SaturatedFat: rows[i].SaturatedFat,
					TransFat: rows[i].TransFat,
					OtherFats: rows[i].OtherFats,
					Fiber: rows[i].Fiber,
					Sugar: rows[i].Sugar,
					OtherCarbohydrates: rows[i].OtherCarbohydrates,
					Cholesterol: rows[i].Cholesterol,
					Protein: rows[i].Protein
				};
				arrayOfIngredients.push(Ingredient);
    }



	res.send(JSON.stringify({
		response: arrayOfIngredients || null
	}));
});

});


app.post('/IngredientReviews',function(req, res) {
	var IngredientID = req.body.IngredientID;

	var queryString = 'SELECT * FROM ingredientreview WHERE IngredientID =' + IngredientID + " ORDER BY TimeAndDate DESC";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		var reviews = [];

    for (var i in rows) {
			var review = {
				ID: rows[i].ID,
				IngredientID: rows[i].IngredientID,
				Username: rows[i].Username,
				UsernameID: rows[i].UsernameID,
				Review: rows[i].Review,
				Recommended: rows[i].Recommended,
				TimeAndDate: rows[i].TimeAndDate
			}
			reviews.push(review);
    }

		res.send(JSON.stringify({
			response: reviews || null,
		}));

});
});

app.post('/RecipeeReviews',function(req, res) {
	var RecipeeID = req.body.RecipeeID;
	var queryString = 'SELECT * FROM recipeereview WHERE RecipeeID =' + RecipeeID + " ORDER BY TimeAndDate DESC";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		var reviews = [];

    for (var i in rows) {
			var review = {
				ID: rows[i].ID,
				RecipeeID: rows[i].RecipeeID,
				Username: rows[i].Username,
				UsernameID: rows[i].UsernameID,
				Review: rows[i].Review,
				Recommended: rows[i].Recommended,
				TimeAndDate: rows[i].TimeAndDate
			}
			reviews.push(review);
    }

		res.send(JSON.stringify({
			response: reviews || null,
		}));

});
});

app.post('/AddIngredientReview',function(req, res) {
	var IngredientIDs = req.body.IngredientID
	var Usernames = req.body.Username
	var Reviews = req.body.Review
	var Recommendeds = req.body.Recommended

	var queryString = 'SELECT * from ingredientreview';
	var post = { Username: Usernames, Review: Reviews, Recommended: Recommendeds, IngredientID: IngredientIDs };

	DatabaseQuery('INSERT INTO ingredientreview SET ?', post, function(err, result) {
	console.log(err);

		res.send(JSON.stringify({
			response: true || null,
		}));

	});
});

app.post('/AddRecipeeReview',function(req, res) {
	var RecipeeIDs = req.body.RecipeeID
	var Usernames = req.body.Username
	var Reviews = req.body.Review
	var Recommendeds = req.body.Recommended
	var queryString = 'SELECT * from recipeereview';
	var post = { Username: Usernames, Review: Reviews, Recommended: Recommendeds, RecipeeID: RecipeeIDs };

	DatabaseQuery('INSERT INTO recipeereview SET ?', post, function(err, result) {
	console.log(err);

		res.send(JSON.stringify({
			response: true || null,
		}));

	});
});




app.post('/AddIngredientToFavourites', function(req, res) {
	var username = req.body.username;
	var ingredientID = req.body.ingredient.id;

	var query = "INSERT IGNORE INTO favouriteingredients VALUES ('" + username  + "', " + ingredientID + ")";
	DatabaseQuery(query, function(err, result) {

	});

});

app.post('/CreateRecipee', uploadRecipeeImage.single('photo'), function(req, res, next) {
	var jsonState = JSON.parse(req.body.obj);
	var RecipeeName = jsonState.RecipeeName;
	var PreparationMethod = jsonState.PreparationMethod;
	var LoggedInUser = jsonState.LoggedInUser;
	var Language = jsonState.IngredientLanguage;


	var post  = { RecipeePhoto: req.file != undefined ? req.file.path : null, RecipeeName: RecipeeName, PreparationMethod: PreparationMethod, RecipeeUsername: LoggedInUser, RecipeeLanguage: Language};

	var query = DatabaseQuery('INSERT INTO recipee SET ?', post, function(err, result) {
		var insertQuery = "INSERT INTO recipeeingredients (RecipeeID, RecipeeIngredientID, RecipeeAmount) VALUES ";


		jsonState.RecipeeIngredients.map(Ingredient =>
			insertQuery = insertQuery + "(" + result.insertId + ", " + Ingredient.ID + ", " + Ingredient.Amount + "),"
		)

		if(insertQuery[insertQuery.length-1] === ",") {
			insertQuery = insertQuery.substring(0, insertQuery.length-1);
		}


		DatabaseQuery(insertQuery, function(err,result) {

			res.send(JSON.stringify({
				response: true || null,
			}));
		});
	});


});

app.post('/SearchForRecipee', function(req, res) {
	var slashes = require('slashes');
 		var RecipeeName = slashes.add(req.body.state.RecipeeName);
		var IncludedIngredients = req.body.state.IncludedIngredients;
		var ExcludedIngredients = req.body.state.ExcludedIngredients;
		var RecipeeLanguage = req.body.state.RecipeeLanguage;
		var onlyMineRecipees = req.body.state.onlyMineRecipees;
		var onlyFavouriteRecipees = req.body.state.onlyFavouriteRecipees;
		var LoggedInUser = req.body.state.LoggedInUser;

		var query = "select * from recipee r, recipeeingredients ri, ingredient i WHERE r.RecipeeName like '%" + RecipeeName + "%'";

		if(onlyMineRecipees === true) {
			query = query + " AND r.recipeeusername = '" + LoggedInUser + "'";
		}

		if(RecipeeLanguage != "All") {
			query = query + " AND r.recipeelanguage = '" + RecipeeLanguage + "'";
		}

		query = query + " AND r.RecipeeID = ri.RecipeeID AND ri.RecipeeIngredientID = i.ID"

		//console.log(query);

		DatabaseQuery(query, function(err, rows, fields) {
			var idOfRecipeToRemove = [];

			if(IncludedIngredients.length != 0) {
    	 	for (var i in rows) {
						var IngredientExists = false;
						for(var j in IncludedIngredients) {
							if(rows[i].Name.toLowerCase() == IncludedIngredients[j].Name.toLowerCase()) {
								IngredientExists = true;
							}
						}

						if(IngredientExists === false) {
							idOfRecipeToRemove.push(rows[i].RecipeeID);
						}
				}
			}

			if(ExcludedIngredients.length != 0) {
    	 	for (var i in rows) {
					for (var j in ExcludedIngredients) {

						if(rows[i].Name.toLowerCase() == ExcludedIngredients[j].Name.toLowerCase()) {
							idOfRecipeToRemove.push(rows[i].RecipeeID);
						}
					}

				}

			}

				var _ = require('lodash');
				idOfRecipeToRemove = _.uniq(idOfRecipeToRemove);

				var RecipeeArray = [];

				for(var i in rows) {
					var exists = false;
					for(var j in idOfRecipeToRemove) {
						if(rows[i].RecipeeID == idOfRecipeToRemove[j]) {
							exists = true;
						}
					}

					if(exists === false) {
						RecipeeArray.push(rows[i]);
					}
				}

					/* got recipees now need to convert amounts */

								for(var i in RecipeeArray) {
										var newAmount = RecipeeArray[i].RecipeeAmount;
          					RecipeeArray[i].Calories = (newAmount * (RecipeeArray[i].Calories/RecipeeArray[i].Amount) );
										RecipeeArray[i].VitaminA != 0 ? RecipeeArray[i].VitaminA = (newAmount * (RecipeeArray[i].VitaminA/RecipeeArray[i].Amount) ) : null;
					          RecipeeArray[i].VitaminB1 = (newAmount * (RecipeeArray[i].VitaminB1/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB2 = (newAmount * (RecipeeArray[i].VitaminB2/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB3 = (newAmount * (RecipeeArray[i].VitaminB3/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB5 = (newAmount * (RecipeeArray[i].VitaminB5/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB6 = (newAmount * (RecipeeArray[i].VitaminB6/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB9 = (newAmount * (RecipeeArray[i].VitaminB9/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminB12 = (newAmount * (RecipeeArray[i].VitaminB12/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminC = (newAmount * (RecipeeArray[i].VitaminC/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminD = (newAmount * (RecipeeArray[i].VitaminD/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminE = (newAmount * (RecipeeArray[i].VitaminE/RecipeeArray[i].Amount) );
					          RecipeeArray[i].VitaminK = (newAmount * (RecipeeArray[i].VitaminK/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Calcium = (newAmount * (RecipeeArray[i].Calcium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Chromium = (newAmount * (RecipeeArray[i].Chromium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Copper = (newAmount * (RecipeeArray[i].Copper/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Folate = (newAmount * (RecipeeArray[i].Folate/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Iron = (newAmount * (RecipeeArray[i].Iron/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Magnesium = (newAmount * (RecipeeArray[i].Magnesium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Manganese = (newAmount * (RecipeeArray[i].Manganese/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Phosphorus = (newAmount * (RecipeeArray[i].Phosphorus/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Potassium = (newAmount * (RecipeeArray[i].Potassium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Sodium = (newAmount * (RecipeeArray[i].Sodium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Selenium = (newAmount * (RecipeeArray[i].Selenium/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Zinc = (newAmount * (RecipeeArray[i].Zinc/RecipeeArray[i].Amount) );
					          RecipeeArray[i].MonounsaturatedFat = (newAmount * (RecipeeArray[i].MonounsaturatedFat/RecipeeArray[i].Amount) );
					          RecipeeArray[i].PolyunsaturatedFat = (newAmount * (RecipeeArray[i].PolyunsaturatedFat/RecipeeArray[i].Amount) );
					          RecipeeArray[i].SaturatedFat = (newAmount * (RecipeeArray[i].SaturatedFat/RecipeeArray[i].Amount) );
					          RecipeeArray[i].TransFat = (newAmount * (RecipeeArray[i].TransFat/RecipeeArray[i].Amount) );
					          RecipeeArray[i].OtherFats = (newAmount * (RecipeeArray[i].OtherFats/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Fiber = (newAmount * (RecipeeArray[i].Fiber/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Sugar = (newAmount * (RecipeeArray[i].Sugar/RecipeeArray[i].Amount) );
					          RecipeeArray[i].OtherCarbohydrates = (newAmount * (RecipeeArray[i].OtherCarbohydrates/RecipeeArray[i].Amount) );
					          RecipeeArray[i].Cholesterol= (newAmount * (RecipeeArray[i].Cholesterol/RecipeeArray[i].Amount) ) ;
					          RecipeeArray[i].Protein = (newAmount * (RecipeeArray[i].Protein/RecipeeArray[i].Amount) );
							}

							var Recipees = [  ];

							for(var i in RecipeeArray) {
								var index = _.findIndex(Recipees, function(o) { return o.RecipeeID == RecipeeArray[i].RecipeeID; });

								if(index == -1 ) {
									var Recipee = {
										RecipeeID: RecipeeArray[i].RecipeeID,
										RecipeeName: RecipeeArray[i].RecipeeName,
										Author: RecipeeArray[i].RecipeeUsername,
										RecipeePhoto: RecipeeArray[i].RecipeePhoto,
										PreparationMethod: RecipeeArray[i].PreparationMethod,
										RecipeeLanguage: RecipeeArray[i].RecipeeLanguage,
										TotalCalories: 0,
										TotalAmount: 0,
										TotalVitaminA: 0,
										TotalVitaminB1: 0,
										TotalVitaminB2: 0,
										TotalVitaminB3: 0,
										TotalVitaminB5: 0,
										TotalVitaminB6: 0,
										TotalVitaminB9: 0,
										TotalVitaminB12: 0,
										TotalVitaminC: 0,
										TotalVitaminD: 0,
										TotalVitaminE: 0,
										TotalVitaminK: 0,
										TotalCalcium: 0,
										TotalChromium: 0,
										TotalCopper: 0,
										TotalFolate: 0,
										TotalIron: 0,
										TotalMagnesium: 0,
										TotalManganese: 0,
										TotalPhosphorus: 0,
										TotalPotassium: 0,
										TotalSodium: 0,
										TotalSelenium: 0,
										TotalZinc: 0,
										TotalMonounsaturatedFat: 0,
										TotalPolyunsaturatedFat: 0,
										TotalSaturatedFat: 0,
										TotalTransFat: 0,
										TotalOtherFats: 0,
										TotalFiber: 0,
										TotalSugar: 0,
										TotalOtherCarbohydrates: 0,
										TotalCholesterol: 0,
										TotalProtein: 0,
										Ingredients: [ ]
									};
									Recipees.push(Recipee);



									/* now add ingredient */
									var Ingredient = {
										IngredientID: RecipeeArray[i].ID,
										IngredientName: RecipeeArray[i].Name,
										Calories: RecipeeArray[i].Calories,
										Amount: RecipeeArray[i].RecipeeAmount,
										Photo: RecipeeArray[i].Photo,
										VitaminA: RecipeeArray[i].VitaminA,
					          VitaminB1: RecipeeArray[i].VitaminB1,
					          VitaminB2: RecipeeArray[i].VitaminB2,
					          VitaminB3: RecipeeArray[i].VitaminB3,
					          VitaminB5: RecipeeArray[i].VitaminB5,
					          VitaminB6: RecipeeArray[i].VitaminB6,
					          VitaminB9: RecipeeArray[i].VitaminB9,
					          VitaminB12: RecipeeArray[i].VitaminB12,
					          VitaminC: RecipeeArray[i].VitaminC,
					          VitaminD: RecipeeArray[i].VitaminD,
					          VitaminE: RecipeeArray[i].VitaminE,
					          VitaminK: RecipeeArray[i].VitaminK,
					          Calcium: RecipeeArray[i].Calcium,
					          Chromium: RecipeeArray[i].Chromium,
					          Copper: RecipeeArray[i].Copper,
					          Folate: RecipeeArray[i].Folate,
					          Iron: RecipeeArray[i].Iron,
					          Magnesium: RecipeeArray[i].Magnesium,
					          Manganese: RecipeeArray[i].Manganese,
					          Phosphorus: RecipeeArray[i].Phosphorus,
					          Potassium: RecipeeArray[i].Potassium,
					          Sodium: RecipeeArray[i].Sodium,
					          Selenium: RecipeeArray[i].Selenium,
					          Zinc: RecipeeArray[i].Zinc,
					          MonounsaturatedFat: RecipeeArray[i].MonounsaturatedFat,
					          PolyunsaturatedFat: RecipeeArray[i].PolyunsaturatedFat,
					          SaturatedFat: RecipeeArray[i].SaturatedFat,
					          TransFat: RecipeeArray[i].TransFat,
					          OtherFats: RecipeeArray[i].OtherFats,
					          Fiber: RecipeeArray[i].Fiber,
					          Sugar: RecipeeArray[i].Sugar,
					          OtherCarbohydrates: RecipeeArray[i].OtherCarbohydrates,
					          Cholesterol: RecipeeArray[i].Cholesterol,
					          Protein: RecipeeArray[i].Protein,
									};



									Recipee.Ingredients.push(Ingredient);
									Recipee.TotalCalories = Recipee.TotalCalories + Ingredient.Calories;
									Recipee.TotalAmount = Recipee.TotalAmount + Ingredient.Amount,
									Recipee.TotalVitaminA = Recipee.TotalVitaminA + Ingredient.VitaminA,
									Recipee.TotalVitaminB1 = Recipee.TotalVitaminB1 + Ingredient.VitaminB1,
									Recipee.TotalVitaminB2 = Recipee.TotalVitaminB2 + Ingredient.VitaminB2,
									Recipee.TotalVitaminB3 = Recipee.TotalVitaminB3 + Ingredient.VitaminB3,
									Recipee.TotalVitaminB5 = Recipee.TotalVitaminB5 + Ingredient.VitaminB5,
									Recipee.TotalVitaminB6 = Recipee.TotalVitaminB6 + Ingredient.VitaminB6,
									Recipee.TotalVitaminB9 = Recipee.TotalVitaminB9 + Ingredient.VitaminB9,
									Recipee.TotalVitaminB12 = Recipee.TotalVitaminB12 + Ingredient.VitaminB12,
									Recipee.TotalVitaminC = Recipee.TotalVitaminC + Ingredient.VitaminC,
									Recipee.TotalVitaminD = Recipee.TotalVitaminD + Ingredient.VitaminD,
									Recipee.TotalVitaminE = Recipee.TotalVitaminE + Ingredient.VitaminE,
									Recipee.TotalVitaminK = Recipee.TotalVitaminK + Ingredient.VitaminK,
									Recipee.TotalCalcium = Recipee.TotalCalcium + Ingredient.Calcium,
									Recipee.TotalChromium = Recipee.TotalChromium + Ingredient.Chromium,
									Recipee.TotalCopper = Recipee.TotalCopper + Ingredient.Copper,
									Recipee.TotalFolate = Recipee.TotalFolate + Ingredient.Folate,
									Recipee.TotalIron = Recipee.TotalIron + Ingredient.Iron,
									Recipee.TotalMagnesium = Recipee.TotalMagnesium + Ingredient.Magnesium,
									Recipee.TotalManganese = Recipee.TotalManganese + Ingredient.Manganese,
									Recipee.TotalPhosphorus = Recipee.TotalPhosphorus + Ingredient.Phosphorus,
									Recipee.TotalPotassium = Recipee.TotalPotassium + Ingredient.Potassium,
									Recipee.TotalSodium = Recipee.TotalSodium + Ingredient.Sodium,
									Recipee.TotalSelenium = Recipee.TotalSelenium + Ingredient.Selenium,
									Recipee.TotalZinc = Recipee.TotalZinc + Ingredient.Zinc,
									Recipee.TotalMonounsaturatedFat = Recipee.TotalMonounsaturatedFat + Ingredient.MonounsaturatedFat,
									Recipee.TotalPolyunsaturatedFat = Recipee.TotalPolyunsaturatedFat + Ingredient.PolyunsaturatedFat,
									Recipee.TotalSaturatedFat = Recipee.TotalSaturatedFat + Ingredient.SaturatedFat,
									Recipee.TotalTransFat = Recipee.TotalTransFat + Ingredient.TransFat,
									Recipee.TotalOtherFats = Recipee.TotalOtherFats + Ingredient.OtherFats,
									Recipee.TotalFiber = Recipee.TotalFiber + Ingredient.Fiber,
									Recipee.TotalSugar = Recipee.TotalSugar + Ingredient.Sugar,
									Recipee.TotalOtherCarbohydrates = Recipee.TotalOtherCarbohydrates + Ingredient.OtherCarbohydrates,
									Recipee.TotalCholesterol = Recipee.TotalCholesterol + Ingredient.Cholesterol,
									Recipee.TotalProtein = Recipee.TotalProtein + Ingredient.Protein



								} else {
									var Ingredient = {
										IngredientID: RecipeeArray[i].ID,
										IngredientName: RecipeeArray[i].Name,
										Calories: RecipeeArray[i].Calories,
										Amount: RecipeeArray[i].RecipeeAmount,
										Photo: RecipeeArray[i].Photo,
										VitaminA: RecipeeArray[i].VitaminA,
					          VitaminB1: RecipeeArray[i].VitaminB1,
					          VitaminB2: RecipeeArray[i].VitaminB2,
					          VitaminB3: RecipeeArray[i].VitaminB3,
					          VitaminB5: RecipeeArray[i].VitaminB5,
					          VitaminB6: RecipeeArray[i].VitaminB6,
					          VitaminB9: RecipeeArray[i].VitaminB9,
					          VitaminB12: RecipeeArray[i].VitaminB12,
					          VitaminC: RecipeeArray[i].VitaminC,
					          VitaminD: RecipeeArray[i].VitaminD,
					          VitaminE: RecipeeArray[i].VitaminE,
					          VitaminK: RecipeeArray[i].VitaminK,
					          Calcium: RecipeeArray[i].Calcium,
					          Chromium: RecipeeArray[i].Chromium,
					          Copper: RecipeeArray[i].Copper,
					          Folate: RecipeeArray[i].Folate,
					          Iron: RecipeeArray[i].Iron,
					          Magnesium: RecipeeArray[i].Magnesium,
					          Manganese: RecipeeArray[i].Manganese,
					          Phosphorus: RecipeeArray[i].Phosphorus,
					          Potassium: RecipeeArray[i].Potassium,
					          Sodium: RecipeeArray[i].Sodium,
					          Selenium: RecipeeArray[i].Selenium,
					          Zinc: RecipeeArray[i].Zinc,
					          MonounsaturatedFat: RecipeeArray[i].MonounsaturatedFat,
					          PolyunsaturatedFat: RecipeeArray[i].PolyunsaturatedFat,
					          SaturatedFat: RecipeeArray[i].SaturatedFat,
					          TransFat: RecipeeArray[i].TransFat,
					          OtherFats: RecipeeArray[i].OtherFats,
					          Fiber: RecipeeArray[i].Fiber,
					          Sugar: RecipeeArray[i].Sugar,
					          OtherCarbohydrates: RecipeeArray[i].OtherCarbohydrates,
					          Cholesterol: RecipeeArray[i].Cholesterol,
					          Protein: RecipeeArray[i].Protein,
									};


									Recipees[index].Ingredients.push(Ingredient);
									Recipees[index].TotalCalories = Recipees[index].TotalCalories + Ingredient.Calories;
									Recipees[index].TotalAmount = Recipees[index].TotalAmount + Ingredient.Amount,
									Recipees[index].TotalVitaminA = Recipees[index].TotalVitaminA + Ingredient.VitaminA,
									Recipees[index].TotalVitaminB1 = Recipees[index].TotalVitaminB1 + Ingredient.VitaminB1,
									Recipees[index].TotalVitaminB2 = Recipees[index].TotalVitaminB2 + Ingredient.VitaminB2,
									Recipees[index].TotalVitaminB3 = Recipees[index].TotalVitaminB3 + Ingredient.VitaminB3,
									Recipees[index].TotalVitaminB5 = Recipees[index].TotalVitaminB5 + Ingredient.VitaminB5,
									Recipees[index].TotalVitaminB6 = Recipees[index].TotalVitaminB6 + Ingredient.VitaminB6,
									Recipees[index].TotalVitaminB9 = Recipees[index].TotalVitaminB9 + Ingredient.VitaminB9,
									Recipees[index].TotalVitaminB12 = Recipees[index].TotalVitaminB12 + Ingredient.VitaminB12,
									Recipees[index].TotalVitaminC = Recipees[index].TotalVitaminC + Ingredient.VitaminC,
									Recipees[index].TotalVitaminD = Recipees[index].TotalVitaminD + Ingredient.VitaminD,
									Recipees[index].TotalVitaminE = Recipees[index].TotalVitaminE + Ingredient.VitaminE,
									Recipees[index].TotalVitaminK = Recipees[index].TotalVitaminK + Ingredient.VitaminK,
									Recipees[index].TotalCalcium = Recipees[index].TotalCalcium + Ingredient.Calcium,
									Recipees[index].TotalChromium = Recipees[index].TotalChromium + Ingredient.Chromium,
									Recipees[index].TotalCopper = Recipees[index].TotalCopper + Ingredient.Copper,
									Recipees[index].TotalFolate = Recipees[index].TotalFolate + Ingredient.Folate,
									Recipees[index].TotalIron = Recipees[index].TotalIron + Ingredient.Iron,
									Recipees[index].TotalMagnesium = Recipees[index].TotalMagnesium + Ingredient.Magnesium,
									Recipees[index].TotalManganese = Recipees[index].TotalManganese + Ingredient.Manganese,
									Recipees[index].TotalPhosphorus = Recipees[index].TotalPhosphorus + Ingredient.Phosphorus,
									Recipees[index].TotalPotassium = Recipees[index].TotalPotassium + Ingredient.Potassium,
									Recipees[index].TotalSodium = Recipees[index].TotalSodium + Ingredient.Sodium,
									Recipees[index].TotalSelenium = Recipees[index].TotalSelenium + Ingredient.Selenium,
									Recipees[index].TotalZinc = Recipees[index].TotalZinc + Ingredient.Zinc,
									Recipees[index].TotalMonounsaturatedFat = Recipees[index].TotalMonounsaturatedFat + Ingredient.MonounsaturatedFat,
									Recipees[index].TotalPolyunsaturatedFat = Recipees[index].TotalPolyunsaturatedFat + Ingredient.PolyunsaturatedFat,
									Recipees[index].TotalSaturatedFat = Recipees[index].TotalSaturatedFat + Ingredient.SaturatedFat,
									Recipees[index].TotalTransFat = Recipees[index].TotalTransFat + Ingredient.TransFat,
									Recipees[index].TotalOtherFats = Recipees[index].TotalOtherFats + Ingredient.OtherFats,
									Recipees[index].TotalFiber = Recipees[index].TotalFiber + Ingredient.Fiber,
									Recipees[index].TotalSugar = Recipees[index].TotalSugar + Ingredient.Sugar,
									Recipees[index].TotalOtherCarbohydrates = Recipees[index].TotalOtherCarbohydrates + Ingredient.OtherCarbohydrates,
									Recipees[index].TotalCholesterol = Recipees[index].TotalCholesterol + Ingredient.Cholesterol,
									Recipees[index].TotalProtein = Recipees[index].TotalProtein + Ingredient.Protein

								}


							}



						var newQuery = "Select * from favouriterecipees WHERE username = '" + LoggedInUser + "'";
						var newArrayList = [];
						DatabaseQuery(newQuery, function(err, rows, fields) {
							if(onlyFavouriteRecipees === true) {

								for(var i=0; i<Recipees.length; i++) {

									var index = _.findIndex(rows, function(o) {
					          return o.RecipeeID == Recipees[i].RecipeeID
					        });


									if(index != -1) {
										newArrayList.push(Recipees[i]);
									}



								}
								Recipees = newArrayList;
							}

							res.send(JSON.stringify({
								response: Recipees,
							}));

						});




		});
});

app.post('/AddRecipeeToFavourites', function(req, res) {
	var username = req.body.LoggedInUser;
	var RecipeeID = req.body.RecipeeID;

	var query = "INSERT IGNORE INTO favouriterecipees VALUES ('" + username  + "', " + RecipeeID + ")";
	DatabaseQuery(query, function(err, result) { console.log(result) });

});

app.post('/CreateDiet', function(req, res) {
	var post = { DietName: req.body.DietName, DietCreator: req.body.username };

	DatabaseQuery('INSERT INTO diet SET ?', post, function(err, result) {
		var insertQuery = "INSERT INTO dietdays (DietID, DietDay, MealTime, RecipeeID) VALUES ";

		for(var i = 0; i < req.body.Diet.Days.length; i++) {
				for(var j=0; j < req.body.Diet.Days[i].Meals.length; j++) {
					insertQuery = insertQuery + "(" + result.insertId + ", "    + req.body.Diet.Days[i].Day  + ", '" + req.body.Diet.Days[i].Meals[j].Time + "'," + req.body.Diet.Days[i].Meals[j].Recipee.RecipeeID  + "),"
				}


		}
		insertQuery = insertQuery.substring(0, insertQuery.length - 1);


		DatabaseQuery(insertQuery, function(err, result) { console.log(result) });

		res.send(JSON.stringify({
			response: true,
		}));
	});
});


app.post('/SearchForDiets', function(req, res) {
	//console.log(req.body);
	var slashes = require('slashes');
	var DietName = slashes.add(req.body.DietName);
	var username = slashes.add(req.body.username);
	var onlyMineDiets = req.body.OnlyMineDiets;
	var onlyFavouriteDiets = req.body.onlyFavouriteDiets;


	var query = "select * from diet WHERE DietName like '%" + DietName + "%' ";


	if(onlyMineDiets === true) {
		query = query + "AND DietCreator = '" + username + "'";
	}

		DatabaseQuery(query, function(err, rows, fields) {
			var DietDaysQuery = "SELECT * FROM dietdays ";
			var first = true;
				var DietInformation = [];

				for(var i = 0; i < rows.length; i++) {

				var Diet = {
					DietID: rows[i].DietID,
					DietName: rows[i].DietName,
					DietCreator: rows[i].DietCreator
				}
				DietInformation.push(Diet);

				if(first === true) {
					DietDaysQuery = DietDaysQuery + "WHERE DietID =" + rows[i].DietID + " ";
					first = false;
				} else {
					DietDaysQuery = DietDaysQuery + "OR DietID =" + rows[i].DietID + " "
				}
			}


		DatabaseQuery(DietDaysQuery, function(err, rows, fields) {
				var DietsList = [ ];
				var _ = require('lodash');

				for(var i = 0; i < rows.length; i++) {
					var Diet = {DietID: rows[i].DietID, DietDays: [] };

					var index = _.findIndex(DietsList, function(o) {
						return o.DietID == rows[i].DietID;
					});

					// if not exists add
					if(index == -1) {
						DietsList.push(Diet);
					}
				}

				for(var i = 0; i < rows.length; i++) {
					var MealTime = rows[i].MealTime;
					var RecipeeID = rows[i].RecipeeID;
					var DietDay = rows[i].DietDay;
					var DietID = rows[i].DietID;


					// find index of dietid
					var index = _.findIndex(DietsList, function(o) {
						return o.DietID == DietID;
					});

					//console.log(index);

					// find if day exists

					var index1 = _.findIndex(DietsList[index].DietDays, function(o) {
						return o.DietDay == DietDay;
					});

					//console.log(index1);

					if(index1 == -1) {
						//console.log('day doesnt exists');
						var Day = {Day: DietDay, Meals: [{MealTime: MealTime, Recipee: {RecipeeID: RecipeeID, RecipeeIngredients: []}} ] };
						DietsList[index].DietDays.push(Day);
					} else {
						//console.log('day exists');
						// find day index and push meals

						var Day = {Day: DietDay, Meals: [{MealTime: MealTime, Recipee: {RecipeeID: RecipeeID, RecipeeIngredients: []}} ] };
						DietsList[index].DietDays[index1].push(Day);
					}
				}

				var getRecipeesWithNutritonalInfoQuery = "SELECT * " +
				"from recipee r, recipeeingredients ri, ingredient i " +
				"WHERE r.RecipeeID = ri.RecipeeID AND ri.RecipeeIngredientID = i.ID"

				DatabaseQuery(getRecipeesWithNutritonalInfoQuery, function(err, rows, fields) {

				var Recipees = [];

				for(var i = 0; i < rows.length; i++) {
				var Recipee = {
				RecipeeID: rows[i].RecipeeID,
				RecipeeName: rows[i].RecipeeName,
				PreparationMethod: rows[i].PreparationMethod,
				RecipeeUsername: rows[i].RecipeeUsername,
				RecipeeLanguage: rows[i].RecipeeLanguage,
				RecipeePhoto: rows[i].RecipeePhoto,
				RecipeeID: rows[i].RecipeeID,
				RecipeeIngredientID: rows[i].RecipeeIngredientID,
				RecipeeAmount: rows[i].RecipeeAmount,
				ID: rows[i].ID,
				Name: rows[i].Name,
				Username: rows[i].Username,
				Description: rows[i].Description,
				Language: rows[i].Language,
				Photo: rows[i].Photo,
				Calories: (rows[i].RecipeeAmount * (rows[i].Calories/rows[i].Amount)),
				Amount: rows[i].Amount,
				VitaminA: (rows[i].RecipeeAmount * (rows[i].VitaminA/rows[i].Amount) ) ,
				VitaminB1 : (rows[i].RecipeeAmount * (rows[i].VitaminB1/rows[i].Amount) ),
				VitaminB2 : (rows[i].RecipeeAmount * (rows[i].VitaminB2/rows[i].Amount) ),
				VitaminB3 : (rows[i].RecipeeAmount * (rows[i].VitaminB3/rows[i].Amount) ),
				VitaminB5 : (rows[i].RecipeeAmount * (rows[i].VitaminB5/rows[i].Amount) ),
				VitaminB6 : (rows[i].RecipeeAmount * (rows[i].VitaminB6/rows[i].Amount) ),
				VitaminB9 : (rows[i].RecipeeAmount * (rows[i].VitaminB9/rows[i].Amount) ),
				VitaminB12 : (rows[i].RecipeeAmount * (rows[i].VitaminB12/rows[i].Amount) ),
				VitaminC : (rows[i].RecipeeAmount * (rows[i].VitaminC/rows[i].Amount) ),
				VitaminD : (rows[i].RecipeeAmount * (rows[i].VitaminD/rows[i].Amount) ),
				VitaminE : (rows[i].RecipeeAmount * (rows[i].VitaminE/rows[i].Amount) ),
				VitaminK : (rows[i].RecipeeAmount * (rows[i].VitaminK/rows[i].Amount) ),
				Calcium : (rows[i].RecipeeAmount * (rows[i].Calcium/rows[i].Amount) ),
				Chromium : (rows[i].RecipeeAmount * (rows[i].Chromium/rows[i].Amount) ),
				Copper : (rows[i].RecipeeAmount * (rows[i].Copper/rows[i].Amount) ),
				Folate : (rows[i].RecipeeAmount * (rows[i].Folate/rows[i].Amount) ),
				Iron : (rows[i].RecipeeAmount * (rows[i].Iron/rows[i].Amount) ),
				Magnesium : (rows[i].RecipeeAmount * (rows[i].Magnesium/rows[i].Amount) ),
				Manganese : (rows[i].RecipeeAmount * (rows[i].Manganese/rows[i].Amount) ),
				Phosphorus : (rows[i].RecipeeAmount * (rows[i].Phosphorus/rows[i].Amount) ),
				Potassium : (rows[i].RecipeeAmount * (rows[i].Potassium/rows[i].Amount) ),
				Sodium : (rows[i].RecipeeAmount * (rows[i].Sodium/rows[i].Amount) ),
				Selenium : (rows[i].RecipeeAmount * (rows[i].Selenium/rows[i].Amount) ),
				Zinc : (rows[i].RecipeeAmount * (rows[i].Zinc/rows[i].Amount) ),
				MonounsaturatedFat : (rows[i].RecipeeAmount * (rows[i].MonounsaturatedFat/rows[i].Amount) ),
				PolyunsaturatedFat : (rows[i].RecipeeAmount * (rows[i].PolyunsaturatedFat/rows[i].Amount) ),
				SaturatedFat : (rows[i].RecipeeAmount * (rows[i].SaturatedFat/rows[i].Amount) ),
				TransFat : (rows[i].RecipeeAmount * (rows[i].TransFat/rows[i].Amount) ),
				OtherFats : (rows[i].RecipeeAmount * (rows[i].OtherFats/rows[i].Amount) ),
				Fiber : (rows[i].RecipeeAmount * (rows[i].Fiber/rows[i].Amount) ),
				Sugar : (rows[i].RecipeeAmount * (rows[i].Sugar/rows[i].Amount) ),
				OtherCarbohydrates : (rows[i].RecipeeAmount * (rows[i].OtherCarbohydrates/rows[i].Amount) ),
				Cholesterol: (rows[i].RecipeeAmount * (rows[i].Cholesterol/rows[i].Amount) ) ,
				Protein : (rows[i].RecipeeAmount * (rows[i].Protein/rows[i].Amount) )
			}
			Recipees.push(Recipee);

}



				// now we have recipees and their nutrtiional information, we need this information inputed in DietsList
				//DietsList[DietID].DietDays[Day].Meals[MealTime].Recipee.RecipeeID

				// go through dietslist
				for(var i = 0; i < DietsList.length; i++) {
					//console.log(DietsList[i].DietID);
					for(var j = 0; j < DietsList[i].DietDays.length; j++) {
						//console.log("\t " + DietsList[i].DietDays[j].Day);
						for(var m = 0; m < DietsList[i].DietDays[j].Meals.length; m++) {
							//console.log("\t \t " + DietsList[i].DietDays[j].Meals[m].Recipee.RecipeeID);

							for(var ii = 0; ii < Recipees.length; ii++) {
								if(Recipees[ii].RecipeeID == DietsList[i].DietDays[j].Meals[m].Recipee.RecipeeID ) {
									// take recipees information and insert into DietsList...
									var obj = Recipees[ii];
									DietsList[i].DietDays[j].Meals[m].Recipee.RecipeeIngredients.push(obj);
								}
							}


						}
					}

				}

				// add diet information
















				var newQuery = "Select * from favouritediets WHERE username = '" + username + "'";

				DatabaseQuery(newQuery, function(err, rows, fields) {
					//console.log(rows);

						var newDietList = [];
					if(onlyFavouriteDiets === true) {


						for(var i=0; i<DietsList.length; i++) {
							var index = _.findIndex(rows, function(o) {
								return o.DietID == DietsList[i].DietID
							});

							if(index != -1) {
								newDietList.push(DietsList[i]);
							}

					}

					//DietsList = newDietList;
				}






				for(var i=0; i<DietsList.length; i++) {
					DietsList[i].RealDays = [];
					for(var j=0; j<DietsList[i].DietDays.length; j++) {
						for(var k=0; k<DietsList[i].DietDays[j].Meals.length; k++) {
							for(var v=0; v<DietsList[i].DietDays[j].Meals.length; v++) {
								DietsList[i].DietDays[j].Meals[v].PreparationMethod = DietsList[i].DietDays[j].Meals[0].Recipee.RecipeeIngredients[0].PreparationMethod;
								DietsList[i].DietDays[j].Meals[v].RecipeeName = DietsList[i].DietDays[j].Meals[0].Recipee.RecipeeIngredients[0].RecipeeName;
								DietsList[i].DietDays[j].Meals[v].Photo = DietsList[i].DietDays[j].Meals[0].Recipee.RecipeeIngredients[0].RecipeePhoto;
								DietsList[i].DietDays[j].Meals[v].RecipeeID = DietsList[i].DietDays[j].Meals[v].Recipee.RecipeeID;
							}
						}
					}
				}


				for(var i=0; i<DietsList.length; i++) {
					for(var j=0; j<DietsList[i].DietDays.length; j++) {
						for(var k=0; k<DietsList[i].DietDays[j].Meals.length; k++) {
							for(var v=0; v<DietsList[i].DietDays[j].Meals.length; v++) {

									var index = _.findIndex(DietsList[i].RealDays, function(o) {
										return o.Day == DietsList[i].DietDays[j].Day
									});

									if(index == -1) {
										DietsList[i].RealDays.push({
											Day: DietsList[i].DietDays[j].Day
										});

										var index1 = _.findIndex(DietsList[i].RealDays, function(o) {
											return o.Day == DietsList[i].DietDays[j].Day
										});

										DietsList[i].RealDays[index1].Meals = [];
										DietsList[i].RealDays[index1].Meals.push(DietsList[i].DietDays[j].Meals[v]);

									} else {
										var index1 = _.findIndex(DietsList[i].RealDays, function(o) {
											return o.Day == DietsList[i].DietDays[j].Day
										});
										DietsList[i].RealDays[index1].Meals.push(DietsList[i].DietDays[j].Meals[v]);

									}



							}
						}
					}
				}



				// add names to newDietList and DietsList then filter diets
				for(var i=0; i<DietsList.length; i++) {

				}

				for(var i=0; i<newDietList.length; i++) {

				}




				if(DietInformation.length === 0) {
					newDietList = [];
					DietsList = [];
				} else {

					//append info:
					for(var i=0; i<DietsList.length; i++) {
						//console.log(DietsList[i].DietID);

						var DietID = DietsList[i].DietID;

						//console.log(DietInformation);

						var index = _.findIndex(DietInformation, function(o) {
							return o.DietID == DietID
						});


						if(index != -1 ) {
							DietsList[i].DietName = DietInformation[index].DietName;
							DietsList[i].DietCreator = DietInformation[index].DietCreator;
						}

					}

					for(var i=0; i<newDietList.length; i++) {
						//console.log(newDietList[i].DietID);

						var DietID = newDietList[i].DietID;

						//console.log(DietInformation);

						var index = _.findIndex(DietInformation, function(o) {
							return o.DietID == DietID
						});

						//console.log("index", index);

						if(index != -1 ) {
							newDietList[i].DietName = DietInformation[index].DietName;
							newDietList[i].DietCreator = DietInformation[index].DietCreator;
						}

					}

				}



				var jsonString = JSON.stringify(DietsList);
				var jsonPretty = JSON.stringify(JSON.parse(jsonString),null,2);
				//console.log(jsonPretty);


				onlyFavouriteDiets === true ? console.log(newDietList) : console.log(DietsList);

					res.send(JSON.stringify({
						response: onlyFavouriteDiets === true ? newDietList : DietsList
					}));
				});








				});
		});
		});
});


app.post('/AddDietToFavourites', function(req, res) {
	var username = req.body.LoggedInUser;
	var DietID = req.body.DietID;

	var query = "INSERT IGNORE INTO favouritediets VALUES ('" + username  + "', " + DietID + ")";
	DatabaseQuery(query, function(err, result) {
		res.send(JSON.stringify({
			response: 'response',
		}));
	});

});


app.post('/CreateDietingJourney', function(req, res) {

		var DietingJourneyCreator = req.body.DietingJourneyCreator;
		var DietingJourneyName = req.body.DietingJourneyName;
		var StartingDate = req.body.StartingDate;
		var DietID = req.body.DietID;
		var DietDays = req.body.DietDays;

		var post  = {
			DietingJourneyCreator: DietingJourneyCreator,
			DietingJourneyName: DietingJourneyName,
			StartingDate: new Date(StartingDate),
			DietID: DietID
		};

	var query = DatabaseQuery('INSERT INTO dietingjourney SET ?', post, function(err, result) {
		var InsertQuery = "INSERT INTO dietingjourneydays (DietingJourneyID, Day, Weight, Note) VALUES ";
		// somthing wrong with this one lol

		for(var i=0; i<DietDays; i++) {
			InsertQuery = InsertQuery + "(" + result.insertId + ", " + (i+1) + ", " + 0 + ", " + "'Note'" + "),";
		}

		if(InsertQuery[InsertQuery.length-1] === ",") {
			InsertQuery = InsertQuery.substring(0, InsertQuery.length-1);
		}



		DatabaseQuery(InsertQuery, function(err, result) {



			res.send(JSON.stringify({
				response: 'response',
			}));
		});
	});



});


app.post('/getDietingJourneys', function(req, res) {
	var username = req.body.username;

	var query = "select * from dietingjourney WHERE DietingJourneyCreator='" + username + "'";

	DatabaseQuery(query, function(err, result) {
		res.send(JSON.stringify({
			response: result,
		}));
	});

});

app.post('/getDietingJourneysDays', function(req, res) {
	var DietingJourneyID = req.body.DietingJourneyID;

	var query = "select * from dietingjourneydays WHERE DietingJourneyID=" + DietingJourneyID + "";

	DatabaseQuery(query, function(err, result) {
		res.send(JSON.stringify({
			response: result,
		}));
	});

});

app.post('/updateDietingJourneysDays', function(req, res) {

	var doesRowExists = 'SELECT EXISTS(SELECT * FROM dietingjourneydays WHERE DietingJourneyID = ' + req.body.DietingJourneyID +  " AND Day=" + req.body.Day + ") AS Solution;";


	DatabaseQuery(doesRowExists, function(err, rows, fields) {
		if(rows[0].Solution == 1) {
			// update hereee

			var DietingJourneyID = req.body.DietingJourneyID;
			var Day = req.body.Day;
			var Weight = req.body.Weight;

			var query = "UPDATE dietingjourneydays SET Weight=" + Weight +
			" WHERE DietingJourneyID=" + DietingJourneyID + " AND Day=" + Day ;


			DatabaseQuery(query, function(err, rows, fields) {
				res.send(JSON.stringify({
					response: 'result',
				}));
			});

		}
		else {

			var post  = {
				DietingJourneyID: req.body.DietingJourneyID,
				Day: req.body.Day,
				Weight: req.body.Weight,
			};

		var query = DatabaseQuery('INSERT INTO dietingjourneydays SET ?', post, function(err, result) {

			res.send(JSON.stringify({
				response: 'result',
			}));

		});

		}
	});


});

app.post('/updateDietingJourneysDaysNote', function(req, res) {
	var slashes = require('slashes');
	var doesRowExists = 'SELECT EXISTS(SELECT * FROM dietingjourneydays WHERE DietingJourneyID = ' + req.body.DietingJourneyID +  " AND Day=" + req.body.Day + ") AS Solution;";

	DatabaseQuery(doesRowExists, function(err, rows, fields) {
		if(rows[0].Solution == 1) {

			var DietingJourneyID = req.body.DietingJourneyID;
			var Day = req.body.Day;
			var Note = slashes.add(req.body.Note);

			var query = "UPDATE dietingjourneydays SET Note='" + Note + "' WHERE DietingJourneyID=" + DietingJourneyID + " AND Day=" + Day ;


			DatabaseQuery(query, function(err, rows, fields) {
				res.send(JSON.stringify({
					response: true,
				}));
			});

		}
		else {

			var post  = {
				DietingJourneyID: req.body.DietingJourneyID,
				Day: req.body.Day,
				Note: req.body.Note,
			};

		var query = DatabaseQuery('INSERT INTO dietingjourneydays SET ?', post, function(err, result) {
			res.send(JSON.stringify({
				response: 'result',
			}));

		});

		}
	});


});



app.post('/AddDietReview',function(req, res) {
	var DietIDs = req.body.DietID
	var Usernames = req.body.Username
	var Reviews = req.body.Review
	var Recommendeds = req.body.Recommended

	var queryString = 'SELECT * from dietreview';
	var post = { Username: Usernames, Review: Reviews, Recommended: Recommendeds, DietID: DietIDs };

	DatabaseQuery('INSERT INTO dietreview SET ?', post, function(err, result) {
	console.log(err);

		res.send(JSON.stringify({
			response: true || null,
		}));

	});
});


app.post('/DietReviews',function(req, res) {
	var DietID = req.body.DietID;
	var queryString = 'SELECT * FROM dietreview WHERE DietID =' + DietID + " ORDER BY TimeAndDate DESC";

	DatabaseQuery(queryString, function(err, rows, fields) {
    if (err) throw err;

		var reviews = [];

    for (var i in rows) {
			var review = {
				ID: rows[i].ID,
				DietID: rows[i].DietID,
				Username: rows[i].Username,
				UsernameID: rows[i].UsernameID,
				Review: rows[i].Review,
				Recommended: rows[i].Recommended,
				TimeAndDate: rows[i].TimeAndDate
			}
			reviews.push(review);
    }

		res.send(JSON.stringify({
			response: reviews || null,
		}));

});
});


app.listen(3001, function () {
  console.log('Server is running. Point your browser to: http://52.18.248.248:3001');
});
