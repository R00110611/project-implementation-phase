/*
  Got this code from
  http://stackoverflow.com/questions/30545749/how-to-provide-a-mysql-database-connection-in-single-file-in-nodejs
  by @Rafael - http://stackoverflow.com/users/1539658/rafael

  Why?
    Because the database connections were not closing before as I had it in file also but not closing and this was crashing
    the web server (MAXIMUM CONNECTIONS LIMITED WAS REACHED...).
*/

var mysql   = require('mysql'),
    config  = require("./config");

var sqlConnection = function sqlConnection(sql, values, next) {
    if (arguments.length === 2) {
        next = values;
        values = null;
    }

    var connection = mysql.createConnection(config.db);
    connection.connect(function(err) {
        if (err !== null) {
            console.log("[MYSQL] Error connecting to mysql:" + err+'\n');
        }
    });

    connection.query(sql, values, function(err) {

        connection.end();

        if (err) {
            throw err;
        }

        next.apply(this, arguments);
    });
}

module.exports = sqlConnection;
