import React, { Component } from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ManagementPanel from './Components/Management/ManagementPanel'
import Unauthenticated from './Components/Authentication/Unauthenticated/Unauthenticated.js'
import Authenticated from './Components/Authentication/Authenticated/Authenticated.js'

class App extends Component {
  constructor(props) {
		super(props);
		this.setAuthenticated = this.setAuthenticated.bind(this);
		this.setUsername = this.setUsername.bind(this);
    this.setLanguage = this.setLanguage.bind(this);

		this.state = {
			authenticated: false,
			username: "",
      Language: "English",
      LanguageValue: 1
		};
	}

  setLanguage(event, index, val) {
    this.setState({LanguageValue: val});
    val === 1 || event.target.value === "English" ? this.setState({Language: 'English'}) : null;
    val === 2 || event.target.value === "Polish" ? this.setState({Language: 'Polish'}) : null;
    val === 3 || event.target.value === "Spanish"? this.setState({Language: 'Spanish'}) : null;
  }

  setAuthenticated(bool) {
		this.setState({authenticated: bool});
	}

	setUsername(usernameLogin) {
    this.setState({username: usernameLogin});
	}

  render() {
    return (
      <MuiThemeProvider className="MuiThemeProvider">
  			<div className="Application">
          <div style={{display: this.state.authenticated === true ? 'none' : null}}>
            {this.state.authenticated === false ? <Unauthenticated authenticated={this.state.authenticated} Language={this.state.Language} setLanguage={this.setLanguage} LanguageValue={this.state.LanguageValue} setUsername={this.setUsername} setAuthenticated={this.setAuthenticated} /> : <null />}
          </div>

            {this.state.authenticated === true ? <Authenticated authenticated={this.state.authenticated} Language={this.state.Language} setLanguage={this.setLanguage} LanguageValue={this.state.LanguageValue} setAuthenticated={this.setAuthenticated} LoggedInUser={this.state.username} /> : <null/> }

          <div className="ManagementPanel">
            {this.state.authenticated === true ? <ManagementPanel Language={this.state.Language} LoggedInUser={this.state.username} /> : <null/> }
          </div>



        </div>
    	</MuiThemeProvider>
    );
  }
}

export default App;
