
import React from 'react'
import Top from './Top/Top.js'
import Middle from './Middle/Middle.js'
import Bottom from './Bottom/Bottom.js'
import Language from '../Language'

export default class Unauthenticated extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ActiveMiddleTab: {
        LoginActive: true,
        CreateAnAccountActive: false,
        ForgotPasswordActive: false
      }
    }

    this.setActiveTab = this.setActiveTab.bind(this);
  }

  setActiveTab(Tab) {
    this.setState({ActiveMiddleTab: {
        LoginActive: Tab == "LoginActive" ? true : false,
        CreateAnAccountActive: Tab == "CreateAnAccountActive" ? true : false,
        ForgotPasswordActive: Tab == "ForgotPasswordActive" ? true : false
      }
    });
  }

    render() {
        return (
            <div className="TopMiddleBottomParent">
              <Top />
              <Middle Language={this.props.Language} setUsername={this.props.setUsername} ActiveMiddleTab={this.state.ActiveMiddleTab} setAuthenticated={this.props.setAuthenticated} />
              <Bottom Language={this.props.Language} setUsername={this.props.setUsername} ActiveMiddleTab={this.state.ActiveMiddleTab} setActiveTab={this.setActiveTab} />
              <div className="LanguageUnauthenticated"> <Language active={1} Language={this.props.Language} setLanguage={this.props.setLanguage} LanguageValue={this.props.LanguageValue} /> </div>
            </div>
        );
    }
}
