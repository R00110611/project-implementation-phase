
import React from 'react'
import Login from './Login'
import CreateAnAccount from './CreateAnAccount'
import ForgottenPassword from './ForgottenPassword'

export default class Middle extends React.Component {
    render() {
        return (
            <div className="Middle">
              { this.props.ActiveMiddleTab.LoginActive === true ? <Login Language={this.props.Language} setUsername={this.props.setUsername} setAuthenticated={this.props.setAuthenticated} /> : null       }
              { this.props.ActiveMiddleTab.CreateAnAccountActive === true ? <CreateAnAccount Language={this.props.Language} setUsername={this.props.setUsername} setAuthenticated={this.props.setAuthenticated} /> : null       }
              { this.props.ActiveMiddleTab.ForgotPasswordActive === true ? <ForgottenPassword Language={this.props.Language} /> : null       }
            </div>
        );
    }
}
