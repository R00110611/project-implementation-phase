
import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'

export default class ForgottenPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Username: "",
      UsernameErrorText: "",
      RetrieveMethod: "Email"
    }

    this.RetrievePassword = this.RetrievePassword.bind(this);
  }

  RetrievePassword() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/retrievePassword', {
        username: this.state.Username,
        retrieveMethod: this.state.RetrieveMethod
      })
      .then(function (response) {
        if(response.data.success === "true") {
          toastr.timeOut = 66666;
          toastr.success('Email with your password has been sent to your email.', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
        }

        if(response.data.success === "false" || response.data.success === "telephoneFalse" ) {
          toastr.error('Username ' + outer.state.Username + ' doesnt exists!', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
        }

        if(response.data.success === "telephoneTrue") {
          toastr.success('Text with your password has been sent to your number', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
        }

        if(response.data.success === "telephoneNoExist") {
          toastr.error('User has no phone number associated to him', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
        }

      })
      .catch(function (error) {
        console.log(error);
      });
  }

    render() {
        return (
            <div className="ForgottenPassword111">
              <div className="ForgottenPasswordText"> {this.props.Language === 'English' ? 'Retrieve password' : this.props.Language === 'Spanish' ? 'Recuperar Contraseña' : 'Odzyskaj haslo'       }





              <div className="ForgottenPasswordInput">
                <TextField
                  floatingLabelText={this.props.Language === 'English' ? 'Username' : this.props.Language === 'Spanish' ? 'Nombre de usuario' : 'Uzytkownik'       }
                  value={this.state.Username}
                    floatingLabelStyle={{ color: 'grey'}}
                  errorText={(this.state.Username.length > 9) ? "This field cannot have more then 9 characters" : this.state.UsernameErrorText}
                  onChange={e => this.setState({ Username: e.target.value, UsernameErrorText: "" }) }
                />
              </div>

              <br/>

              <div className="RadioButtons">
                <RadioButtonGroup defaultSelected={"Email"} onChange={e => this.setState({ RetrieveMethod: e.target.value })}      >
                      <RadioButton
                        value="Email"
                        labelStyle={{color: "grey"}}
                        label={this.props.Language === 'English' ? "Retrieve password by Email" : this.props.Language === 'Spanish' ? 'Recuperar contraseña por correo electrónico' : 'Odzyskaj haslo uzywajac e-mail'       }
                        style={{width: '90%'}}
                      />
                      <RadioButton
                        value="Telephone"
                        labelStyle={{color: "grey"}}
                        label={this.props.Language === 'English' ? 'Retrieve password by telephone' : this.props.Language === 'Spanish' ? 'Recuperar contraseña por teléfono' : 'Odzyskaj haslo telefonem'       }
                        style={{width: '90%'}}
                      />
                </RadioButtonGroup>
              </div>
</div>

  <br/>

              <div className="ForgottenPasswordButton">
                <RaisedButton
                  label={this.props.Language === 'English' ? 'Submit' : this.props.Language === 'Spanish' ? 'recuperar' : 'Odzyskaj haslo'       }
                  onClick={this.RetrievePassword}
                  backgroundColor="navy"
                  labelColor="white"
                  style={{alignitems: 'center'}}
                  disabled={( (this.state.Username.length > 9) ? true : false)}
                />
              </div>
            </div>
        );
    }
}
