
import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Username: "",
      Password: "",
      UsernameErrorText: "",
      PasswordErrorText: ""
    }

    this.SignIn = this.SignIn.bind(this);
    this.IncorrectCredentialsEntered = this.IncorrectCredentialsEntered.bind(this);
    this.Login = this.Login.bind(this);
  }

  Login() {
    this.props.setUsername(this.state.Username);
    this.props.setAuthenticated(true);
  }

  IncorrectCredentialsEntered(userExists) {
    userExists === true ? this.setState({PasswordErrorText: this.props.Language === 'English' ? 'Incorrect Password' : this.props.Language === 'Spanish' ? 'Contraseña incorrecta' : 'Haslo Niepoprawne'        }) : this.setState({UsernameErrorText: this.props.Language === 'English' ? 'User does not exists' : this.props.Language === 'Spanish' ? 'El usuario no existe' : 'Uzytkownik nie istnieje'      });
  }

  SignIn() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/login', {
        username: this.state.Username,
        password: this.state.Password
      })
      .then(function (response) {
        response.data.AuthenticateUser === true ? outer.Login() : outer.IncorrectCredentialsEntered(response.data.UserExists);
      })
      .catch(function (error) {
        console.log(error);
      });
    }

    render() {
        return (
            <div>
              <div className="LoginText"> {this.props.Language === 'English' ? 'Sign-in' : this.props.Language === 'Spanish' ? 'Iniciar sesión' : 'Logowanie'       }


              <div style={{textAlign: 'center', overflow: 'hidden'}}>
                  <TextField
                    floatingLabelText={this.props.Language === 'English' ? 'Username' : this.props.Language === 'Spanish' ? 'Nombre de usuario' : 'Uzytkownik'       }
                    floatingLabelStyle={{ color: 'grey'}}
                    value={this.state.Username}
                    errorText={(this.state.Username.length > 9) ? (this.state.Username.length > 9) ? this.props.Language === 'English' ? "This field cannot have more then 9 characters" : this.props.Language === 'Spanish' ? 'Este campo no puede contener más de 9 caracteres' : 'Uzytkownik nie moze miec wiecej niz 9 liter!' : this.state.PasswordErrorText : this.state.UsernameErrorText}
                    onChange={e => this.setState({ Username: e.target.value, UsernameErrorText: "" }) }
                  />


                  <br/>

                  <TextField
                    floatingLabelText={this.props.Language === 'English' ? 'Password' : this.props.Language === 'Spanish' ? 'Contraseña' : 'Haslo'}
                    type='password'
                    floatingLabelStyle={{ color: 'grey'}}
                    value={this.state.Password}
                    errorText={ (this.state.Password.length > 25) ? this.props.Language === 'English' ? "This field cannot have more then 25 characters" : this.props.Language === 'Spanish' ? 'Este campo no puede contener más de 25 caracteres' : 'Haslo nie moze miec wiecej niz 25 liter!' : this.state.PasswordErrorText}
                    onChange={e => this.setState({ Password: e.target.value, PasswordErrorText: "" }) }
                  />


                  <br/><br/>

</div>
        </div>
        <br/>


            <div style={{textAlign: 'center'}}>
                  <RaisedButton
                    label={this.props.Language === 'English' ? 'Sign-in' : this.props.Language === 'Spanish' ? 'Iniciar sesión' : 'Zaloguj sie'}
                    backgroundColor="navy"
                    labelColor="white"
                    onClick={this.SignIn}
                    style={{alignitems: 'center'}}
                    disabled={( (this.state.Username.length > 9) ||  (this.state.Password.length > 25) ? true : false)}
                  />
              </div>


                <br/>

            </div>
        );
    }
}
