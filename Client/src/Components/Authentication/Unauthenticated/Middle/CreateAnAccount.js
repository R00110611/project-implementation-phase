
import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'

export default class CreateAnAccount extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Username: "",
      Password: "",
      EmailAddress: "",
      UsernameErrorText: "",
      PasswordErrorText: "",
      EmailAddressErrorText: ""
    }

    this.CreateAnAccount = this.CreateAnAccount.bind(this);
    this.IncorrectCredentialsEntered = this.IncorrectCredentialsEntered.bind(this);
    this.Login = this.Login.bind(this);
    this.Validate = this.Validate.bind(this);
  }

  Login() {
    this.props.setUsername(this.state.Username);
    this.props.setAuthenticated(true);
  }

  Validate() {
    var validated = false;

    if(this.state.EmailAddress.length == 0 || this.state.EmailAddress.length > 224) {
        this.setState({EmailAddressErrorText: "Email address cannot be empty or have more than 224 characters!"});
        this.setState({EmailAddressErrorText: this.props.Language === 'English' ? 'Email address cannot be empty or have more than 224 characters!' : this.props.Language === 'Spanish' ? '¡La dirección de correo electrónico no puede estar vacía o tener más de 224 caracteres!' : 'Pole email nie moze miec wiecej niz 224 liter lub byc puste!'            });
    }

    if(this.state.Username.length == 0) {
        this.setState({UsernameErrorText: "Username cannot be empty"});
    }

    if(this.state.Password.length == 0) {
        this.setState({PasswordErrorText: "Password cannot be empty"});
    }

    if(this.state.EmailAddress.length != 0 && this.state.Username.length != 0 && this.state.Password.length != 0) {
      validated = true;
    }

    if(this.state.Username.search('\'') != -1 || this.state.EmailAddress.search('\'') != -1 || this.state.Password.search('\'') != -1 ) {
      toastr.error('make sure input doesn\'t contain apostrophoes or quotes! ', 'Error', {positionClass: 'toast-top-center', preventDuplicates: true});
      validated = false;
    }
    return validated;
  }

  CreateAnAccount() {
    var axios = require('axios');
    var outer = this;

    if(this.Validate() === true) {

    axios.post('http://52.18.248.248:3001/register', {
        username: this.state.Username,
        emailAddress: this.state.EmailAddress,
        password: this.state.Password
      })
      .then(function (response) {
        response.data.registered == "true" ? outer.Login() : outer.IncorrectCredentialsEntered()
      })
      .catch(function (error) {
        console.log(error);
      });

    }
  }

  IncorrectCredentialsEntered() {
    this.setState({UsernameErrorText: "Username already exists"})
  }


    render() {
        return (
            <div className="ForgottenPassword111">
              <div className="ForgottenPasswordText"> {this.props.Language === 'English' ? 'Register' : this.props.Language === 'Spanish' ? 'Registro' : 'Rejestracja'}

              <div style={{textAlign: 'center', overflow: 'hidden'}}>
                <TextField
                  floatingLabelText={this.props.Language === 'English' ? 'Username' : this.props.Language === 'Spanish' ? 'Nombre de usuario' : 'Uzytkownik'       }
                  value={this.state.Username}
                  floatingLabelStyle={{color: 'grey'}}
                  errorText={(this.state.Username.length > 9) ? this.props.Language === 'English' ? 'This field cannot have more then 9 characters' : this.props.Language === 'Spanish' ? 'Este campo no puede contener más de 9 caracteres' : 'Nazwa azytkownika nie moze miec wiecej niz 9 liter!' : this.state.UsernameErrorText}
                  onChange={e => this.setState({ Username: e.target.value, UsernameErrorText: "" }) }
                />

                <br/>

                <TextField
                  floatingLabelText="Email Address"
                  value={this.state.EmailAddress}
                  errorText={this.state.EmailAddressErrorText}
                  floatingLabelStyle={{color: 'grey'}}
                  onChange={e => this.setState({ EmailAddress: e.target.value, EmailAddressErrorText: "" }) }
                />

                <br/>

                <TextField
                  floatingLabelText={this.props.Language === 'English' ? 'Password' : this.props.Language === 'Spanish' ? 'Contraseña' : 'Haslo'}
                  type='password'
                  floatingLabelStyle={{color: 'grey'}}
                  value={this.state.Password}
                  errorText={(this.state.Password.length > 25) ? (this.state.Password.length > 25) ? this.props.Language === 'English' ? "This field cannot have more then 25 characters" : this.props.Language === 'Spanish' ? 'Este campo no puede contener más de 25 caracteres' : 'Haslo nie moze miec wiecej niz 25 liter!' : this.state.PasswordErrorText : this.state.PasswordErrorText}
                  onChange={e => this.setState({ Password: e.target.value, PasswordErrorText: "" }) }
                />
              </div>

              <br/>

</div>

  <br/>
              <div className="RegisterButton">
                <RaisedButton
                  label={this.props.Language === 'English' ? 'Register' : this.props.Language === 'Spanish' ? 'Registrar' : 'Zarejestruj'}
                  backgroundColor="navy"
                    floatingLabelStyle={{color: 'white'}}
                  labelColor="white"
                  style={{alignitems: 'center'}}
                  onClick={this.CreateAnAccount}
                  disabled={( (this.state.Username.length > 9) ||  (this.state.Password.length > 25) ? true : false)}
                />
              </div>

            </div>
        );
    }
}
