
import React from 'react'

const divStyle = {
  activeStyle: {
    textAlign: 'center',
    color: 'white'
  },
  inactiveStyle: {
    display: 'none'
  }
}

export default class Bottom extends React.Component {
    render() {
        return (
            <div className="Bottom" style={{textAlign: 'center'}}>
              <table className="BottomTable">
                <tr>
                  <td> <div className="floatLeft" style={this.props.ActiveMiddleTab.ForgotPasswordActive === true ? divStyle.inactiveStyle : null} onClick={this.props.setActiveTab.bind(null, 'ForgotPasswordActive')}> {this.props.Language === 'English' ? 'Retrieve password' : this.props.Language === 'Spanish' ? 'Recuperar Contraseña' : 'Odzyskaj haslo'       } </div> </td>
                  <td> <div className="floatLeft" style={this.props.ActiveMiddleTab.CreateAnAccountActive === true ? divStyle.inactiveStyle : null} onClick={this.props.setActiveTab.bind(null, 'CreateAnAccountActive')}> {this.props.Language === 'English' ? 'Register' : this.props.Language === 'Spanish' ? 'Registro' : 'Rejestracja'       } </div> </td>
                  <td> <div className="floatLeft" style={this.props.ActiveMiddleTab.LoginActive === true ? divStyle.inactiveStyle : null} onClick={this.props.setActiveTab.bind(null, 'LoginActive')}> {this.props.Language === 'English' ? 'Sign-in' : this.props.Language === 'Spanish' ? 'Iniciar sesión' : 'Zaloguj'       } </div> </td>
                </tr>
              </table>
            </div>
        );
    }
}
