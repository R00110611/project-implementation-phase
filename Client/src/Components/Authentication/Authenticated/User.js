import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import {fullWhite} from 'material-ui/styles/colors';
import LoginIcon from 'material-ui/svg-icons/action/lock';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const styles = {
  labelStyle1: {
    color: 'red'
  },
  underlineStyle: {
  borderColor: orange500
},
  inputStyle: {
    color: blue500
  },
  contentStyle: {
    color: blue500,
    textAlign: "center"
  },
  titleStyle: {
    color: blue500,
    backgroundColor: orange500
  },
  dialogStyle: {
    height: '1'
  },
  dialogContentStyle: {
    textAlign: "center"
  }
}

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      height: null,
      weight: null,
      DateOfBirth: null,
      FirstName: null,
      LastName: null,
      Gender: null,
      GenderValue: null,
      TelephoneNumber: null
    }
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.updateUserProperties = this.updateUserProperties.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.handleGenderChange = this.handleGenderChange.bind(this);
    this.getGender = this.getGender.bind(this);
    this.isFloat = this.isFloat.bind(this);
    this.isNumeric = this.isNumeric.bind(this);
}
// http://stackoverflow.com/questions/175739/is-there-a-built-in-way-in-javascript-to-check-if-a-string-is-a-valid-number
isNumeric(value) {
    return /^\d+$/.test(value);
}

isFloat(e) {
  if(isNaN(e.target.value) === false) {
    return true;
  } else {
    return false;
  }
}


handleGenderChange(event, index, val) {
  val === 1 ? this.setState({GenderValue: 1, Gender: 'Male'}) : null;
  val === 2 ? this.setState({GenderValue: 2, Gender: 'Female'}) : null;
}

handleChange = (event, date) => {
  this.setState({
    DateOfBirth: date,
  });
};


  getGender(gender) {
    console.log(gender);
    if(gender === 'Male') {
      return 1;
    }
    if(gender === 'Female') {
      return 2;
    }
  }

  handleOpen = () => {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/getUserProperties', {
        username: this.props.LoggedInUser
      })
      .then(function (response) {
        console.log(response.data);



        outer.setState({
          height: response.data.response.Height,
          weight: response.data.response.Weight,
          FirstName: response.data.response.FirstName,
          LastName: response.data.response.LastName,
          DateOfBirth: response.data.response.DateOfBirth != null || response.data.response.DateOfBirth === undefined ? new Date(response.data.response.DateOfBirth) : null,
          Gender: response.data.response.Gender,
          GenderValue: outer.getGender(response.data.response.Gender),
          TelephoneNumber: response.data.response.TelephoneNumber
        });

        outer.setState({open: true});
      })
      .catch(function (error) {
        console.log(error);
      });

  };

  handleClose = () => {
    this.setState({open: false});
  };

  formatDate(date){
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

  updateUserProperties() {
    console.log(this.state);
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/updateUserProperties', {
        username: this.props.LoggedInUser,
        height: this.state.height,
        weight: this.state.weight,
        DateOfBirth: this.state.DateOfBirth != null || this.state.DateOfBirth === undefined ? this.formatDate(this.state.DateOfBirth).toString() : null,
        FirstName: this.state.FirstName,
        LastName: this.state.LastName,
        Gender: this.state.Gender,
        TelephoneNumber: this.state.TelephoneNumber
      })
      .then(function (response) {
        console.log(response);

        outer.handleClose();
      })
      .catch(function (error) {
        console.log(error);
      });


      this.setState({height: ""});
      this.setState({weight: ""});
      this.setState({DateOfBirth: ""});
      this.setState({FirstName: ""});
      this.setState({LastName: ""});
      this.setState({Gender: null});
      this.setState({TelephoneNumber: null});
  }

    render() {
      const actions = [
            <FlatButton
              label="Cancel"
              primary={true}
              onClick={this.handleClose}
            />,
            <FlatButton
              label="Submit"
              primary={true}
              onClick={this.updateUserProperties}
            />,
          ];

        return (
            <div>
              <span className="username" onClick={this.handleOpen}> {this.props.LoggedInUser} </span>

              <Dialog
                        title={this.props.Language === 'English' ? 'Update user properties' : this.props.Language === 'Spanish' ? 'Actualizar las propiedades del usuario' : 'Zaaktualizuj informacje o uzytkowniku'       }
                        actions={actions}
                        modal={false}
                        open={this.state.open}
                        onRequestClose={this.handleClose}
                        autoScrollBodyContent={true}
                        contentStyle={styles.contentStyle}
                        titleStyle={styles.titleStyle}
                        style={styles.dialogStyle}
                        contentStyle={styles.dialogContentStyle}
                        >

                      <br/>

                      <DatePicker
                        hintText={this.props.Language === 'English' ? 'Date of Birth' : this.props.Language === 'Spanish' ? 'Fecha de nacimiento' : 'Data urodzenia'       }
                        value={this.state.DateOfBirth}
                        floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                        floatingLabelShrinkStyle={{textAlign: 'center'}}
                        hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                        style={{textAlign: 'center', color: 'white'}}
                        inputStyle={{textAlign: 'center'}}
                        textFieldStyle={{textAlign: 'center'}}
                          underlineStyle={{borderColor: orange500}}
                        floatingLabelFocusStyle={{textAlign: 'center'}}
                        floatingLabelText="Starting Date"
                        textFieldStyle={{cursor: 'pointer', color: 'white'}}
                        onChange={this.handleChange}
                        maxDate={new Date()}
                        floatingLabelText={this.props.Language === 'English' ? 'Date of Birth' : this.props.Language === 'Spanish' ? 'Fecha de nacimiento' : 'Data urodzenia'       }
                      />

                      <br/>

                      <TextField
                      underlineStyle={{borderColor: orange500}}
                      floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                      floatingLabelShrinkStyle={{textAlign: 'center'}}
                      hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                      style={{textAlign: 'center', color: 'white'}}
                      inputStyle={{textAlign: 'center'}}
                      textFieldStyle={{textAlign: 'center'}}
                      floatingLabelFocusStyle={{textAlign: 'center'}}
                        floatingLabelText={this.props.Language === 'English' ? 'Height in CM' : this.props.Language === 'Spanish' ? 'Altura en CM' : 'Wzrost w CM'       }
                        value={this.state.height}
                        onChange={e => e.target.value.length < 6 && this.isFloat(e) === true ? this.setState({ height: e.target.value }) : null }
                      />

                      <br/>

                      <TextField
                      underlineStyle={{borderColor: orange500}}
                      floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                      floatingLabelShrinkStyle={{textAlign: 'center'}}
                      hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                      style={{textAlign: 'center', color: 'white'}}
                      inputStyle={{textAlign: 'center'}}
                      textFieldStyle={{textAlign: 'center'}}
                      floatingLabelFocusStyle={{textAlign: 'center'}}
                        floatingLabelText={this.props.Language === 'English' ? 'Weight in KG' : this.props.Language === 'Spanish' ? 'Peso en KG' : 'Waga w KG'       }
                        value={this.state.weight}
                        onChange={e => e.target.value.length < 6 && this.isFloat(e) === true ? this.setState({ weight: e.target.value }) : null }
                      />

                      <br/>

                      <TextField

                        floatingLabelText={this.props.Language === 'English' ? 'First Name' : this.props.Language === 'Spanish' ? 'Nombre de pila' : 'Imie'       }
                        underlineStyle={{borderColor: orange500}}
                        floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                        floatingLabelShrinkStyle={{textAlign: 'center'}}
                        hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                        style={{textAlign: 'center', color: 'white'}}
                        inputStyle={{textAlign: 'center'}}
                        textFieldStyle={{textAlign: 'center'}}
                        floatingLabelFocusStyle={{textAlign: 'center'}}
                        value={this.state.FirstName}
                        onChange={e => e.target.value.length < 35 ? this.setState({ FirstName: e.target.value }) : null }
                      />

                      <br/>

                      <TextField
                      underlineStyle={{borderColor: orange500}}
                      floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                      floatingLabelShrinkStyle={{textAlign: 'center'}}
                      hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                      style={{textAlign: 'center', color: 'white'}}
                      inputStyle={{textAlign: 'center'}}
                      textFieldStyle={{textAlign: 'center'}}
                      floatingLabelFocusStyle={{textAlign: 'center'}}
                        floatingLabelText={this.props.Language === 'English' ? 'Last Name' : this.props.Language === 'Spanish' ? 'Apellido' : 'Nazwisko'       }
                        value={this.state.LastName}
                        onChange={e => e.target.value.length < 35 ? this.setState({ LastName: e.target.value }) : null }
                      />

                      <br/>

                      <div style={{display: 'inline'}}>
                      <SelectField
                        floatingLabelText={this.props.Language === 'English' ? 'Gender' : this.props.Language === 'Spanish' ? 'Género' : 'Plec'       }
                        value={this.state.GenderValue}
                        onChange={this.handleGenderChange}
                        underlineStyle={{borderColor: orange500}}
                        floatingLabelStyle={{ color: 'grey'}}
                      >
                        <MenuItem value={1} primaryText="Male" />
                        <MenuItem value={2} primaryText="Female" />

                      </SelectField>
                      </div>


                      <br/>


                      <TextField
                      underlineStyle={{borderColor: orange500}}
                      floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                      floatingLabelShrinkStyle={{textAlign: 'center'}}
                      hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                      style={{textAlign: 'center', color: 'white'}}
                      inputStyle={{textAlign: 'center'}}
                      textFieldStyle={{textAlign: 'center'}}
                      floatingLabelFocusStyle={{textAlign: 'center'}}
                      floatingLabelText={this.props.Language === 'English' ? 'Telephone Number' : this.props.Language === 'Spanish' ? 'Número de teléfono' : 'Numer telefonu'       }
                      value={this.state.TelephoneNumber}
                      onChange={e => e.target.value.length < 12 && this.isNumeric(e.target.value) === true ? this.setState({ TelephoneNumber: e.target.value }) : null }
                      />


              </Dialog>
            </div>
        );
    }
}
