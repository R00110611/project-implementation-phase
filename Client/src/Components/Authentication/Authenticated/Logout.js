import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import {fullWhite} from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors';
import ExitIcon from 'material-ui/svg-icons/action/exit-to-app';

export default class Logout extends React.Component {
    render() {
        return (
            <div>
                <RaisedButton
                  label={this.props.Language === 'English' ? 'Sign-out' : this.props.Language === 'Spanish' ? 'Desconectar' : 'Wyloguj'       }
                  onClick={this.props.setAuthenticated.bind(null, false)}
                  backgroundColor="white"
                  className="ObjectTwo"
                  labelColor="navy"
                  icon={<ExitIcon color={"navy"} />}  >
                </RaisedButton>

            </div>
        );
    }
}
