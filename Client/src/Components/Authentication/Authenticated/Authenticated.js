import React from 'react'
import Logout from './Logout'
import User from './User'
import Langauge from '../Language'

export default class Authenticated extends React.Component {
    render() {
        return (
            <div className="Authentication">
                <ul className="AuthenticationBanner">
                  <li>
                    <Logout Language={this.props.Language} setAuthenticated={this.props.setAuthenticated} />
                  </li>

                  <li>
                    <User Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} />
                  </li>

                  <li>
                    <div className="LanguageUnauthenticated"> <Langauge active={this.props.authenticated === true ? 1 : 2} Language={this.props.Language} setLanguage={this.props.setLanguage} LanguageValue={this.props.LanguageValue} /> </div>
                  </li>

                </ul>

            </div>
        );
    }
}
