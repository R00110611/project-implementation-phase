import React from 'react'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class Language extends React.Component {
    render() {
        return (
            <div className="Language">
            <div className="lang" style={this.props.active === 2 ? divStyle.inactiveStyle : null}>
                <select onChange={this.props.setLanguage} >
                  <option value="English" selected={this.props.Language==="English" ? true : false} > English </option>
                  <option value="Polish" selected={this.props.Language==="Polish" ? true : false}  > Polski </option>
                  <option value="Spanish" selected={this.props.Language==="Spanish" ? true : false}  > Español </option>
                </select>
              </div>
            </div>
        );
    }
}
