
import React from 'react'
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import CalculateDietEffect from './CalculateDietEffect'
import ActiveDiet from './ActiveDiet'
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';
import Favourite from 'material-ui/svg-icons/action/grade';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import See from 'material-ui/svg-icons/action/zoom-in'
import Add from 'material-ui/svg-icons/content/add';

const styles = {
  activeRow: {
    backgroundColor: 'white'
  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  }
}

const divStyles = {
  activeDiv: {
    display: 'none'
  },
  inactiveDiv: {

  },
  inactiveDiv1: {
    display: 'none'
  },
  activeDiv1: {
    color: 'navy',
    textAlign: 'center',
    fontSize: '190%'
  }
}

export default class DietSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      DietList: [],
      DietName: "",
      IncludedIngredients: [],
      ExcludedIngredients: [],
      onlyMineDiets: false,
      selected: "IncludedIngredients",
      IngredientName: "",
      ActiveDiet: -1,
      ActiveDietObj: {},
      onlyFavouriteDiets: false,
      DietReviews: [],
      activeRecipee: -1,
      initialSearchMade: false
    }

    this.searchDiets = this.searchDiets.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.check = this.check.bind(this);
    this.handleRowClick = this.handleRowClick.bind(this);
    this.check1 = this.check1.bind(this);
    this.getReviews = this.getReviews.bind(this);
    this.setActiveRecipee = this.setActiveRecipee.bind(this);
    this.setInitialSearch = this.setInitialSearch.bind(this);
    this.goBack = this.goBack.bind(this);
  }



  goBack() {
    this.setState({ActiveDiet: -1});
  }

setInitialSearch() {
  this.setState({initialSearchMade: true});
}

  setActiveRecipee(RecipeeID) {
    this.setState({activeRecipee: RecipeeID});
  }

  getReviews(Diet) {
    var outer = this;
    var axios = require('axios');


    axios.post('http://52.18.248.248:3001/DietReviews', {
        DietID: Diet
      })
      .then(function (response) {

        var arr = [];
        response.data.response.map(function(name, index) {
             arr.push({
               ID: name.ID,
               DietID: name.DietID,
               Username: name.Username,
               UsernameID: name.UsernameID,
               Review: name.Review,
               Recommended: name.Recommended,
               TimeAndDate: name.TimeAndDate

             });
        })

        outer.setState({DietReviews: arr});
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  check1(e, isc) {
    this.setState({onlyFavouriteDiets: isc}, this.searchDiets);
  }

  addIngredient(e, i, v) {
    var incIngr = this.state.IncludedIngredients;
    var exclIngr = this.state.ExcludedIngredients;

    this.state.selected === "IncludedIngredients" ? incIngr.push({Name: this.state.IngredientName}) : exclIngr.push({Name: this.state.IngredientName});
    this.state.selected === "IncludedIngredients" ? this.setState({IncludedIngredients: incIngr}, this.searchDiets) : this.setState({ExcludedIngredients: exclIngr}, this.searchDiets);

  }

  searchDiets() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForDiets', {
      DietName: this.state.DietName,
      OnlyMineDiets: this.state.onlyMineDiets,
      username: this.props.LoggedInUser,
      onlyFavouriteDiets: this.state.onlyFavouriteDiets
      })
      .then(function (response) {
        outer.setState({DietList: response.data.response, initialSearchMade: true});
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  check(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

  //  console.log(value, name);

    if(name === "Mine") {
      this.setState({onlyMineDiets: value}, this.searchDiets);
    }

    if(name === "Favourite") {
      this.setState({onlyFavouriteDiets: value}, this.searchDiets);
    }

    //name === "Mine" ? value === true ? this.setState({onlyMineRecipees: true}, this.SearchRecipee) : this.setState({onlyMineRecipees: false}, this.SearchRecipee) : value === true ? this.setState({onlyFavouriteRecipees: true}, this.SearchRecipee) : this.setState({onlyFavouriteRecipees: false}, this.SearchRecipee)
  }

  handleRowClick(DietID) {
    this.getReviews(DietID.DietID);
    this.setState({ActiveDiet: DietID.DietID, ActiveDietObj: DietID});
  }

  isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
  }

    render() {
        return (
            <div>

                                <div className="DietSearchOptions" style={this.state.ActiveDiet != -1 ? divStyles.activeDiv : divStyles.inactiveDiv}>
                                <br/>
                                    <input onChange={e => e.target.value.length < 35 ? this.setState({ DietName: e.target.value }, this.searchDiets) : null } type="text" value={this.state.DietName} placeholder={this.props.Language === 'English' ? 'Diet Name' : this.props.Language === 'Spanish' ? 'Nombre' : 'Nazwa'       } style={{textAlign: 'center'}} />
                                    <br/>

                                    <br/> <br/>


                                    <div>
                                          Only mine diets:
                                          <input
                                            name="Mine"
                                            type="checkbox"
                                            checked={this.state.onlyMineDiets}
                                            onChange={this.check}
                                          />

                                          <br/>

                                          Only mine favourite diets:
                                          <input
                                            name="Favourite"
                                            type="checkbox"
                                            checked={this.state.onlyFavouriteDiets}
                                            onChange={this.check}
                                          />
                                      </div>



                                      <br/>
                                  </div>




                                  <div className="dietsFound" style={this.state.initialSearchMade === true && this.state.ActiveDiet == -1 ? divStyle.activeStyle : divStyle.inactiveStyle} >
                                          <table style={{width: '100%'}}>

                                          <th> </th>
                                          <th> Diet Name </th>
                                          <th> Diet weight effect </th>

                                          {this.state.DietList.map(DietID =>

                                            <tr>
                                              <td style={{textAlign: 'center' }} > <Add onClick={this.props.addDietToDietingJourney.bind(null, DietID)} style={{cursor: 'pointer', color: 'white'}}/> </td>
                                              <td style={{textAlign: 'center'}}> {DietID.DietName} </td>
                                              <td style={{textAlign: 'center'}}> <CalculateDietEffect height={this.props.height} weight={this.props.weight} DateOfBirth={this.props.DateOfBirth} Gender={this.props.Gender} Diet={DietID} /> </td>
                                              <td style={{textAlign: 'center'}}>  <See onClick={this.handleRowClick.bind(null, DietID)} style={{cursor: 'pointer', color: 'white'}}/> </td>
                                            </tr>



                                          )}
                                          </table>
                                  </div>



                                  <div className="activeDiet" style={this.state.ActiveDiet != -1 ? divStyle.activeStyle : divStyle.inactiveStyle}>

                                    { this.state.ActiveDiet !=-1 ?
                                         <ActiveDiet
                                         ActiveDietObj={this.state.ActiveDietObj}
                                         activeDietID={this.state.ActiveDiet}
                                         LoggedInUser={this.props.LoggedInUser}
                                         Reviews={this.state.DietReviews}
                                         rowClick={this.handleRowClick}
                                         setAct={this.setActiveRecipee}
                                         goBack={this.goBack}
                                         Language={this.props.Language}
                                         />
                                     :
                                     'select diet to see it here'
                                   }

                                     <br/>
                                </div>











            </div>
        );
    }
}
