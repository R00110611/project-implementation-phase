
import React from 'react'
import DatePicker from 'material-ui/DatePicker';
import TextField from 'material-ui/TextField';
import DietSearch from './DietSearch'
import CalculateDietEffect from './CalculateDietEffect'
import RaisedButton from 'material-ui/RaisedButton';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import DownArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';

const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}

const divStyles = {
  activeStyle: {
    backgroundColor: '#525963',
  },
  inactiveStyle: {
    display: 'none'
  },
  hoverStyle: {
    cursor: 'pointer'
  },
  inhoverStyle: {

  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {

  }
}

export default class DietJourneyCreation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      StartingDate: null,
      DietingJourneyName: "",
      DietList: [],
      hoverActive: -1,
      SelectDietActive: false,
      EnterDetailsActive: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.addDietToDietingJourney = this.addDietToDietingJourney.bind(this);
    this.getTotalEffect = this.getTotalEffect.bind(this);
    this.getEffect = this.getEffect.bind(this);
    this.createDietingJourney = this.createDietingJourney.bind(this);
    this.removeDietFromDietingJourney = this.removeDietFromDietingJourney.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onHover = this.onHover.bind(this);
    this.initialize = this.initialize.bind(this);
  }

  initialize() {
    this.setState({
      StartingDate: null,
      DietingJourneyName: "",
      DietList: [],
      hoverActive: -1,
      SelectDietActive: false,
      EnterDetailsActive: false
    })
  }



onHover(activeNr) {
    this.setState({hoverActive: activeNr});
}


onClick(tabToActivate) {
  if(tabToActivate === 1) {
    if(this.state.SelectDietActive === true) {
      this.setState({SelectDietActive: false});
    } else {
      this.setState({EnterDetailsActive: false, SelectDietActive: true});
    }
  }

    if(tabToActivate == 2) {
      if(this.state.EnterDetailsActive === true) {
        this.setState({EnterDetailsActive: false});
      } else {
        this.setState({SelectDietActive: false, EnterDetailsActive: true});
      }
  }
}

createDietingJourney() {
  toastr.success('Created dieting journey!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

  var uniqueDays = [];

  for(var i =0; i<this.state.DietList[0].DietDays.length; i++) {
    uniqueDays.push(this.state.DietList[0].DietDays[i].Day);
  }

  var _ = require('lodash');
  uniqueDays = _.uniq(uniqueDays);


  var outer = this;
  var axios = require('axios');

  axios.post('http://52.18.248.248:3001/CreateDietingJourney', {
    DietingJourneyCreator: this.props.LoggedInUser,
    DietingJourneyName: this.state.DietingJourneyName,
    StartingDate: this.state.StartingDate,
    DietID: this.state.DietList[0].DietID,
    DietDays: uniqueDays.length
    })
    .then(function (response) {
      outer.initialize();
    })
    .catch(function (error) {
      console.log(error);
    });



}

getEffect(DietObj) {
  var year1 =  this.props.DateOfBirth.getFullYear();
  var year2 = new Date().getFullYear();

  var age = year2 - year1;
  var BMR = 0;


  if(this.props.Gender == "Male") {
    BMR = 66 + (13.7 * this.props.weight) + (5 * this.props.height) - (6.8 * age);
  }
  if(this.props.Gender == "Female") {
    BMR = 655 + (9.6 * this.props.weight) + (1.8 * this.props.height) - (4.7 * age);
  }


  var totalCalories = 0;

  for(var i=0; i<DietObj.DietDays.length; i++) {
    for(var j=0; j<DietObj.DietDays[i].Meals.length; j++) {
      for(var m=0; m<DietObj.DietDays[i].Meals[j].Recipee.RecipeeIngredients.length; m++) {
        totalCalories = totalCalories + DietObj.DietDays[i].Meals[j].Recipee.RecipeeIngredients[m].Calories;
      }
    }
  }


  var daysInDiet = DietObj.DietDays.length;

  var totalBMR = BMR * daysInDiet;

  var effect = totalCalories - totalBMR;



  var weightChange = 0;


  if(effect < 0) {
    weightChange = weightChange + (effect)/7000;

  }
  if(effect > 0) {
    weightChange = weightChange + (effect)/7000;
  } else if(effect == 0){
  }

  return weightChange;
}



  getTotalEffect() {
    var totalWeightChange = 0;

    for(var i = 0; i<this.state.DietList.length; i++ ) {
      totalWeightChange=totalWeightChange + this.getEffect(this.state.DietList[i]);
    }

    var weightChange = "";


    if(totalWeightChange < 0) {
      weightChange = "LOST: " + totalWeightChange + " KG";

    }
    if(totalWeightChange > 0) {
      weightChange = "GAINED: " + totalWeightChange  + " KG";
    } else if(totalWeightChange == 0){
    }

    return weightChange;
  }

  addDietToDietingJourney(Diet) {
    var arr = this.state.DietList;
    if(arr.length < 1)  {

    var existsAlready = false;

    arr.map(Diet1 => {
      if(Diet.DietID === Diet1.DietID)
        existsAlready = true;
    });

    if (existsAlready === false) {
      arr.push(Diet);
      this.setState({DietList: arr});

    }

    if(existsAlready === true) {

    }

  }

  }

removeDietFromDietingJourney() {
  var _ = require('lodash');
  var arr = this.state.DietList;
  arr.splice(0, 1);
  this.setState({DietList: arr});
}


handleChange = (event, date) => {
  this.setState({
    StartingDate: date,
  });
};



    render() {
        return (
            <div>

        <div className="DietCreationParent">
        <br/>
            <div className="DescriptionIC1">
              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 1)} style={this.state.hoverActive === 1 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 1) }>
                <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 1. Select diet for the dieting journey
                  <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.SelectDietActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                </span>
                </div>

                <div className="testinh" style={this.state.SelectDietActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >
                  <div>
                    <DietSearch Language={this.props.Language} addDietToDietingJourney={this.addDietToDietingJourney} height={this.props.height} weight={this.props.weight} DateOfBirth={this.props.DateOfBirth} Gender={this.props.Gender} LoggedInUser={this.props.LoggedInUser}  />

                    <br/>

                    {this.state.DietList.length > 0 ? <div className="dietSelectiedDJ"> <p> <span style={{fontWeight: 'bold'}}> Diet Selected: </span>  {this.state.DietList[0].DietName}  <p> <DeleteIcon style={{cursor: 'pointer'}} onClick={this.removeDietFromDietingJourney} /> </p> </p> </div> : null }


                    <br/>
                  </div>


                </div>
              </div>

              <br/>

              <div className="DescriptionIC1">
                <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 2)} style={this.state.hoverActive === 2 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 2) }>
                  <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 2. Enter dieting journey details
                    <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.EnterDetailsActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                  </span>
                  </div>

                  <div className="testinh" style={this.state.EnterDetailsActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >

                                  <div className="DietSearchOptions" style={{textAlign: 'center'}}>
                                    <br/>
                                    <input onChange={e => this.setState({ DietingJourneyName: e.target.value })} type="text" value={this.state.DietingJourneyName} placeholder={this.props.Language === 'English' ? 'Name *' : this.props.Language === 'Spanish' ? 'Nombre *' : 'Nazwa *'       } style={{textAlign: 'center'}} />
                                    <br/>
                                    <span style={this.state.DietingJourneyName.length > 50 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.DietingJourneyName.length}/50 </span>




                                      <DatePicker
                                        hintText="Starting Date"
                                        value={this.state.StartingDate}
                                        onChange={this.handleChange}
                                        floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                                        floatingLabelShrinkStyle={{textAlign: 'center'}}
                                        hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                                        style={{textAlign: 'center', color: 'white'}}
                                        inputStyle={{textAlign: 'center'}}
                                        textFieldStyle={{textAlign: 'center'}}
                                        floatingLabelFocusStyle={{textAlign: 'center'}}
                                        floatingLabelText="Starting Date"
                                        errorText={this.state.StartingDate === null ? "Cannot be empty" :  ""}
                                        textFieldStyle={{cursor: 'pointer', color: 'white'}}
                                      />
                                      <br/>
                                  </div>
                                  <br/>
                  </div>
                </div>

                <br/>

                <div>
                <RaisedButton label="Create"
                   onClick={this.createDietingJourney}
                   backgroundColor="navy"
                   labelColor="white"
                   style={{width: '60%', marginLeft:'20%',marginRight:'20%'}}
                   disabled={this.state.StartingDate === null || this.state.DietingJourneyName.length == 0 || this.state.DietList.length == 0 ? true : false }
                 >
             </RaisedButton>
             </div>
                          <br/>
        </div>







            </div>
        );
    }
}
