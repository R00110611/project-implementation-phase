
import React from 'react'
import ActiveRecipee from './ActiveRecipee'

const styles = {
  activeTD: {
    backgroundColor: 'navy',
    borderStyle: 'solid',
    borderColor: '#3c5b05',
    color: 'white',
    cursor: 'pointer'
  },
  inactiveTD: {
    backgroundColor: 'red',
    borderStyle: 'solid',
    borderColor: 'yellow'
  }
}

const divStyles = {
  activeStyle: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle: {
    cursor: 'pointer'
  },
  activeDiv: {

  },
  inactiveDiv: {
    display: 'none'
  }
}




export default class ActiveDiet extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeDay: -1,
      activeTime: "",
      activeRecipee: -1,
      activeMeal: {},
      DietSummaryActive: true,
      DietReviewsActive: false
    }

    this.onHover = this.onHover.bind(this);
    this.setActiveRecipee = this.setActiveRecipee.bind(this);
    this.setActiveRecipees = this.setActiveRecipees.bind(this);
    this.setActive = this.setActive.bind(this);
    this.goBack = this.goBack.bind(this);
  }


  goBack() {
    this.setState({activeRecipee: -1});
  }

  setActive(tab) {

    if(tab === 1) {
      this.setState({DietSummaryActive: true, DietReviewsActive: false});
    } else {
      this.setState({DietReviewsActive: true, DietSummaryActive: false});
    }
  }

  onHover(day, timee) {
    this.setState({activeDay: day, activeTime: timee})
  }

  setActiveRecipee(RecipeeID, Meal) {
    this.setState({activeRecipee: RecipeeID, activeMeal: Meal});
    this.props.setAct(RecipeeID);
  }

  setActiveRecipees(RecipeeID) {
    this.setState({activeRecipee: RecipeeID});
    this.props.setAct(RecipeeID);



  }

    render() {
        return (
            <div>
            <br/>

            <h2 style={{textAlign: 'center'}}> <span style={{cursor:'pointer', fontWeight: 'bold', color:'black', textAlign: 'center'}} onClick={this.props.goBack}   > Go back to Diet Search </span> </h2>

            <div className="Navigation">
              <br/>


              <div style={this.state.DietSummaryActive === true ? divStyles.activeDiv : divStyles.inactiveDiv}>
                <div className="IngredientDescription1">
                  <h2> Diet Description </h2>

                  <div className="InsidesDiet">
                    <span style={{fontWeight:'bold'}} > Diet name </span> {this.props.ActiveDietObj.DietName}
                    <br/>
                    <span style={{fontWeight:'bold'}} > Creator of the Diet </span> {this.props.ActiveDietObj.DietCreator}
                  </div>

                  <br/>
                </div>
                <br/>


                <div className="IngredientDescription1" style={this.state.activeRecipee === -1 ? divStyles.activeDiv : divStyles.inactiveDiv}>
                  <h2> Diet Nutrition </h2>

                  <div style={{width: '80%', marginLeft:'10%', marginRight: '10%'}}>
                          <table className="CompareTable">
                        {this.props.ActiveDietObj.RealDays.map(DietDay =>
                          <tr> <td style={{backgroundColor: '#546582'}}> Day: {DietDay.Day} </td>
                          <td style={{backgroundColor: 'white', borderStyle: 'solid', borderColor: '#055b21'}} >
                          {DietDay.Meals.map(Meal =>
                            <td onClick={this.setActiveRecipee.bind(this, Meal.RecipeeID, Meal)} onMouseOut={this.onHover.bind(this, -1, "XX:XX:XX")} onMouseOver={this.onHover.bind(this, DietDay.Day, Meal.MealTime)} style={ this.state.activeTime === Meal.MealTime && this.state.activeDay === DietDay.Day ? styles.activeTD : styles.inactiveTD  }  >
                            <td> {Meal.MealTime}
                            <td>
                              {Meal.RecipeeName}
                            </td>
                            </td>
                            </td>
                          )}</td>

                          </tr>
                        )}
                        </table>
                </div>


                  <br/>
                </div>


                                <div className="IngredientDescription1" style={this.state.activeRecipee != -1 ? divStyles.activeDiv : divStyles.inactiveDiv}>
                                  {this.state.activeRecipee != -1 ? <ActiveRecipee goBack1={this.goBack} ActiveRecipee={this.state.activeMeal} setActiveRecipees={this.setActiveRecipees} /> : <null />}

                                </div>


                <br/>
              </div>





            </div>



            </div>
        );
    }
}
