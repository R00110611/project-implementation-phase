
import React from 'react'

const styles = {
  activeStyle: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle: {
    cursor: 'pointer'
  }
}

const divStyles = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  }

}

export default class ActiveRecipee extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      RecipeeInformationActive: true,
      ReviewsActive: false
    }

    this.activate = this.activate.bind(this);
  }

  activate(num) {
    if(num === 1) {
      this.setState({RecipeeInformationActive: true, ReviewsActive: false });
    } else {
      this.setState({RecipeeInformationActive: false, ReviewsActive: true });
    }
  }

    render() {
        return (
            <div>
              <h2 style={{textAlign: 'center'}}> <span style={{cursor:'pointer', fontWeight: 'bold', color:'black', textAlign: 'center'}} onClick={this.props.goBack1}> Go back to Diet overview </span> </h2>


              <div>
                    <br/>
                                  <h1> Recipee Information </h1>
                    <div style={this.state.RecipeeInformationActive === true ? divStyles.activeStyle : divStyles.inactiveStyle} >
                      <h2> Recipee Description </h2>
                        <div className="DescriptionInside">
                            {this.props.ActiveRecipee.RecipeePhoto === null || this.props.ActiveRecipee.RecipeePhoto === undefined  ? <img src={"http://52.18.248.248:3001/RecipeeImages/DefaultRecipeePhoto.png"} height={90} width={90} /> : <img src={"http://52.18.248.248:3001/" + this.props.ActiveRecipee.RecipeePhoto} height={90} width={90} />}
                            <br/>
                            <span style={{fontWeight: 'bold'}}> Recipee Name </span> {this.props.ActiveRecipee.RecipeeName}
                            <br/>
                            <span style={{fontWeight: 'bold'}}> Preparation Method </span> {this.props.ActiveRecipee.PreparationMethod}
                            <br/>
                        </div>
                    </div>

                    <br/>

                    <div className="DescriptionInside" style={this.state.RecipeeInformationActive === true ? divStyles.activeStyle : divStyles.inactiveStyle}>
                      <h2> Recipee Ingredients </h2>
                        <div className="DescriptionInside">
                        <table style={{width: '100%'}}>
                                        <tr style={{backgroundColor: '#21262d', color: 'white'}}>
                                          <th> {this.props.Language === 'English' ? 'Ingredient Name' : this.props.Language === 'Spanish' ? 'Nombre del ingrediente' : 'Nazwa składnika'       } </th>
                                          <th> Photo </th>
                                          <th> {this.props.Language === 'English' ? 'Amount' : this.props.Language === 'Spanish' ? 'Cantidad' : 'Ilosc'       }</th>
                                          <th> {this.props.Language === 'English' ? 'Calories' : this.props.Language === 'Spanish' ? 'Calorías' : 'Kalorie'       } </th>
                                          <th> {this.props.Language === 'English' ? 'Vitamins' : this.props.Language === 'Spanish' ? 'Vitaminas' : 'Witaminy'       }</th>
                                          <th>{this.props.Language === 'English' ? 'Minerals' : this.props.Language === 'Spanish' ? 'Minerales' : 'Minerały'       }</th>
                                          <th> Fats</th>
                                          <th> Carbohydrates</th>
                                          <th> Other</th>
                                        </tr>

                                          {this.props.ActiveRecipee.Recipee.RecipeeIngredients != null || this.props.ActiveRecipee.Recipee.RecipeeIngredients != undefined ?
                                             this.props.ActiveRecipee.Recipee.RecipeeIngredients.map(Ingredient =>
                                              <tr style={{backgroundColor: '#404b5e'}}>
                                              <td> {Ingredient.Name} </td>
                                              <td> {Ingredient.Photo === null ? <img src={"http://52.18.248.248:3001/RecipeeImages/DefaultRecipeePhoto.png"} height={90} width={90} /> : <img src={"http://52.18.248.248:3001/" + Ingredient.Photo} height={90} width={90} />} </td>
                                              <td> {Ingredient.Amount} </td>
                                              <td> {Ingredient.Calories} </td>

                                              <td>
                                                <tr> A {Ingredient.VitaminA} </tr>
                                                <tr> B1 {Ingredient.VitaminB1} </tr>
                                                <tr> B2 {Ingredient.VitaminB2} </tr>
                                                <tr> B3 {Ingredient.VitaminB3} </tr>
                                                <tr> B5 {Ingredient.VitaminB5} </tr>
                                                <tr> B6 {Ingredient.VitaminB6} </tr>
                                                <tr> B9 {Ingredient.VitaminB9} </tr>
                                                <tr> B12 {Ingredient.VitaminB12} </tr>
                                                <tr> C {Ingredient.VitaminC} </tr>
                                                <tr> D {Ingredient.VitaminD} </tr>
                                                <tr> E {Ingredient.VitaminE} </tr>
                                                <tr> K {Ingredient.VitaminK} </tr>
                                              </td>

                                              <td>
                                                <tr> {this.props.Language === 'English' ? 'Calcium ' : this.props.Language === 'Spanish' ? 'Calcio ' : 'Wapń '}  {Ingredient.Calcium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Chromium ' : this.props.Language === 'Spanish' ? 'Cromo ' : 'Chrom '} {Ingredient.Chromium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Copper ' : this.props.Language === 'Spanish' ? 'Cobre ' : 'Miedź '} {Ingredient.Copper} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Folate ' : this.props.Language === 'Spanish' ? 'Folato ' : 'Kwas foliowy '} {Ingredient.Folate} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Iron ' : this.props.Language === 'Spanish' ? 'Hierro ' : 'Żelazo '} {Ingredient.Iron} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Magnesium ' : this.props.Language === 'Spanish' ? 'Magnesio ' : 'Magnez '} {Ingredient.Magnesium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Manganese ' : this.props.Language === 'Spanish' ? 'Manganeso ' : 'Mangan '} {Ingredient.Manganese} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Phosphorus ' : this.props.Language === 'Spanish' ? 'Fósforo ' : 'Fosfor '} {Ingredient.Phosphorus} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Potassium ' : this.props.Language === 'Spanish' ? 'Potasio ' : 'Potas '} {Ingredient.Potassium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Sodium ' : this.props.Language === 'Spanish' ? 'Sodio ' : 'Sód '} {Ingredient.Sodium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Selenium ' : this.props.Language === 'Spanish' ? 'Selenio ' : 'Selen '} {Ingredient.Selenium} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Zinc ' : this.props.Language === 'Spanish' ? 'Zinc ' : 'Cynk '} {Ingredient.Zinc} </tr>
                                              </td>

                                              <td>
                                                <tr> {this.props.Language === 'English' ? 'Monounsaturated Fat ' : this.props.Language === 'Spanish' ? 'Grasa monosaturada ' : 'kwasów tłuszczowych jednonienasyconych '       } {Ingredient.MonounsaturatedFat} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Polyunsaturated Fat ' : this.props.Language === 'Spanish' ? 'Grasa poli-insaturada ' : 'Tłuszcz wielonienasycony '       } {Ingredient.PolyunsaturatedFat} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Saturated Fat ' : this.props.Language === 'Spanish' ? 'Grasa saturada ' : 'Tłuszcz nasycony '       } {Ingredient.SaturatedFat} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Trans Fat ' : this.props.Language === 'Spanish' ? 'Grasas trans ' : 'Tłuszcze trans '       } {Ingredient.TransFat} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Other Fats ' : this.props.Language === 'Spanish' ? 'Otras grasas ' : 'Pozostałe tłuszcze '       }  {Ingredient.OtherFats} </tr>
                                              </td>

                                              <td>
                                                <tr> {this.props.Language === 'English' ? 'Fiber ' : this.props.Language === 'Spanish' ? 'Fibra ' : 'Błonnik '       }  {Ingredient.Fiber} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Sugar ' : this.props.Language === 'Spanish' ? 'Azúcar ' : 'Cukier '       } {Ingredient.Sugar} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Other Carbohydrates ' : this.props.Language === 'Spanish' ? 'Otros carbohidratos ' : 'Inny Węglowodany '       } {Ingredient.OtherCarbohydrates} </tr>
                                              </td>

                                              <td>
                                                <tr> {this.props.Language === 'English' ? 'Cholesterol ' : this.props.Language === 'Spanish' ? 'Colesterol ' : 'cholesterol '       } {Ingredient.Cholesterol} </tr>
                                                <tr> {this.props.Language === 'English' ? 'Protein ' : this.props.Language === 'Spanish' ? 'Colesterol ' : 'Białko '       } {Ingredient.Protein}  </tr>
                                              </td>
                                                </tr>

                                             ) : "empty"}
                                        </table>

                        </div>
                        <br/>
                    </div>


                    </div>




              <br/>
            </div>
        );
    }
}
