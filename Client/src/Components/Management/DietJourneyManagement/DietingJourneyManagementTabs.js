import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';

import CreateAnDiet from './DietCreation/DietJourneyCreation';
import MyDietJourney from './MyDietJourney/MyDietJourney'

import AddIngredient from 'material-ui/svg-icons/content/add';
import SearchIngredient from 'material-ui/svg-icons/action/search';
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';
import BuildIcon from 'material-ui/svg-icons/action/build';

const styles = {
    activeStyle: {
      color: 'white',
      textTransform: 'none',
    },
    inactiveStyle: {
      color: '#a8adb5',
      textTransform: 'none'
    }
};

export default class DietingJourneyManagementTabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CreateDietJourneyTabActive: true,
      SearchForDietJourneyTabActive: false,
      MyDietJournetTabActive: false,
      height: null,
      weight: null,
      DateOfBirth: null,
      Gender: null,
      DietingJourneys: [],
      Diet: {}
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.setDietingJourneys = this.setDietingJourneys.bind(this);
    this.setDiet = this.setDiet.bind(this);
  }

  setDiet(Diet) {
    this.setState({Diet: Diet});
  }

  setDietingJourneys() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/getDietingJourneys', {
        username: this.props.LoggedInUser
      })
      .then(function (response) {
        outer.setState({DietingJourneys: response.data.response})
      })
      .catch(function (error) {
        console.log(error);
      });



  }

  componentWillMount() {
      var outer = this;
      var axios = require('axios');

      axios.post('http://52.18.248.248:3001/getUserProperties', {
          username: this.props.LoggedInUser
        })
        .then(function (response) {
          outer.setState({
            height: response.data.response.Height,
            weight: response.data.response.Weight,
            DateOfBirth: response.data.response.DateOfBirth != null || response.data.response.DateOfBirth === undefined ? new Date(response.data.response.DateOfBirth) : null,
            Gender: response.data.response.Gender
          });

        })
        .catch(function (error) {
          console.log(error);
        });


        this.setDietingJourneys();

  }

  handleTabChange(e) {
    e.props.value === 1 ? this.setState({CreateDietJourneyTabActive: true}) : this.setState({CreateDietJourneyTabActive: false})
    e.props.value === 2 ? this.setState({SearchForDietJourneyTabActive: true}) : this.setState({SearchForDietJourneyTabActive: false})
    e.props.value === 3 ? this.setState({MyDietJournetTabActive: true}, this.setDietingJourneys()) : this.setState({MyDietJournetTabActive: false})

  }




    render() {
        return (
          <div>
                    <div className="tabs">
            <Tabs inkBarStyle={{background: 'white'}}  tabItemContainerStyle={{background: '#062b68'}}>

              <Tab label="Create a Dieting Journey" icon={<AddIngredient />} style={this.state.CreateIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={1}  >
                <CreateAnDiet Language={this.props.Language} height={this.state.height} weight={this.state.weight} DateOfBirth={this.state.DateOfBirth} Gender={this.state.Gender} LoggedInUser={this.props.LoggedInUser} />
              </Tab>

              <Tab label="Manage Dieting Journeys" icon={<BuildIcon />} style={this.state.SearchForIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={3} >
                <MyDietJourney Language={this.props.Language} setDiet={this.setDiet} Diet1={this.state.Diet} LoggedInUser={this.props.LoggedInUser} DietingJourneys={this.state.DietingJourneys} />
                <br/>
              </Tab>

            </Tabs>
            </div>
          </div>
        );
    }
}
