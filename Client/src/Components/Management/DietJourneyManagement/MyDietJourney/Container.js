
import React from 'react'
import DaysOfDietingJourney from './DaysOfDietingJourney'
import WeightGraph from './WeightGraph'
import PrintDiet from './PrintDiet'

const style1 = {
  activeStyle: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle: {
    cursor: 'pointer'
  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class Container extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Diet: null
    }

    this.getDiet = this.getDiet.bind(this);
  }


  getDiet() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForDiets', {
      DietName: "",
      OnlyMineDiets: false,
      username: "",
      onlyFavouriteDiets: false
      })
      .then(function (response) {
        var _ = require('lodash');
				var DietIndex = _.findIndex(response.data.response, function(o) {
          return o.DietID == outer.props.DietID;
        });

        outer.setState({Diet: response.data.response[DietIndex]});
      })
      .catch(function (error) {
        console.log(error);
      });

  }

    render() {
        return (
            <div>
              <div className="WhatToCompare11">
                <span style={this.props.WeightManagementActive === true ? style1.activeStyle : style1.inactiveStyle} onClick={this.props.activateWeightManagement}> Weight Management </span> or <span style={this.props.NutritionActive === true ? style1.activeStyle : style1.inactiveStyle} onClick={this.props.activateNutrition}  > Nutrition </span>
              </div>

              <br/>

              <div className="Navigation" style={this.props.WeightManagementActive === true ? divStyle.activeStyle : divStyle.inactiveStyle } >
                <br/>
                {this.props.ActiveDietingJourneyID === null ? null : <DaysOfDietingJourney Language={this.props.Language} setDietJourneyNote={this.props.setDietJourneyNote} DietingJourney={this.props.DietingJourney} ActiveDietingJourneyID={this.props.ActiveDietingJourneyID} setDietJourneyWeight={this.props.setDietJourneyWeight} GoBack={this.props.GoBack} DaysOfDietingJourney={this.props.DietJourneyDays}/> }
                <br/>
                {this.props.ActiveDietingJourneyID === null ? null : <WeightGraph Language={this.props.Language} DietingJourney={this.props.DietingJourney} data={this.props.data} ActiveDietingJourneyID={this.props.ActiveDietingJourneyID} DaysOfDietingJourney={this.props.DietJourneyDays} /> }
                <br/>
              </div>

              {this.props.NutritionActive === true ? <div className="Navigation"> <PrintDiet Language={this.props.Language} Diet1={this.props.Diet1} DietID={this.props.DietID} /> </div> : <null />}

              <br/>

            </div>
        );
    }
}
