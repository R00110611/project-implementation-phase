
import React from 'react'
import LineChart from 'react-linechart';
import 'react-linechart/dist/styles.css';

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class WeightGraph extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    }

    this.printDate = this.printDate.bind(this);
    this.formatDate = this.formatDate.bind(this);
  }

  formatDate(date){
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

  printDate(Datess) {
    return this.formatDate(Datess)
  }

    render() {
        return (
            <div className="IngredientDescription1">
              <h2> Weight Graph </h2>

              <LineChart
                width={600}
                height={400}
                data={this.props.data}
                xLabel="Day"
                yLabel="Weight"
              />

              <br/>



            </div>
        );
    }
}
