
import React from 'react'
import TextField from 'material-ui/TextField';

const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}

export default class DaysOfDietingJourney extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    }

    this.formatDate = this.formatDate.bind(this);
    this.handleWeightChange = this.handleWeightChange.bind(this);
    this.handleNoteChange = this.handleNoteChange.bind(this);
  }

  handleNoteChange(newNote, Day) {
    if(newNote.length < 250) {
      this.props.setDietJourneyNote(Day.Day, newNote, this.props.ActiveDietingJourneyID);
    }
  }

  handleWeightChange(newWeight, Day) {
    this.props.setDietJourneyWeight(Day.Day, newWeight, this.props.ActiveDietingJourneyID )
  }

  formatDate(date){
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

    render() {
        return (
            <div className="IngredientDescription1">
              <h2> Days Of Dieting Journey </h2>

              <div className="dontOverflow">
                    <table className="tableMDJ">
                    <th> Date </th>
                    <th> Day </th>
                    <th> Weight </th>
                    <th> Notes </th>

                      {this.props.DaysOfDietingJourney.map(Day =>
                        <tr>
                        <td> {this.formatDate(Day.Date)} </td>
                          <td> {Day.Day} </td>

                          <td>

                          <input onChange={e => this.handleWeightChange(e.target.value, Day)} type="text" value={Day.Weight} placeholder={this.props.Language === 'English' ? 'Weight' : this.props.Language === 'Spanish' ? 'Weight' : 'Weight'       } style={{textAlign: 'center'}} />

                          </td>

                          <td>
                            <br/>
                            <textarea value={Day.Note} onChange={e => this.handleNoteChange(e.target.value, Day)} style={{resize: 'vertical', textAlign: 'center'}} placeholder={this.props.Language === 'English' ? 'Note' : this.props.Language === 'Spanish' ? 'Note' : 'Notatka'       }>


                            </textarea>
                            <br/>
                            <span style={Day.Note > 249 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {Day.Note.length}/249 </span>
                          </td>


                        </tr>
                      )}
                    </table>
                    <br/>
                </div>
                <br/>
            </div>
        );
    }
}
