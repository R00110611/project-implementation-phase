
import React from 'react'
import TextField from 'material-ui/TextField';
import Container from './Container'


const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

const style1 = {
  activeStyle: {
    fontWeight: 'bold'
  },
  inactiveStyle: {

  }
}

const style2 = {
  activeStyle: {
    color: 'white',
    backgroundColor: 'navy',
    cursor: 'pointer'
  },
  inactiveStyle: {

  }
}

export default class MyDietJourney extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        ActiveDietingJourneyID: null,
        DietingJourney: {},
        DietJourneyDays: [],
        data: [
             {
                 color: "yellow",
                 points:[{x:1, y: 2}]
             }
         ],
        DietID: null,
        WeightManagementActive: true,
        NutritionActive: false,
        DietList: [],
        Diet: null
    }

    this.rowClick = this.rowClick.bind(this);
    this.getDietJourneyDays = this.getDietJourneyDays.bind(this);
    this.GoBack = this.GoBack.bind(this);
    this.setDietJourneyWeight = this.setDietJourneyWeight.bind(this);
    this.setDietJourneyNote = this.setDietJourneyNote.bind(this);
    this.isFloat = this.isFloat.bind(this);
    this.formatDate = this.formatDate.bind(this);
    this.activateWeightManagement = this.activateWeightManagement.bind(this);
    this.activateNutrition = this.activateNutrition.bind(this);
    this.getDiet = this.getDiet.bind(this);
    this.onHover = this.onHover.bind(this);
  }



  onHover(activeNr) {
      this.setState({hoverActive: activeNr});
  }

  formatDate(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

  isFloat(weight) {
    if(isNaN(weight) === false) {
      return true;
    } else {
      return false;
    }
  }


  getDietJourneyDays(DietingJourneyID) {
    var getDietJourneyDays = [];


    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForDiets', {
        username: "",
        DietName: ""
      })
      .then(function (response) {
        var _ = require('lodash');

        var index = _.findIndex(response.data.response, function(o) {
          return o.DietID == DietingJourneyID.DietID
        });

        var arr = [];




        //length different than below

        var uniqueDays = [];

        for(var i =0; i<response.data.response[index].DietDays.length; i++) {
          uniqueDays.push(response.data.response[index].DietDays[i].Day);
        }

        var _ = require('lodash');
        uniqueDays = _.uniq(uniqueDays);

        for(var i=0; i<uniqueDays.length; i++) {
          //console.log('create obj');
          var obj = {Day: i+1, Weight: null, Note: null, Date: new Date(DietingJourneyID.StartingDate)};
          obj.Date.setDate(obj.Date.getDate() + i);

          arr.push(obj);

        }







          axios.post('http://52.18.248.248:3001/getDietingJourneysDays', {
              DietingJourneyID: DietingJourneyID.DietingJourneyID
            })
            .then(function (response) {

              for(var i=0; i<response.data.response.length; i++) {
                var day = response.data.response[i].Day;
                var weight = response.data.response[i].Weight;
                var note = response.data.response[i].Note;

                var index = _.findIndex(arr, function(o) {
                  return o.Day == day
                });

                if(response.data.response.length != 0) {
              arr[index].Weight = weight;
              arr[index].Note = note;
            } else {

            }

            }



            var data = [
              {
                color: "yellow",
                points: [
                ]
              }
            ]

             arr.map(Day =>
               { Day.Weight != 0 ? data[0].points.push({x: Day.Day, y: Day.Weight}) : null}
             );


            outer.setState({DietJourneyDays: arr, data: data});


            })
            .catch(function (error) {
              console.log(error);
            });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  setDietJourneyNote(Day, Note, ActiveDietingJourneyID) {
    var _ = require('lodash');

    var outer = this;
    var axios = require('axios');

    var arr = this.state.DietJourneyDays;

    var index = _.findIndex(arr, function(o) {
      return o.Day == Day
    });

    arr[index].Note=Note;

    this.setState({DietJourneyDays: arr})

    axios.post('http://52.18.248.248:3001/updateDietingJourneysDaysNote', {
        DietingJourneyID: ActiveDietingJourneyID,
        Day: Day,
        Note: Note
      })
      .then(function (response) {
          if(response.data.response === true) {

          }

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  setDietJourneyWeight(Day, Weight, ActiveDietingJourneyID) {
    var _ = require('lodash');

    var arr = this.state.DietJourneyDays;

    var index = _.findIndex(arr, function(o) {
      return o.Day == Day
    });

    arr[index].Weight=Weight;


    if(this.isFloat(Weight) === true && Weight != "") {

    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/updateDietingJourneysDays', {
        DietingJourneyID: ActiveDietingJourneyID,
        Day: Day,
        Weight: Weight
      })
      .then(function (response) {
        var arr = outer.state.DietJourneyDays;

        var data = [
          {
            color: "yellow",
            points: [
            ]
          }
        ]

         arr.map(Day =>
           { Day.Weight != 0 ? data[0].points.push({x: Day.Day, y: Day.Weight}) : null}
         );


        outer.setState({DietJourneyDays: arr, data: data});
      })
      .catch(function (error) {
        console.log(error);
      });
    } else {
      var arr = this.state.DietJourneyDays;

      var index = _.findIndex(arr, function(o) {
        return o.Day == Day
      });


      if(Weight == "") {
        Weight = null
      }


      arr[index].Weight=Weight;

      this.setState({DietJourneyDays: arr});
    }

  }

  rowClick(DietingJourney) {
    this.setState({ActiveDietingJourneyID: DietingJourney.DietingJourneyID, DietingJourney: DietingJourney, DietID: DietingJourney.DietID}, this.getDietJourneyDays(DietingJourney), this.getDiet(DietingJourney.DietID));
  }

  GoBack() {
    this.setState({ActiveDietingJourneyID: null});
  }

  activateWeightManagement() {
    this.setState({WeightManagementActive: true, NutritionActive: false});
  }

  activateNutrition() {
    this.setState({NutritionActive: true, WeightManagementActive: false});
  }

  getDiet(DietID) {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForDiets', {
      DietName: "",
      OnlyMineDiets: false,
      username: "",
      onlyFavouriteDiets: false
      })
      .then(function (response) {
        var _ = require('lodash');
				var DietIndex = _.findIndex(response.data.response, function(o) {
          return o.DietID == DietID;
        });


        //outer.setState({Diet: response.data.response[DietIndex]});
        outer.props.setDiet(response.data.response[DietIndex]);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

    render() {
        return (
            <div className="DietCreationParent">
              <div style={this.state.ActiveDietingJourneyID === null ? null : divStyle.inactiveStyle}>
                <div style={{textAlign: 'center'}}> <h2 style={{color: 'black'}}> Select dieting Journey you want to manage </h2> </div>


                <div className="wrapper12">
                <br/>
                <table className="wrapper12" style={{backgroundColor: 'white'}}>
                <tr style={{backgroundColor: 'gray', width: '100%'}}>
                  <th> Name</th>
                  <th> Start Date</th>
                </tr>
                  {this.props.DietingJourneys.map(DietingJourney =>
                  <tr onClick={this.rowClick.bind(null, DietingJourney)} onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, DietingJourney.DietingJourneyID)} style={this.state.hoverActive === DietingJourney.DietingJourneyID ? style2.activeStyle : style2.inactiveStyle}                       >
                    <td> {DietingJourney.DietingJourneyName} </td>
                    <td> {this.formatDate(new Date(DietingJourney.StartingDate))} </td>
                  </tr>
                  )}
                </table>
                <br/>
              </div>
                <br/>
              </div>



              <div style={this.state.ActiveDietingJourneyID === null ? divStyle.inactiveStyle : null}>
                <div style={{textAlign: 'center'}}> <h1> <span onClick={this.GoBack} style={{textAlign: 'center', color: 'black', cursor: 'pointer'}}> Select other dieting journey </span>  </h1> </div>
<br/>
                <Container
                  WeightManagementActive={this.state.WeightManagementActive}
                  NutritionActive={this.state.NutritionActive}
                  Diet={this.state.Diet}
                  DietID={this.state.DietID}
                  Diet1={this.props.Diet1}
                  activateWeightManagement={this.activateWeightManagement}
                  activateNutrition={this.activateNutrition}
                  ActiveDietingJourneyID={this.state.ActiveDietingJourneyID}
                  DietingJourney={this.state.DietingJourney}
                  setDietJourneyNote={this.setDietJourneyNote}
                  data={this.state.data}
                  DietJourneyDays={this.state.DietJourneyDays}
                  setDietJourneyWeight={this.setDietJourneyWeight}
                  GoBack={this.GoBack}
                  Language={this.props.Language}
                />


                </div>

            </div>
        );
    }
}
