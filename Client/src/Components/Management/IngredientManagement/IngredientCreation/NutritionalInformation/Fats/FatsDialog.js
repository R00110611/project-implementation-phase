import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors';
import Dialog from 'material-ui/Dialog';

const styles = {
  labelStyle1: {
    color: 'red',
    fontSize: '150%'
  },
  underlineStyle: {
    borderColor: orange500,
  },
  inputStyle: {
    color: blue500,
  },
  style: {
    width: '50%'
  },
  vitaminsUnderlineStyle: {
    borderColor: blue500,
    width: '50%'
  },
  vitaminStyle: {
    width: '50%'
  },
  vitaminInputStyle: {
    width: '50%'
  },
  titleStyle: {
    color: blue500,
    backgroundColor: orange500
  },
  dialogStyle: {
    height: '1'
  },
  dialogContentStyle: {
    textAlign: "center"
  },
  VitaminTextStyle: {
    width: '10%',
    marginLeft: '5%'
  },
  FatTextStyle: {
    width: '32%',
    marginLeft: '5%'
  },
  DescriptionStyle: {
    width: '25%'
  }
}


export default class Fats extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false
    }

    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

    render() {
      const actions = [
            <FlatButton
              label="RESET"
            />,
            <FlatButton
              label="OK"
              onClick={this.handleClose}
            />
          ];
        return (
            <div>
              <FlatButton label="Fats" backgroundColor={'red'} onClick={this.handleOpen} />

                <Dialog
                  title="Fats"
                  actions={actions}
                  modal={false}
                  open={this.state.open}
                  onRequestClose={this.handleClose}
                  autoScrollBodyContent={true}
                  contentStyle={styles.contentStyle}
                  titleStyle={styles.titleStyle}
                  style={styles.dialogStyle}
                  contentStyle={styles.dialogContentStyle}
                >

                  <TextField
                    floatingLabelStyle={styles.labelStyle1}
                    floatingLabelText="Monounsaturated Fat"
                    underlineStyle={styles.underlineStyle}
                    inputStyle={styles.inputStyle}
                    value={this.props.state.Ingredient.NutritionalInformation.Fats.MonounsaturatedFat}
                    onChange={e => this.setState({ MonounsaturatedFat: e.target.value })}
                    style={styles.FatTextStyle}
                  />

                  <TextField
                    floatingLabelStyle={styles.labelStyle1}
                    floatingLabelText="Polyunsaturated Fat"
                    underlineStyle={styles.underlineStyle}
                    inputStyle={styles.inputStyle}
                    value={this.state.PolyunsaturatedFat}
                    onChange={e => this.setState({ PolyunsaturatedFat: e.target.value })}
                    style={styles.FatTextStyle}
                  />

                  <TextField
                    floatingLabelStyle={styles.labelStyle1}
                    floatingLabelText="Saturated Fat"
                    underlineStyle={styles.underlineStyle}
                    inputStyle={styles.inputStyle}
                    value={this.state.SaturatedFat}
                    onChange={e => this.setState({ SaturatedFat: e.target.value })}
                    style={styles.FatTextStyle}
                  />

                  <TextField
                    floatingLabelStyle={styles.labelStyle1}
                    floatingLabelText="Trans Fat"
                    underlineStyle={styles.underlineStyle}
                    inputStyle={styles.inputStyle}
                    value={this.state.TransFat}
                    onChange={e => this.setState({ TransFat: e.target.value })}
                    style={styles.FatTextStyle}
                  />

                  <TextField
                    floatingLabelStyle={styles.labelStyle1}
                    floatingLabelText="Other fats"
                    underlineStyle={styles.underlineStyle}
                    inputStyle={styles.inputStyle}
                    value={this.state.OtherFats}
                    onChange={e => this.setState({ OtherFats: e.target.value })}
                    style={styles.FatTextStyle}
                  />

              </Dialog>
            </div>
        );
    }
}
