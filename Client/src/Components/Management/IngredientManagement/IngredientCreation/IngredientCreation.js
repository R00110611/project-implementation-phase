
import React from 'react'
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DropzoneComponent from './DropzoneComponent'
var Dropzone = require('react-dropzone');
import Notifications, {notify} from 'react-notify-toast';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import Help from 'material-ui/svg-icons/action/help';
import DownArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';


const styles = {
  labelStyle1: {
    color: 'navy',
    fontSize: '100%'
  },
  underlineStyle: {
    borderColor: orange500,
    color: 'white'
  },
  inputStyle: {
    color: 'black',
    cursor: 'pointer'
  },
  style: {
    width: '50%'
  },
  vitaminsUnderlineStyle: {
    borderColor: 'white',
    width: '50%'
  },
  vitaminStyle: {
    width: '50%'
  },
  vitaminInputStyle: {
    width: '50%'
  },
  titleStyle: {
    color: blue500,
    backgroundColor: orange500
  },
  dialogStyle: {
    height: '1'
  },
  dialogContentStyle: {
    textAlign: "center"
  },
  VitaminTextStyle: {
    width: '10%',
    marginLeft: '5%'
  },
  FatTextStyle: {
    width: '32%',
    marginLeft: '5%'
  },
  DescriptionStyle: {
    width: '25%'
  },
  NutritionalInputStyle: {
    color: 'white',
    cursor: 'pointer',
    textAlign: 'center',
  },
  NutritionalInputStyleStyle: {
    width: '100%',
    textAlign: 'center'
  },
  FloatingLabelStyle: {
    color: 'white',
    textAlign: 'center',
    width: '100%'
  },
  floatingLabelFocusStyle: {
    textAlign: 'center',
    width: '100%'
  },
  textAreaFloatingLabelStyle: {
    color: 'white',
    textAlign: 'left',
    width: '100%'
  },
  floatingLabelShrinkStyle: {
    textAlign: 'center',
    width: '100%',
  },
  hintStyle: {
    textAlign: 'center',
    width: '100%'
  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    width: '50%',
    marginLeft: '25%',
    marginRight: '25%',
    color: 'black',
    marginTop: '2%',
    backgroundColor: 'red'
  }
}

const divStyles = {
  activeStyle: {
    backgroundColor: '#525963',
  },
  inactiveStyle: {
    display: 'none'
  },
  hoverStyle: {
    cursor: 'pointer'
  },
  inhoverStyle: {

  }
}

const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}

export default class IngredientCreation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      VitaminsDialogOpen: false,
      MineralsDialogOpen: false,
      FatsDialogOpen: false,
      CarbohydratesDialogOpen: false,
      IngredientAmount: "",
      Calories: "",
      VitaminA: "",
      VitaminB1: "",
      VitaminB2: "",
      VitaminB3: "",
      VitaminB5: "",
      VitaminB6: "",
      VitaminB9: "",
      VitaminB12: "",
      VitaminC: "",
      VitaminD: "",
      VitaminE: "",
      VitaminK: "",
      Calcium: "",
      Chromium: "",
      Copper: "",
      Folate: "",
      Iron: "",
      Magnesium: "",
      Manganese: "",
      Phosphorus: "",
      Potassium: "",
      Sodium: "",
      Selenium: "",
      Zinc: "",
      MonounsaturatedFat: "",
      PolyunsaturatedFat: "",
      SaturatedFat: "",
      TransFat: "",
      OtherFats: "",
      Fiber: "",
      OtherCarbohydrates: "",
      Sugar: "",
      Cholesterol: "",
      Protein: "",
      IngredientName: "",
      IngredientDescription: "",
      IngredientLanguage: "English",
      value: 1,
      IngredientPhotos: [1],
      image1: false,
      username: this.props.LoggedInUser,
      filesPhotos: [],
      photoImage: new FormData(),
      NutritionalInformationActive: false,
      DescriptionActive: false,
      activeDiv: 1,
      hoverActive: 0
    };

    this.createIngredient = this.createIngredient.bind(this);
    this.handleVitaminsDialogOpen = this.handleVitaminsDialogOpen.bind(this);
    this.handleVitaminsDialogClose = this.handleVitaminsDialogClose.bind(this);
    this.clearVitamins = this.clearVitamins.bind(this);
    this.clearMinerals = this.clearMinerals.bind(this);
    this.clearFats = this.clearFats.bind(this);
    this.clearCarbohydrates = this.clearCarbohydrates.bind(this);
    this.handleIngredientLanguageChange = this.handleIngredientLanguageChange.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.removePhoto = this.removePhoto.bind(this);
    this.isFloat = this.isFloat.bind(this);
    this.Continue = this.Continue.bind(this);
    this.GoBack = this.GoBack.bind(this);
    this.ingredientCreated = this.ingredientCreated.bind(this);
    this.isDisabled = this.isDisabled.bind(this);
    this.Help = this.Help.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onHover = this.onHover.bind(this);
}

  Help() {
    var Information = "";

    if(this.props.Language == "English") {
      Information = "Enter the Nutritional Information of the ingredient you want to add to the database, please enter the amounts in grams!";
    }
    if(this.props.Language == "Polish") {
      Information = "Wprowadź odżywcze składnika, który chcesz dodać do bazy danych, należy wprowadzić ilości w gramach!";
    }
    if(this.props.Language == "Spanish") {
      Information = "Ingrese la información nutricional del ingrediente que desea agregar a la base de datos, ingrese las cantidades en gramos.";
    }

    toastr.info(Information, 'Help', {positionClass: 'toast-top-center', preventDuplicates: true});
  }

  onHover(activeNr) {
      this.setState({hoverActive: activeNr});
  }

  isDisabled() {
    var Disabled = false;

    if (this.state.IngredientAmount == 0 || this.state.IngredientAmount == "") {
        Disabled = true;
    }
    return Disabled;
  }

ingredientCreated() {
  toastr.success('Ingredient ' + this.state.IngredientName + ' has been created successfully', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

  this.setState({
    VitaminsDialogOpen: false,
    MineralsDialogOpen: false,
    FatsDialogOpen: false,
    CarbohydratesDialogOpen: false,
    IngredientAmount: "",
    Calories: "",
    VitaminA: "",
    VitaminB1: "",
    VitaminB2: "",
    VitaminB3: "",
    VitaminB5: "",
    VitaminB6: "",
    VitaminB9: "",
    VitaminB12: "",
    VitaminC: "",
    VitaminD: "",
    VitaminE: "",
    VitaminK: "",
    Calcium: "",
    Chromium: "",
    Copper: "",
    Folate: "",
    Iron: "",
    Magnesium: "",
    Manganese: "",
    Phosphorus: "",
    Potassium: "",
    Sodium: "",
    Selenium: "",
    Zinc: "",
    MonounsaturatedFat: "",
    PolyunsaturatedFat: "",
    SaturatedFat: "",
    TransFat: "",
    OtherFats: "",
    Fiber: "",
    OtherCarbohydrates: "",
    Sugar: "",
    Cholesterol: "",
    Protein: "",
    IngredientName: "",
    IngredientDescription: "",
    IngredientLanguage: "English",
    value: 1,
    IngredientPhotos: [1],
    image1: false,
    username: this.props.LoggedInUser,
    filesPhotos: [],
    photoImage: new FormData(),
    NutritionalInformationActive: false,
    DescriptionActive: false,
    activeDiv: 0
  })
}

setLanguage(event, index, val) {
  this.setState({LanguageValue: val});
  val === 1 || event.target.value === "English" ? this.setState({Language: 'English'}) : null;
  val === 2 || event.target.value === "Polish" ? this.setState({Language: 'Polish'}) : null;
  val === 3 || event.target.value === "Spanish"? this.setState({Language: 'Spanish'}) : null;
}

onClick(tabToActivate) {
  if(tabToActivate == 1) {
    if(this.state.DescriptionActive === true) {
      this.setState({DescriptionActive: false});
    } else {
      this.setState({NutritionalInformationActive: false, DescriptionActive: true});
    }
  }

  if(tabToActivate == 2) {
    if(this.state.NutritionalInformationActive === true) {
      this.setState({NutritionalInformationActive: false});
    } else {
      this.setState({DescriptionActive: false, NutritionalInformationActive: true});
    }
  }

}

isFloat(e) {
  if(isNaN(e.target.value) === false) {
    return true;
  } else {
    return false;
  }
}

Continue() {
  this.setState({NutritionalInformationActive: false, DescriptionActive: true});
}

GoBack() {
  this.setState({NutritionalInformationActive: true, DescriptionActive: false});
}

createIngredient() {
  var outer = this;
  var request = require('superagent');

  var photo = new FormData();
  photo.append('photo', this.state.filesPhotos);

  var obj = JSON.stringify(this.state);
  photo.append('obj', obj);

  request.post('http://52.18.248.248:3001/CreateIngredient')
    .send(photo)
    .end(function(err, resp) {
      if (err) { console.error(err); }
      outer.ingredientCreated();
      return resp;
    });
}

removePhoto(photoNr) {
  this.setState({IngredientPhotos: [1], filesPhotos: [], image1: false});
}

onDrop(files) {
  if(this.state.image1 === false) {
    this.state.IngredientPhotos.push(files[0].preview);
    this.setState({image1: true});
    this.setState({filesPhotos: files[0]});
  }
}

handleIngredientLanguageChange(event, index, val) {
  this.setState({value: val});
  event.target.value === 'English' ? this.setState({IngredientLanguage: 'English'}) : null;
  event.target.value === 'Polish' ? this.setState({IngredientLanguage: 'Polish'}) : null;
  event.target.value === 'Spanish' ? this.setState({IngredientLanguage: 'Spanish'}) : null;
}

handleVitaminsDialogOpen = () => {
  this.setState({VitaminsDialogOpen: true});
};

handleVitaminsDialogClose = () => {
  this.setState({VitaminsDialogOpen: false});
};

handleMineralsDialogOpen = () => {
  this.setState({MineralsDialogOpen: true});
};

handleMineralsDialogClose = () => {
  this.setState({MineralsDialogOpen: false});
};

handleFatsDialogOpen = () => {
  this.setState({FatsDialogOpen: true});
};

handleFatsDialogClose = () => {
  this.setState({FatsDialogOpen: false});
};

handleCarbohydratesDialogOpen = () => {
  this.setState({CarbohydratesDialogOpen: true});
};

handleCarbohydratesDialogClose = () => {
  this.setState({CarbohydratesDialogOpen: false});
};

clearCarbohydrates() {
  this.setState({Fiber: ""});
  this.setState({OtherCarbohydrates: ""});
  this.setState({Sugar: ""});
}

clearVitamins() {
  this.setState({VitaminA: ""});
  this.setState({VitaminB1: ""});
  this.setState({VitaminB2: ""});
  this.setState({VitaminB3: ""});
  this.setState({VitaminB5: ""});
  this.setState({VitaminB6: ""});
  this.setState({VitaminB9: ""});
  this.setState({VitaminB12: ""});
  this.setState({VitaminC: ""});
  this.setState({VitaminD: ""});
  this.setState({VitaminE: ""});
  this.setState({VitaminK: ""});
}

clearMinerals() {
  this.setState({Calcium: ""});
  this.setState({Chromium: ""});
  this.setState({Copper: ""});
  this.setState({Folate: ""});
  this.setState({Iron: ""});
  this.setState({Magnesium: ""});
  this.setState({Manganese: ""});
  this.setState({Phosphorus: ""});
  this.setState({Potassium: ""});
  this.setState({Sodium: ""});
  this.setState({Selenium: ""});
  this.setState({Zinc: ""});
}

clearFats() {
  this.setState({MonounsaturatedFat: ""});
  this.setState({PolyunsaturatedFat: ""});
  this.setState({SaturatedFat: ""});
  this.setState({TransFat: ""});
  this.setState({OtherFats: ""});
}


    render() {
      const VitaminDialogActions = [
            <FlatButton
              label="RESET"
              primary={true}
              onClick={this.clearVitamins}
            />,
            <FlatButton
              label="OK"
              primary={true}
              onClick={this.handleVitaminsDialogClose}
            />,
          ];

          const MineralsDialogActions = [
                <FlatButton
                  label="RESET"
                  primary={true}
                  onClick={this.clearMinerals}
                />,
                <FlatButton
                  label="OK"
                  primary={true}
                  onClick={this.handleMineralsDialogClose}
                />,
              ];

              const FatsDialogActions = [
                    <FlatButton
                      label="RESET"
                      primary={true}
                      onClick={this.clearFats}
                    />,
                    <FlatButton
                      label="OK"
                      primary={true}
                      onClick={this.handleFatsDialogClose}
                    />,
                  ];

                  const CarbohydratesDialogActions = [
                        <FlatButton
                          label="RESET"
                          primary={true}
                          onClick={this.clearCarbohydrates}
                        />,
                        <FlatButton
                          label="OK"
                          primary={true}
                          onClick={this.handleCarbohydratesDialogClose}
                        />,
                      ];

        return (
            <div className="IngredientCreation">
              <br/>
                <div style={{textAlign: 'center'}}>
                  <Help onClick={this.Help} style={{cursor: 'pointer', color: 'white'}}/>
                </div>
            <br/>

            <div className="ContainerIC">
              <div className="DescriptionIC">

              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 1)} style={this.state.hoverActive === 1 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 1)}>
                <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}} >
                {this.props.Language === 'English' ? 'Description' : this.props.Language === 'Spanish' ? 'Descripción' : 'Opis'       }
                <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.DescriptionActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                </span>
              </div>

                <div className="ContentsIC" style={this.state.DescriptionActive === true ? divStyles.activeStyle : divStyles.inactiveStyle} >

                <br/>


                <div style={{textAlign: 'center'}}>


                      <input onChange={e => this.setState({ IngredientName: e.target.value })} type="text" value={this.state.IngredientName} placeholder={this.props.Language === 'English' ? 'Name *' : this.props.Language === 'Spanish' ? 'Nombre *' : 'Nazwa *'       } style={{textAlign: 'center'}} />
                      <br/>
                      <span style={this.state.IngredientName.length > 50 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.IngredientName.length}/50 </span>

                      <br/> <br/>


                      <textarea value={this.state.IngredientDescription} onChange={e => this.setState({ IngredientDescription: e.target.value })} style={{resize: 'vertical', textAlign: 'center'}} placeholder={this.props.Language === 'English' ? 'Description' : this.props.Language === 'Spanish' ? 'Descripción' : 'Opis'       }>

                      </textarea>
                      <br/>
                      <span style={this.state.IngredientDescription.length > 250 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.IngredientDescription.length}/250 </span>


                      <br/> <br/>

                      <span style={{color: 'black'}}> {this.props.Language === 'English' ? 'Language' : this.props.Language === 'Spanish' ? 'Idioma' : 'Język'} </span>

                      <select onChange={this.handleIngredientLanguageChange} >

                        <option value="English" selected={this.state.IngredientLanguage==="English" ? true : false} > {this.props.Language === 'English' ? 'English' : this.props.Language === 'Spanish' ? 'Inglés' : 'Angielski'} </option>
                        <option value="Polish" selected={this.state.IngredientLanguage==="Polish" ? true : false}  > {this.props.Language === 'English' ? 'Polish' : this.props.Language === 'Spanish' ? 'Polaco' : 'Polski'} </option>
                        <option value="Spanish" selected={this.state.IngredientLanguage==="Spanish" ? true : false}  > {this.props.Language === 'English' ? 'Spanish' : this.props.Language === 'Spanish' ? 'Español' : 'Hiszpanski'} </option>
                      </select>


                      <br/>   <br/>

                    {this.state.image1 === true ?
                      <p style={{color: 'black'}}> Ingredient Image <br/> <img onClick={this.removePhoto} src={this.state.IngredientPhotos[1]} height="100px" width="100px" className="image1" onClick={this.removePhoto.bind(this, 1)}  /> </p>
                      :
                      <DropzoneComponent Language={this.props.Language} onDrop={this.onDrop} /> }
                    <br/>



                </div>







                </div>

              </div>

              <br/>

              <div className="NutritionalInformationIC">
              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 2)} style={this.state.hoverActive === 2 ? divStyles.hoverStyle : divStyles.inhoverStyle}  onClick={this.onClick.bind(this, 2)}>
                <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}>
                {this.props.Language === 'English' ? 'Nutritional Information' : this.props.Language === 'Spanish' ? 'Información Nutricional' : 'Wartość odżywcza'       }
                <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.NutritionalInformationActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                </span>
              </div>

                <div className="ContentsIC" style={this.state.NutritionalInformationActive === true ? divStyles.activeStyle : divStyles.inactiveStyle}>

                <br/>

                  <div style={{textAlign: 'center'}}>

                  <input onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ IngredientAmount: e.target.value }) : null } type="text" value={this.state.IngredientAmount} placeholder={this.props.Language === 'English' ? 'Amount *' : this.props.Language === 'Spanish' ? 'Cantidad *' : 'Ilosc *'       } style={{textAlign: 'center'}} />
                  <br/><br/>

                  <input onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Calories: e.target.value }) : null } type="text" value={this.state.Calories} placeholder={this.props.Language === 'English' ? 'Calories' : this.props.Language === 'Spanish' ? 'Calorías' : 'Kalorie'} style={{textAlign: 'center'}} />
                  <br/><br/>

                  <input onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Cholesterol: e.target.value }) : null } type="text" value={this.state.Cholesterol} placeholder={this.props.Language === 'English' ? 'Cholesterol' : this.props.Language === 'Spanish' ? 'Colesterol' : 'Cholesterol'} style={{textAlign: 'center'}} />
                  <br/><br/>

                  <input onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Protein: e.target.value }) : null } type="text" value={this.state.Protein} placeholder={this.props.Language === 'English' ? 'Protein' : this.props.Language === 'Spanish' ? 'Proteína' : 'Białko'} style={{textAlign: 'center'}} />
                  <br/><br/>


                  <span className="ButtonGroup">
                    <RaisedButton label={this.props.Language === 'English' ? 'Vitamins' : this.props.Language === 'Spanish' ? 'Vitaminas' : 'Witaminy'       }
                      onClick={this.handleVitaminsDialogOpen}
                      backgroundColor="#5889d8"
                    style={{marginLeft: '20%', marginRight: '20%', width: '60%'}}
                      className="VitaminsButton"
                      labelColor="white"  >
                    </RaisedButton>
                  </span>

                    <Dialog
                              title={this.props.Language === 'English' ? 'Vitamins' : this.props.Language === 'Spanish' ? 'Vitaminas' : 'Witaminy'       }
                              actions={VitaminDialogActions}
                              modal={false}
                              open={this.state.VitaminsDialogOpen}
                              onRequestClose={this.handleVitaminsDialogClose}
                              autoScrollBodyContent={true}
                              titleStyle={styles.titleStyle}
                              style={styles.dialogStyle}
                              contentStyle={styles.dialogContentStyle}
                            >

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminA' : this.props.Language === 'Spanish' ? 'Vitamina A' : 'Witamina A'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminA}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminA: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />

                              <br/>

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB1' : this.props.Language === 'Spanish' ? 'Vitamina B1' : 'Witamina B1'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB1}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB1: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />

                              <br/>

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB2' : this.props.Language === 'Spanish' ? 'Vitamina B2' : 'Witamina B2'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB2}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB2: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />

                              <br/>

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB3' : this.props.Language === 'Spanish' ? 'Vitamina B3' : 'Witamina B3'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB3}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB3: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />

                              <br/>

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB5' : this.props.Language === 'Spanish' ? 'Vitamina B5' : 'Witamina B5'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB5}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB5: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />

                              <br/>

                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB6' : this.props.Language === 'Spanish' ? 'Vitamina B6' : 'Witamina B6'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB6}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB6: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB9' : this.props.Language === 'Spanish' ? 'Vitamina B9' : 'Witamina B9'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB9}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB9: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminB12' : this.props.Language === 'Spanish' ? 'Vitamina B12' : 'Witamina B12'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminB12}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminB12: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminC' : this.props.Language === 'Spanish' ? 'Vitamina C' : 'Witamina C'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminC}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminC: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminD' : this.props.Language === 'Spanish' ? 'Vitamina D' : 'Witamina D'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminD}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminD: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminE' : this.props.Language === 'Spanish' ? 'Vitamina E' : 'Witamina E'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminE}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminE: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                              <br/>
                              <TextField
                                floatingLabelStyle={styles.labelStyle1}
                                floatingLabelText={this.props.Language === 'English' ? 'VitaminK' : this.props.Language === 'Spanish' ? 'Vitamina K' : 'Witamina K'       }
                                underlineStyle={styles.underlineStyle}
                                inputStyle={styles.inputStyle}
                                value={this.state.VitaminK}
                                onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ VitaminK: e.target.value }) : null }
                                style={styles.VitaminTextStyle}
                              />
                            </Dialog>


                            <br/>

                  <span className="ButtonGroup">
                      <RaisedButton label={this.props.Language === 'English' ? 'Minerals' : this.props.Language === 'Spanish' ? 'Minerales' : 'Minerały'       }
                        onClick={this.handleMineralsDialogOpen}
                        backgroundColor="#5889d8"
                    style={{marginLeft: '20%', marginRight: '20%', width: '60%', marginTop: '1%'}}
                        className="MineralsButton"
                        labelColor="white"  >
                      </RaisedButton>
                </span>
                      <Dialog
                                title={this.props.Language === 'English' ? 'Minerals' : this.props.Language === 'Spanish' ? 'Minerales' : 'Minerały'       }
                                actions={MineralsDialogActions}
                                modal={false}
                                open={this.state.MineralsDialogOpen}
                                onRequestClose={this.handleMineralsDialogClose}
                                autoScrollBodyContent={true}
                                contentStyle={styles.contentStyle}
                                titleStyle={styles.titleStyle}
                                style={styles.dialogStyle}
                                contentStyle={styles.dialogContentStyle}
                              >

                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Calcium' : this.props.Language === 'Spanish' ? 'Calcio' : 'Wapń'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Calcium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Calcium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Chromium' : this.props.Language === 'Spanish' ? 'Cromo' : 'Chrom'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Chromium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Chromium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Copper' : this.props.Language === 'Spanish' ? 'Cobre' : 'Miedź'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Copper}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Copper: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Folate' : this.props.Language === 'Spanish' ? 'Folato' : 'Kwas foliowy'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Folate}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Folate: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Iron' : this.props.Language === 'Spanish' ? 'Hierro' : 'Żelazo'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Iron}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Iron: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Magnesium' : this.props.Language === 'Spanish' ? 'Magnesio' : 'Magnez'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Magnesium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Magnesium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Manganese' : this.props.Language === 'Spanish' ? 'Manganeso' : 'Mangan'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Manganese}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Manganese: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Phosphorus' : this.props.Language === 'Spanish' ? 'Fósforo' : 'Fosfor'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Phosphorus}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Phosphorus: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Potassium' : this.props.Language === 'Spanish' ? 'Potasio' : 'Potas'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Potassium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Potassium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Sodium' : this.props.Language === 'Spanish' ? 'Sodio' : 'Sód'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Sodium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Sodium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Selenium' : this.props.Language === 'Spanish' ? 'Selenio' : 'Selen'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Selenium}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Selenium: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Zinc' : this.props.Language === 'Spanish' ? 'Zinc' : 'Cynk'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.Zinc}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Zinc: e.target.value }) : null }
                                  style={styles.VitaminTextStyle}
                                />
                              </Dialog>

                            <br/>

                  <span className="ButtonGroup">
                        <RaisedButton label={this.props.Language === 'English' ? 'Fats' : this.props.Language === 'Spanish' ? 'Grasas' : 'Tłuszcze'       }
                          onClick={this.handleFatsDialogOpen}
                          backgroundColor="#5889d8"
                          className="FatsButton"
                  style={{marginLeft: '20%', marginRight: '20%', width: '60%', marginTop: '1%'}}
                          labelColor="white"  >
                        </RaisedButton>
                </span>
                        <Dialog
                                  title={this.props.Language === 'English' ? 'Fats' : this.props.Language === 'Spanish' ? 'Grasas' : 'Tłuszcze'       }
                                  actions={FatsDialogActions}
                                  modal={false}
                                  open={this.state.FatsDialogOpen}
                                  onRequestClose={this.handleFatsDialogClose}
                                  autoScrollBodyContent={true}
                                  contentStyle={styles.contentStyle}
                                  titleStyle={styles.titleStyle}
                                  style={styles.dialogStyle}
                                  contentStyle={styles.dialogContentStyle}
                                >
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Monounsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa monosaturada' : 'Kwasów tłuszczowych jednonienasyconych'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.MonounsaturatedFat}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ MonounsaturatedFat: e.target.value }) : null }
                                  style={styles.FatTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Polyunsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa poli-insaturada' : 'Tłuszcz wielonienasycony'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.PolyunsaturatedFat}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ PolyunsaturatedFat: e.target.value }) : null }
                                  style={styles.FatTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Saturated Fat' : this.props.Language === 'Spanish' ? 'Grasa saturada' : 'Tłuszcz nasycony'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.SaturatedFat}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ SaturatedFat: e.target.value }) : null }
                                  style={styles.FatTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Trans Fat' : this.props.Language === 'Spanish' ? 'Grasas trans' : 'Tłuszcze trans'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.TransFat}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ TransFat: e.target.value }) : null }
                                  style={styles.FatTextStyle}
                                />
                          <br/>
                                <TextField
                                  floatingLabelStyle={styles.labelStyle1}
                                  floatingLabelText={this.props.Language === 'English' ? 'Other Fats' : this.props.Language === 'Spanish' ? 'Otras grasas' : 'Pozostałe tłuszcze'       }
                                  underlineStyle={styles.underlineStyle}
                                  inputStyle={styles.inputStyle}
                                  value={this.state.OtherFats}
                                  onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ OtherFats: e.target.value }) : null }
                                  style={styles.FatTextStyle}
                                />
                        </Dialog>

                            <br/>

                <span className="ButtonGroup">
                  <RaisedButton label={this.props.Language === 'English' ? 'Carbohydrates' : this.props.Language === 'Spanish' ? 'Carbohidratos' : 'Węglowodany'       }
                    onClick={this.handleCarbohydratesDialogOpen}
                    backgroundColor="#5889d8"
                  style={{marginLeft: '20%', marginRight: '20%', width: '60%', marginTop: '1%'}}
                    className="CarbohydratesButton"
                    labelColor="white"  >
                  </RaisedButton>

                  <br/><br/>
                </span>

                  <Dialog
                            title={this.props.Language === 'English' ? 'Carbohydrates' : this.props.Language === 'Spanish' ? 'Carbohidratos' : 'Węglowodany'       }
                            actions={CarbohydratesDialogActions}
                            modal={false}
                            open={this.state.CarbohydratesDialogOpen}
                            onRequestClose={this.handleCarbohydratesDialogClose}
                            autoScrollBodyContent={true}
                            contentStyle={styles.contentStyle}
                            titleStyle={styles.titleStyle}
                            style={styles.dialogStyle}
                            contentStyle={styles.dialogContentStyle}
                          >
                          <br/>
                          <TextField
                            floatingLabelStyle={styles.labelStyle1}
                            floatingLabelText={this.props.Language === 'English' ? 'Fiber' : this.props.Language === 'Spanish' ? 'Fibra' : 'Błonnik'       }
                            underlineStyle={styles.underlineStyle}
                            inputStyle={styles.inputStyle}
                            value={this.state.Fiber}
                            onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Fiber: e.target.value }) : null }
                            style={styles.FatTextStyle}
                          />
                          <br/>
                          <TextField
                            floatingLabelStyle={styles.labelStyle1}
                            floatingLabelText={this.props.Language === 'English' ? 'Sugar' : this.props.Language === 'Spanish' ? 'Azúcar' : 'Cukier'       }
                            underlineStyle={styles.underlineStyle}
                            inputStyle={styles.inputStyle}
                            value={this.state.Sugar}
                            onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ Sugar: e.target.value }) : null }
                            style={styles.FatTextStyle}
                          />
                          <br/>
                          <TextField
                            floatingLabelStyle={styles.labelStyle1}
                            floatingLabelText={this.props.Language === 'English' ? 'Other Carbohydrates' : this.props.Language === 'Spanish' ? 'Otros carbohidratos' : 'Inne Węglowodany'       }
                            underlineStyle={styles.underlineStyle}
                            inputStyle={styles.inputStyle}
                            value={this.state.OtherCarbohydrates}
                            onChange={e => e.target.value.length < 12 && this.isFloat(e) === true ? this.setState({ OtherCarbohydrates: e.target.value }) : null }
                            style={styles.FatTextStyle}
                          />


                  </Dialog>






                  </div>




                </div>
              </div>
            </div>

            <br/>


          <div>
            <RaisedButton
              label={this.props.Language === 'English' ? 'Create' : this.props.Language === 'Spanish' ? 'Crear' : 'Stwórz'       }
              onClick={this.createIngredient}
              backgroundColor={'navy'}
              disabled={ (this.state.IngredientAmount == 0) || (this.state.IngredientDescription.length > 250) || (this.state.IngredientAmount.length < 1) || (this.state.IngredientName.length < 1) || (this.state.IngredientName.length > 50) || (this.state.username.IngredientName > 255) ? true : false }
              style={divStyle.activeStyleCreateButton}
              labelColor="white"  >
            </RaisedButton>

            <br/> <br/>


          </div>


            </div>
        );
    }
}
