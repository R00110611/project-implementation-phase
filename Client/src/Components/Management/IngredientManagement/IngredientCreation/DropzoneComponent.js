
import React from 'react'
var Dropzone = require('react-dropzone');

export default class DropzoneComponent extends React.Component {
    render() {
        return (
            <div className="Dropzone1">
            <Dropzone onDrop={this.props.onDrop} style={{backgroundColor: 'gray', width: '100%', height: '120px', color:'black', borderStyle: 'solid'}} activeStyle={{width: '100%', height: '120px', color:'black', borderColor:'black', borderStyle: 'solid', backgroundColor: 'red'}}>
              <div>
                {this.props.Language === 'English' ? 'Try dropping some ingredient photo here, or click to select the photo to upload. ' : this.props.Language === 'Spanish' ? 'Intente dejar caer alguna foto aquí, o haga clic para seleccionar las fotos que desea cargar.' : 'Spróbuj upuszczenie jakieś zdjęcie tutaj lub kliknij, aby wybrać zdjęcie do przesłania.'       }
              </div>
            </Dropzone>
            </div>
        );
    }
}
