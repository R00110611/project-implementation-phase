import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import CreateAnIngredient from './IngredientCreation/IngredientCreation';
import CompareIngredients from './IngredientComparison/IngredientComparison';
import SearchForIngredient from './IngredientSearch/IngredientSearch';

import AddIngredient from 'material-ui/svg-icons/content/add';
import SearchIngredient from 'material-ui/svg-icons/action/search';
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';

import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'

const styles = {
    activeStyle: {
      color: 'white',
      textTransform: 'none',


    },
    inactiveStyle: {
      color: '#a8adb5',
      textTransform: 'none'
    }
};

export default class IngredientManagementTabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CreateIngredientTabActive: true,
      SearchForIngredientTabActive: false,
      CompareIngredientsTabActive: false,
      IngredientsForComparison: []
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.addIngredientForComparison = this.addIngredientForComparison.bind(this);
    this.removeIngredientFromComparison = this.removeIngredientFromComparison.bind(this);
    this.addIngredientToFavourites = this.addIngredientToFavourites.bind(this);
  }



  handleTabChange(e) {
    e.props.value === 1 ? this.setState({CreateIngredientTabActive: true}) : this.setState({CreateIngredientTabActive: false})
    e.props.value === 2 ? this.setState({SearchForIngredientTabActive: true}) : this.setState({SearchForIngredientTabActive: false})
    e.props.value === 3 ? this.setState({CompareIngredientsTabActive: true}) : this.setState({CompareIngredientsTabActive: false})
  }

  addIngredientForComparison(ingredient) {
    var arr = this.state.IngredientsForComparison;
    var existsAlready = false;

    arr.map(Ingredient => {
      if(Ingredient.id === ingredient.id)
        existsAlready = true;
    });

    if (existsAlready === false) {
      arr.push(ingredient);
      this.setState({IngredientsForComparison: arr});

      var message = "";
      var title ="";
      if(this.props.Language == "English") {
        message = "Added ingredient " + ingredient.name +  " to the comparison, you can see it in the Compare Ingredients Tab!";
        title = "Success";
      }
      if(this.props.Language == "Spanish") {
        message = "Ingrediente añadido a la comparación, se puede ver en la pestaña Comparar ingredientes!";
        title = "Éxito";
      }
      if(this.props.Language == "Polish") {
        message = "Dodano składnik do porównania, można zobaczyć go w składnikach Porównaj Tab!";
        title = "Powodzenie";
      }

      toastr.success(message, title, {positionClass: 'toast-top-center', preventDuplicates: true});
    }

    if(existsAlready === true) {

      var message = "";
      var title ="";
      if(this.props.Language == "English") {
        message = "This ingredient already is in the Compare Tab!";
        title = "Error";
      }
      if(this.props.Language == "Spanish") {
        message = "Este ingrediente ya está en la pestaña Comparar!";
        title = "Error";
      }
      if(this.props.Language == "Polish") {
        message = "Ten składnik jest już w zakładce Porównaj!";
        title = "Błąd";
      }

      toastr.error(message, title, {positionClass: 'toast-top-center', preventDuplicates: true});
    }

  }

  addIngredientToFavourites(ingredient) {
    var outer = this;
    var axios = require('axios');

    var message = "";
    var title ="";
    if(this.props.Language == "English") {
      message = "Ingredient " + ingredient.name + " has been added to your favourite list!";
      title = "Success";
    }
    if(this.props.Language == "Spanish") {
      message = "Ingredient ha sido añadido a su lista de favoritos!";
      title = "Éxito";
    }
    if(this.props.Language == "Polish") {
      message = "Składnik został dodany do listy ulubionych!";
      title = "Powodzenie";
    }

    toastr.success(message, title, {positionClass: 'toast-top-center', preventDuplicates: true});

    axios.post('http://52.18.248.248:3001/AddIngredientToFavourites', {
        ingredient: ingredient,
        username: this.props.LoggedInUser
      })
      .then(function (response) {
      })
      .catch(function (error) {
      });

  }

  removeIngredientFromComparison(ingredientID) {
    var arr = this.state.IngredientsForComparison;

    arr = arr.filter(function( obj ) {
        return obj.id !== ingredientID;
    });

    this.setState({IngredientsForComparison: arr});
  }

    render() {
        return (
          <div className="tabs">
            <Tabs inkBarStyle={{background: 'white'}}  tabItemContainerStyle={{background: '#062b68'}}>

              <Tab label={this.props.Language === 'English' ? 'Create an Ingredient' : this.props.Language === 'Spanish' ? 'Crear un ingrediente' : 'Stworz skladnik'       } icon={<AddIngredient />} style={this.state.CreateIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={1}  >
                <CreateAnIngredient Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} />
              </Tab>

              <Tab label={this.props.Language === 'English' ? 'Search for Ingredients' : this.props.Language === 'Spanish' ? 'Búsqueda de Ingredientes' : 'Wyszukaj skladniki'       } icon={<SearchIngredient />} style={this.state.SearchForIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={2} >
                <SearchForIngredient Language={this.props.Language} addIngredientToFavourites={this.addIngredientToFavourites} LoggedInUser={this.props.LoggedInUser} addIngredientForComparison={this.addIngredientForComparison} />
              </Tab>

              <Tab label={this.props.Language === 'English' ? 'Compare Ingredients' : this.props.Language === 'Spanish' ? 'Comparar Ingredientes' : 'Porownaj skladniki'       } icon={<CompareIngredient />} style={this.state.CompareIngredientsTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={3} >
                <CompareIngredients Language={this.props.Language} IngredientsForComparison={this.state.IngredientsForComparison} RemoveIngredient={this.removeIngredientFromComparison} />
              </Tab>
            </Tabs>

          </div>
        );
    }
}
