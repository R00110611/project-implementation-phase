
import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete';

const divStyle = {
  activeStyle: {
    textAlign: 'center'
  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class IngredientComparison extends React.Component {
  constructor(props) {
		super(props);

		 this.state = {

     };

     this.ClickedRow =  this.ClickedRow.bind(this);
}

  ClickedRow(ingId) {

  }


    render() {
        return (
            <div>

            <div className="toDoDescription" style={this.props.IngredientsForComparison.length != 0 ? divStyle.inactiveStyle : divStyle.activeStyle}>
              {this.props.Language === 'English' ? 'Here you can compare ingredients, you can do that by adding ingredients in the search tab.' : this.props.Language === 'Spanish' ? 'Aquí puedes comparar ingredientes, puedes hacerlo añadiendo ingredientes en la pestaña de búsqueda.' : 'Tu można porównać składniki, można to zrobić przez dodanie składników w zakładce wyszukiwania.'       }
            </div>

            <div className="CompareIngredinets" style={this.props.IngredientsForComparison.length == 0 ? divStyle.inactiveStyle : divStyle.activeStyle} >

                { this.props.IngredientsForComparison.map(Ingredient =>
                  <tr className="table1">
                      <div className="RemoveButton">
                        <DeleteIcon className="RemoveIcon" onClick={ this.props.RemoveIngredient.bind(null, Ingredient.id)} />
                      </div>

                      <div className="IngDescription">
                        <h2> {this.props.Language === 'English' ? 'Description' : this.props.Language === 'Spanish' ? 'Descripción' : 'Opis'       } </h2>
                         <img src={Ingredient.photoURL != null || Ingredient.photoURL != undefined ? "http://52.18.248.248:3001/" + Ingredient.photoURL : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="50" width="50" />
                         <br/>
                          {Ingredient.name}
                         <br/>
                      </div>

                      <div className="IngInfo">
                      <h2> {this.props.Language === 'English' ? 'Energy' : this.props.Language === 'Spanish' ? 'Energía' : 'Energia'       } </h2>
                        {this.props.Language === 'English' ? 'Calories' : this.props.Language === 'Spanish' ? 'Calorías' : 'Kalorie'       } {Ingredient.Calories}
                         <br/>
                        {this.props.Language === 'English' ? 'Amount' : this.props.Language === 'Spanish' ? 'Cantidad' : 'Ilosc'       } {Ingredient.Amount}
                      </div>

                      <div className="VitaminsInfo">
                      <h2> {this.props.Language === 'English' ? 'Vitamins' : this.props.Language === 'Spanish' ? 'Vitaminas' : 'Witaminy'       } </h2>
                         A {Ingredient.VitaminA}
                         <br/>
                         B1 {Ingredient.VitaminB1}
                         <br/>
                         B2 {Ingredient.VitaminB2}
                         <br/>
                         B3 {Ingredient.VitaminB3}
                         <br/>
                         B5 {Ingredient.VitaminB5}
                         <br/>
                         B6 {Ingredient.VitaminB6}
                         <br/>
                         B9 {Ingredient.VitaminB9}
                         <br/>
                         B12 {Ingredient.VitaminB12}
                         <br/>
                         C {Ingredient.VitaminC}
                         <br/>
                         D {Ingredient.VitaminD}
                         <br/>
                         E {Ingredient.VitaminE}
                         <br/>
                         K {Ingredient.VitaminK}

                      </div>

                      <div className="MineralsInfo">
                      <h2> {this.props.Language === 'English' ? 'Minerals' : this.props.Language === 'Spanish' ? 'Minerales' : 'Minerały'       } </h2>
                       {this.props.Language === 'English' ? 'Calcium' : this.props.Language === 'Spanish' ? 'Calcio' : 'Wapń'       } {Ingredient.Calcium}
                         <br/>
                       {this.props.Language === 'English' ? 'Chromium' : this.props.Language === 'Spanish' ? 'Cromo' : 'Chrom'       } {Ingredient.Chromium}
                         <br/>
                       {this.props.Language === 'English' ? 'Copper' : this.props.Language === 'Spanish' ? 'Cobre' : 'Miedź'       } {Ingredient.Copper}
                         <br/>
                       {this.props.Language === 'English' ? 'Folate' : this.props.Language === 'Spanish' ? 'Folato' : 'Kwas foliowy'       } {Ingredient.Folate}
                         <br/>
                       {this.props.Language === 'English' ? 'Iron' : this.props.Language === 'Spanish' ? 'Hierro' : 'Żelazo'       } {Ingredient.Iron}
                         <br/>
                       {this.props.Language === 'English' ? 'Magnesium' : this.props.Language === 'Spanish' ? 'Magnesio' : 'Magnez'       } {Ingredient.Magnesium}
                         <br/>
                       {this.props.Language === 'English' ? 'Manganese' : this.props.Language === 'Spanish' ? 'Manganeso' : 'Mangan'       } {Ingredient.Manganese}
                         <br/>
                       {this.props.Language === 'English' ? 'Phosphorus' : this.props.Language === 'Spanish' ? 'Fósforo' : 'Fosfor'       } {Ingredient.Phosphorus}
                         <br/>
                       {this.props.Language === 'English' ? 'Potassium' : this.props.Language === 'Spanish' ? 'Potasio' : 'Potas'       } {Ingredient.Potassium}
                         <br/>
                       {this.props.Language === 'English' ? 'Sodium' : this.props.Language === 'Spanish' ? 'Sodio' : 'Sód'       } {Ingredient.Sodium}
                         <br/>
                       {this.props.Language === 'English' ? 'Selenium' : this.props.Language === 'Spanish' ? 'Selenio' : 'Selen'       } {Ingredient.Selenium}
                         <br/>
                       {this.props.Language === 'English' ? 'Zinc' : this.props.Language === 'Spanish' ? 'Zinc' : 'Cynk'       } {Ingredient.Zinc}

                      </div>

                      <div className="FatsInfo">
                        <h2> {this.props.Language === 'English' ? 'Fats' : this.props.Language === 'Spanish' ? 'Grasas' : 'Tłuszcze'       } </h2>
                         {this.props.Language === 'English' ? 'Monounsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa monosaturada' : 'kwasów tłuszczowych jednonienasyconych'       } {Ingredient.MonounsaturatedFat}
                         <br/>
                         {this.props.Language === 'English' ? 'Polyunsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa poli-insaturada' : 'Tłuszcz wielonienasycony'       } {Ingredient.PolyunsaturatedFat}
                         <br/>
                         {this.props.Language === 'English' ? 'Saturated Fat' : this.props.Language === 'Spanish' ? 'Grasa saturada' : 'Tłuszcz nasycony'       } {Ingredient.SaturatedFat}
                         <br/>
                         {this.props.Language === 'English' ? 'Trans Fat' : this.props.Language === 'Spanish' ? 'Grasas trans' : 'Tłuszcze trans'       } {Ingredient.TransFat}
                         <br/>
                         {this.props.Language === 'English' ? 'Other Fats' : this.props.Language === 'Spanish' ? 'Otras grasas' : 'Pozostałe tłuszcze'       } {Ingredient.OtherFats}

                      </div>

                      <div className="CarbohydratesInfo">
                      <h2> {this.props.Language === 'English' ? 'Carbohydrates' : this.props.Language === 'Spanish' ? 'Carbohidratos' : 'Węglowodany'       } </h2>
                         {this.props.Language === 'English' ? 'Fiber' : this.props.Language === 'Spanish' ? 'Fibra' : 'Błonnik'       } {Ingredient.Fiber}
                         <br/>
                         {this.props.Language === 'English' ? 'Sugar' : this.props.Language === 'Spanish' ? 'Azúcar' : 'Cukier'       } {Ingredient.Sugar}
                         <br/>
                         {this.props.Language === 'English' ? 'Other Carbohydrates' : this.props.Language === 'Spanish' ? 'Otros carbohidratos' : 'Inny Węglowodany'       } {Ingredient.OtherCarbohydrates}


                      </div>

                      <div className="OtherInfo">
                      <h2> {this.props.Language === 'English' ? 'Other' : this.props.Language === 'Spanish' ? 'Otro' : 'Inny'       } </h2>
                         {this.props.Language === 'English' ? 'Cholesterol' : this.props.Language === 'Spanish' ? 'Colesterol' : 'cholesterol'       } {Ingredient.Cholesterol}
                         <br/>
                         {this.props.Language === 'English' ? 'Protein' : this.props.Language === 'Spanish' ? 'Colesterol' : 'Białko'       } {Ingredient.Protein}

                      </div>






                  </tr>
                )}

              </div>


            </div>
        );
    }
}
