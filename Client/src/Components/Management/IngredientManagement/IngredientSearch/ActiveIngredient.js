import React from 'react'
import IngredientReviews from './IngredientReviews'
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';


const styles = {
  activeStyle: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle: {
    cursor: 'pointer'
  }
}

const divStyles = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  }

}

export default class ActiveIngredient extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
      NutritionalInformationActive: true,
      ReviewsActive: false

		};


    this.activate = this.activate.bind(this);
}

activate(num) {
  if(num === 1) {
    this.setState({NutritionalInformationActive: true, ReviewsActive: false });
  } else {
    this.setState({NutritionalInformationActive: false, ReviewsActive: true });
  }
}

    render() {
        return (
            <div>

            <div className="goBack">
              <h2 style={{fontWeight: 'bold', color:'black', textAlign: 'center', cursor: 'pointer'}} onClick={this.props.goBack} > Go back to Ingredient Search </h2>
            </div>

            <div className="navigation">
            <div className="IngredientInformations">

            <div className="navbar">

              <span onClick={this.activate.bind(null, 1)} style={this.state.NutritionalInformationActive === true ? styles.activeStyle : styles.inactiveStyle}> {this.props.Language === 'English' ? 'Ingredient Information' : this.props.Language === 'Spanish' ? 'Información de Ingredientes' : 'Informacje o składniku'} </span>
              or
                            <span onClick={this.activate.bind(null, 2)} style={this.state.ReviewsActive === true ? styles.activeStyle : styles.inactiveStyle}> {this.props.Language === 'English' ? 'Ingredient Reviews' : this.props.Language === 'Spanish' ? 'Información de Ingredientes' : 'Informacje o składniku'} </span>
                            <br/>
            </div>

              <br/>


              <div style={this.state.ReviewsActive === true ? divStyles.activeStyle : divStyles.inactiveStyle}>
                  <IngredientReviews Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} Ingredient={this.props.Ingredient} Reviews={this.props.Reviews} state={this.props.state} ActiveIngredient={this.props.Ingredient} rowClick={this.props.rowClick} />
              </div>

              <div className="IngredientDescription1" style={this.state.NutritionalInformationActive === true ? divStyles.activeStyle : divStyles.inactiveStyle} >
              <h2> {this.props.Language === 'English' ? 'Ingredient Description' : this.props.Language === 'Spanish' ? 'Descripción del ingrediente' : 'Opis skladnika'       } </h2>

                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                                            <br/>
                    <li> <img src={this.props.Ingredient.photoURL != null || this.props.Ingredient.photoURL != undefined ? "http://52.18.248.248:3001/" + this.props.Ingredient.photoURL : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="110" width="130" />  </li>
                    <li> <span style={{fontWeight: 'bold'}} > {this.props.Language === 'English' ? 'Ingredient Name' : this.props.Language === 'Spanish' ? 'Nombre del ingrediente' : 'Nazwa składnika'       } </span> {this.props.Ingredient.name} </li>
                    <li> <span style={{fontWeight: 'bold'}} >{this.props.Language === 'English' ? 'Creator' : this.props.Language === 'Spanish' ? 'Creador' : 'Twórca'       }</span> {this.props.Ingredient.Username}  </li>
                    <li> <span style={{fontWeight: 'bold'}} > {this.props.Language === 'English' ? 'Description' : this.props.Language === 'Spanish' ? 'Descripción' : 'Opis'       } </span>{this.props.Ingredient.Description}  </li>
                    <br/>
                  </div>
                  <br/>
                </div>



              <br/>

              <div className="IngredientNutritionalInformation" style={this.state.NutritionalInformationActive === true ? divStyles.activeStyle : divStyles.inactiveStyle}>

              <h2 className="h2W"> {this.props.Language === 'English' ? 'Ingredient Nutritional Information' : this.props.Language === 'Spanish' ? 'Ingredientes Información Nutricional' : 'Składnik Wartość odżywcza'       } </h2>
                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Energy' : this.props.Language === 'Spanish' ? 'Energía' : 'Energia'       } </h3>

                  <li> {this.props.Language === 'English' ? 'Calories' : this.props.Language === 'Spanish' ? 'Calorías' : 'Kalorie'       } {this.props.Ingredient.Calories} </li>
                  <li> {this.props.Language === 'English' ? 'Amount' : this.props.Language === 'Spanish' ? 'Cantidad' : 'Ilosc'       } {this.props.Ingredient.Amount} </li>
                  </div>

                  <br/>

                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Vitamins' : this.props.Language === 'Spanish' ? 'Vitaminas' : 'Witaminy'       } </h3>

                  <li> A {this.props.Ingredient.VitaminA}</li>
                  <li> B1 {this.props.Ingredient.VitaminB1}</li>
                  <li> B2 {this.props.Ingredient.VitaminB2}</li>
                  <li> B3 {this.props.Ingredient.VitaminB3}</li>
                  <li> B5 {this.props.Ingredient.VitaminB5}</li>
                  <li> B6 {this.props.Ingredient.VitaminB6}</li>
                  <li> B9 {this.props.Ingredient.VitaminB9}</li>
                  <li> B12 {this.props.Ingredient.VitaminB12}</li>
                  <li> C {this.props.Ingredient.VitaminC}</li>
                  <li> D {this.props.Ingredient.VitaminD}</li>
                  <li> E {this.props.Ingredient.VitaminE}</li>
                  <li> K {this.props.Ingredient.VitaminK}</li>
                  </div>
                  <br/>
                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Minerals' : this.props.Language === 'Spanish' ? 'Minerales' : 'Minerały'       } </h3>

                <li> {this.props.Language === 'English' ? 'Calcium' : this.props.Language === 'Spanish' ? 'Calcio' : 'Wapń'       } {this.props.Ingredient.Calcium}</li>
                <li> {this.props.Language === 'English' ? 'Chromium' : this.props.Language === 'Spanish' ? 'Cromo' : 'Chrom'       } {this.props.Ingredient.Chromium}</li>
                <li> {this.props.Language === 'English' ? 'Copper' : this.props.Language === 'Spanish' ? 'Cobre' : 'Miedź'       } {this.props.Ingredient.Copper}</li>
                <li> {this.props.Language === 'English' ? 'Folate' : this.props.Language === 'Spanish' ? 'Folato' : 'Kwas foliowy'       } {this.props.Ingredient.Folate}</li>
                <li> {this.props.Language === 'English' ? 'Iron' : this.props.Language === 'Spanish' ? 'Hierro' : 'Żelazo'       } {this.props.Ingredient.Iron}</li>
                <li> {this.props.Language === 'English' ? 'Magnesium' : this.props.Language === 'Spanish' ? 'Magnesio' : 'Magnez'       } {this.props.Ingredient.Magnesium}</li>
                <li> {this.props.Language === 'English' ? 'Manganese' : this.props.Language === 'Spanish' ? 'Manganeso' : 'Mangan'       } {this.props.Ingredient.Manganese}</li>
                <li> {this.props.Language === 'English' ? 'Phosphorus' : this.props.Language === 'Spanish' ? 'Fósforo' : 'Fosfor'       } {this.props.Ingredient.Phosphorus}</li>
                <li> {this.props.Language === 'English' ? 'Potassium' : this.props.Language === 'Spanish' ? 'Potasio' : 'Potas'       } {this.props.Ingredient.Potassium}</li>
                <li> {this.props.Language === 'English' ? 'Sodium' : this.props.Language === 'Spanish' ? 'Sodio' : 'Sód'       } {this.props.Ingredient.Sodium}</li>
                <li> {this.props.Language === 'English' ? 'Selenium' : this.props.Language === 'Spanish' ? 'Selenio' : 'Selen'       } {this.props.Ingredient.Selenium}</li>
                <li> {this.props.Language === 'English' ? 'Zinc' : this.props.Language === 'Spanish' ? 'Zinc' : 'Cynk'       } {this.props.Ingredient.Zinc}</li>
                </div>
                  <br/>
                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Fats' : this.props.Language === 'Spanish' ? 'Grasas' : 'Tłuszcze'       } </h3>

                <li> {this.props.Language === 'English' ? 'Monounsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa monosaturada' : 'kwasów tłuszczowych jednonienasyconych'       } {this.props.Ingredient.MonounsaturatedFat}</li>
                <li> {this.props.Language === 'English' ? 'Polyunsaturated Fat' : this.props.Language === 'Spanish' ? 'Grasa poli-insaturada' : 'Tłuszcz wielonienasycony'       } {this.props.Ingredient.PolyunsaturatedFat}</li>
                <li> {this.props.Language === 'English' ? 'Saturated Fat' : this.props.Language === 'Spanish' ? 'Grasa saturada' : 'Tłuszcz nasycony'       } {this.props.Ingredient.SaturatedFat}</li>
                <li> {this.props.Language === 'English' ? 'Trans Fat' : this.props.Language === 'Spanish' ? 'Grasas trans' : 'Tłuszcze trans'       } {this.props.Ingredient.TransFat}</li>
                <li> {this.props.Language === 'English' ? 'Other Fats' : this.props.Language === 'Spanish' ? 'Otras grasas' : 'Pozostałe tłuszcze'       } {this.props.Ingredient.OtherFats}</li>
                </div>
                  <br/>
                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Carbohydrates' : this.props.Language === 'Spanish' ? 'Carbohidratos' : 'Węglowodany'       } </h3>

                  <li> {this.props.Language === 'English' ? 'Fiber' : this.props.Language === 'Spanish' ? 'Fibra' : 'Błonnik'       } {this.props.Ingredient.Fiber}</li>
                  <li> {this.props.Language === 'English' ? 'Sugar' : this.props.Language === 'Spanish' ? 'Azúcar' : 'Cukier'       } {this.props.Ingredient.Sugar}</li>
                  <li> {this.props.Language === 'English' ? 'Other Carbohydrates' : this.props.Language === 'Spanish' ? 'Otros carbohidratos' : 'Inny Węglowodany'       } {this.props.Ingredient.OtherCarbohydrates}</li>
                  </div>
                  <br/>
                  <div style={{borderRadius:'20px', border: '2px solid DimGrey', backgroundColor: 'gray', width: '80%', marginLeft: '10%', marginRight: '10%'}} >
                <h3> {this.props.Language === 'English' ? 'Other' : this.props.Language === 'Spanish' ? 'Otro' : 'Inny'       } </h3>

                  <li> {this.props.Language === 'English' ? 'Cholesterol' : this.props.Language === 'Spanish' ? 'Colesterol' : 'cholesterol'       } {this.props.Ingredient.Cholesterol}</li>
                  <li> {this.props.Language === 'English' ? 'Protein' : this.props.Language === 'Spanish' ? 'Colesterol' : 'Białko'       } {this.props.Ingredient.Protein}</li>
                  </div>
                  <br/>

              </div>
              <br/>
            </div>
            <br/>
              </div>
              <br/> <br/>






            </div>
        );
    }
}
