import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import TextField from 'material-ui/TextField';

const styles = {
  block: {
    maxWidth: 250,
  },
  radioButton: {
    marginBottom: 16,
    width: '90%'
  },
  textAreaFloatingLabelStyle: {
  color: 'white',
  textAlign: 'left',
  width: '100%'
}
};
const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}
export default class IngredientReviews extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
      textArea: "",
      Recommended: 0,
      selected: "Positive"
		};

    this.addReview = this.addReview.bind(this);
    this.formatDate = this.formatDate.bind(this);
}

addReview() {
  var recc = 0;

  if(this.state.selected === "Positive") {
    recc = 1;
  } else {
    recc = 0;
  }

  var outer = this;
  var axios = require('axios');

  axios.post('http://52.18.248.248:3001/AddIngredientReview', {
    IngredientID: this.props.Ingredient.id,
    Username: this.props.LoggedInUser,
    Review: this.state.textArea,
    Recommended: recc,
    })
    .then(function (response) {
      if(response.data.response === true) {
        outer.props.rowClick(outer.props.ActiveIngredient);
        outer.setState({textArea: ""});
      }
      else {

      }

    })
    .catch(function (error) {
      console.log(error);
    });

}

formatDate(date){
  // return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  var dateFormatted = "";
  dateFormatted = date.replace("T", " ");
  dateFormatted = dateFormatted.replace("Z", "");
  dateFormatted = dateFormatted.substring(0, 19);
  return dateFormatted;
}

    render() {
        return (
            <div className="Reviews">

              <div className="reviewsOfIngredient">
                <h2> Reviews of the ingredient </h2>
                <table style={{marginLeft: '5%', marginRight: '5%', width: '90%'}} className="ReviewTable">

                <tr style={{backgroundColor: '#21262d', color: 'white'}}>
                <th> {this.props.Language === 'English' ? 'Review' : this.props.Language === 'Spanish' ? 'revisión' : 'Recenzja'       } </th>
                <th> {this.props.Language === 'English' ? 'Username' : this.props.Language === 'Spanish' ? 'Nombre de usuario' : 'Nazwa Użytkownika'       } </th>
                <th> {this.props.Language === 'English' ? 'Type of Review' : this.props.Language === 'Spanish' ? 'Tipo de revisión' : 'Typ recenzji'       } </th>
                <th> {this.props.Language === 'English' ? 'Date' : this.props.Language === 'Spanish' ? 'Cuando' : 'Kiedy'       } </th>
                </tr>

                  {this.props.Reviews.map(ReviewInd =>
                    <tr className="ReviewRow" style={{backgroundColor: '#38404f'}} >
                        <td className="ReviewTableDate"> {ReviewInd.Review} </td>
                        <td> {ReviewInd.Username} </td>
                        <td> {ReviewInd.Recommended === 0 ? this.props.Language === 'English' ? 'Negative' : this.props.Language === 'Spanish' ? 'Negativo' : 'Negatywny'        : this.props.Language === 'English' ? 'Positive' : this.props.Language === 'Positivo' ? 'Pozytywny' : 'Uzytkownik'                             } </td>
                        <td> {this.formatDate(ReviewInd.TimeAndDate).toString()} </td>
                    </tr>
                  )}
                </table>
                <br/>
              </div>

              <br/>

              <div className="addReview">
                <h2> {this.props.Language === 'English' ? 'Add review' : this.props.Language === 'Spanish' ? 'añadir una opinión' : 'Dodaj opinie'       } </h2>


                <div>

                <textarea value={this.state.textArea} onChange={e => this.setState({ textArea: e.target.value })} style={{resize: 'vertical', textAlign: 'center', width:'60%', marginLeft: '20%', marginRight: '20%'}} placeholder={this.props.Language === 'English' ? 'Review' : this.props.Language === 'Spanish' ? 'revisión' : 'Recenzja'       }>

                </textarea>
                <br/>
                <span style={this.state.textArea.length > 250 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.textArea.length}/250 </span>
                </div>

                <br/><br/>

                <div className="RadioButtons2">
                  <RadioButtonGroup defaultSelected={"Positive"} onChange={e => this.setState({ selected: e.target.value })}      >
                        <RadioButton
                          value="Positive"
                          label="Positive"
                          style={styles.radioButton}
                        />
                        <RadioButton
                          value="Negative"
                          label="Negative"
                          style={styles.radioButton}
                        />
                  </RadioButtonGroup>
                </div>




                <br/>
                <RaisedButton backgroundColor="#0a1c35" labelColor='white' disabled={ this.state.textArea.length > 250 ? true : false } onClick={this.addReview} label={this.props.Language === 'English' ? 'Add' : this.props.Language === 'Spanish' ? 'Añadir' : 'Dodaj'       } />
                <br/><br/>


              </div>

            </div>
        );
    }
}
