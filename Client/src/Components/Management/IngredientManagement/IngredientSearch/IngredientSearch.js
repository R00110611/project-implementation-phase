import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import ActiveIngredient from './ActiveIngredient'
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import UpArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import DownArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';
import Favourite from 'material-ui/svg-icons/action/grade';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import See from 'material-ui/svg-icons/action/zoom-in'


const styles = {
  activeRow: {
    backgroundColor: 'white'
  },
  block: {
    maxWidth: 250,
  },
  checkbox: {
    marginBottom: 16,
  },
}


const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  }
}


export default class IngredientSearch extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
      IngredientList: [],
      name: "",
      activeIngredient: 0,
      actIngredient: {},
      IngredientReviews: [],
      onlyMineIngredients: false,
      Language: "All",
      LanguageValue: 1,
      onlyFavouriteIngredients: false,
      searchMade: 0
		};

    this.search = this.search.bind(this);
    this.rowClick = this.rowClick.bind(this);
    this.getReviews = this.getReviews.bind(this);
    this.check = this.check.bind(this);
    this.check1 = this.check1.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleLanguageChange = this.handleLanguageChange.bind(this);
    this.sortCaloriesAscending = this.sortCaloriesAscending.bind(this);
    this.sortCaloriesDescending = this.sortCaloriesDescending.bind(this);
    this.sortFiberAscending = this.sortFiberAscending.bind(this);
    this.sortFiberDescending = this.sortFiberDescending.bind(this);
    this.goBack = this.goBack.bind(this);

}

goBack() {
  this.setState({activeIngredient: 0})
}

sortCaloriesAscending() {
  var arr = this.state.IngredientList;

  arr.sort(function(a, b) {
      return a.Calories - b.Calories;
  });

  this.setState({IngredientList: arr});
}

sortCaloriesDescending() {
  var arr = this.state.IngredientList;
  arr.sort(function (a, b) {
    return b.Calories - a.Calories;
  });

  this.setState({IngredientList: arr});
}

sortFiberAscending() {

  var arr = this.state.IngredientList;

  arr.sort(function(a, b) {
      return a.Fiber - b.Fiber;
  });

  this.setState({IngredientList: arr});

}

sortFiberDescending() {

  var arr = this.state.IngredientList;
  arr.sort(function (a, b) {
    return b.Fiber - a.Fiber;
  });

  this.setState({IngredientList: arr});

}

handleLanguageChange(event, index, val) {
  event.target.value === 'All' ? this.setState({LanguageValue: 1, Language: 'All'}, this.search) : null;
  event.target.value === 'English' ? this.setState({LanguageValue: 2, Language: 'English'}, this.search) : null;
  event.target.value === 'Spanish' ? this.setState({LanguageValue: 3, Language: 'Spanish'}, this.search) : null;
  event.target.value === 'Polish' ? this.setState({LanguageValue: 4, Language: 'Polish'}, this.search) : null;
}

  handleNameChange(e) {
    this.setState({name: e.target.value}, this.search);
  }

check(event) {
  const target = event.target;
  const value = target.type === 'checkbox' ? target.checked : target.value;
  const name = target.name;

  name === "Mine" ? value === true ? this.setState({onlyMineIngredients: true}, this.search) : this.setState({onlyMineIngredients: false}, this.search) : value === true ? this.setState({onlyFavouriteIngredients: true}, this.search) : this.setState({onlyFavouriteIngredients: false}, this.search)
}

check1(e, isc) {
  this.setState({onlyFavouriteIngredients: isc}, this.search);
}


rowClick(Ingredient) {
  this.getReviews(Ingredient);
  this.setState({actIngredient: Ingredient});
  this.setState({activeIngredient: Ingredient.id});
}

getReviews(Ingredient) {
  var outer = this;
  var axios = require('axios');

  axios.post('http://52.18.248.248:3001/IngredientReviews', {
      IngredientID: Ingredient.id
    })
    .then(function (response) {
      var arr = [];
      response.data.response.map(function(name, index) {
           arr.push({
             ID: name.ID,
             IngredientID: name.IngredientID,
             Username: name.Username,
             UsernameID: name.UsernameID,
             Review: name.Review,
             Recommended: name.Recommended,
             TimeAndDate: name.TimeAndDate

           });
      })

      outer.setState({IngredientReviews: arr});
    })
    .catch(function (error) {
    });


}


  search() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForIngredient', {
        name: this.state.name,
        myIngredients: this.state.onlyMineIngredients,
        username: this.props.LoggedInUser,
        IngredientLangauge: this.state.Language,
        onlyFavouriteIngredients: this.state.onlyFavouriteIngredients
      })
      .then(function (response) {
        var arr = [];
        response.data.response.map(function(name, index) {
             arr.push({id: name.ID, name: name.Name, photoURL: name.Photo, Username: name.Username, Description: name.Description, Language: name.Language, Calories: name.Calories, Amount: name.Amount, VitaminA: name.VitaminA, VitaminB1: name.VitaminB1, VitaminB2: name.VitaminB2, VitaminB3: name.VitaminB3, VitaminB5: name.VitaminB5, VitaminB6: name.VitaminB6, VitaminB9: name.VitaminB9, VitaminB12: name.VitaminB12, VitaminC: name.VitaminC, VitaminD: name.VitaminD, VitaminE: name.VitaminE, VitaminK: name.VitaminK, Calcium: name.Calcium, Chromium: name.Chromium, Copper: name.Copper, Folate: name.Folate, Iron: name.Iron, Magnesium: name.Magnesium, Manganese: name.Manganese, Phosphorus: name.Phosphorus, Potassium: name.Potassium, Sodium: name.Sodium, Selenium: name.Selenium, Zinc: name.Zinc, MonounsaturatedFat: name.MonounsaturatedFat, PolyunsaturatedFat: name.PolyunsaturatedFat, SaturatedFat: name.SaturatedFat, TransFat: name.TransFat, OtherFats: name.OtherFats, Fiber: name.Fiber, Sugar: name.Sugar, OtherCarbohydrates: name.OtherCarbohydrates, Cholesterol: name.Cholesterol, Protein: name.Protein });
        })
        outer.setState({IngredientList: arr, searchMade: 1});
      })
      .catch(function (error) {
      });
  }

    render() {
        return (
            <div>
                        <div className="searchBox1" style={this.state.activeIngredient === 0 ? divStyle.activeStyle : divStyle.inactiveStyle} >
                            <br/>

                            <input onChange={e => this.setState({ name: e.target.value }, this.search)} type="text" value={this.state.IngredientName} placeholder={this.props.Language === 'English' ? 'Ingredient Name' : this.props.Language === 'Spanish' ? 'Nombre' : 'Nazwa'       } style={{textAlign: 'center'}} />
                            <br/> <br/>

                                    <div>
                                    Only mine ingredients:
                                    <input
                                      name="Mine"
                                      type="checkbox"
                                      checked={this.state.onlyMineIngredients}
                                      onChange={this.check}
                                    />

                                    <br/>

                                    Only favourite ingredients:
                                    <input
                                      name="Favourite"
                                      type="checkbox"
                                      checked={this.state.onlyFavouriteIngredients}
                                      onChange={this.check}
                                    />




                                    </div>


                            <span style={{color: 'black'}}> {this.props.Language === 'English' ? 'Language' : this.props.Language === 'Spanish' ? 'Idioma' : 'Język'} </span>

                            <select onChange={this.handleLanguageChange} >
                              <option value="All" selected={this.state.IngredientLanguage==="All" ? true : false} > {this.props.Language === 'English' ? 'All' : this.props.Language === 'Spanish' ? 'todas' : 'Wszystkie'} </option>
                              <option value="English" selected={this.state.IngredientLanguage==="English" ? true : false} > {this.props.Language === 'English' ? 'English' : this.props.Language === 'Spanish' ? 'Inglés' : 'Angielski'} </option>
                              <option value="Polish" selected={this.state.IngredientLanguage==="Polish" ? true : false}  > {this.props.Language === 'English' ? 'Polish' : this.props.Language === 'Spanish' ? 'Polaco' : 'Polski'} </option>
                              <option value="Spanish" selected={this.state.IngredientLanguage==="Spanish" ? true : false}  > {this.props.Language === 'English' ? 'Spanish' : this.props.Language === 'Spanish' ? 'Español' : 'Hiszpanski'} </option>
                            </select>
<br/><br/>
                      </div>



                      <div className="ingredientBox1" style={this.state.searchMade === 1 && this.state.activeIngredient === 0 ? divStyle.activeStyle : divStyle.inactiveStyle} >
                        <table className="tableOfIngredients1">
                          <tr>
                            <th> {this.props.Language === 'English' ? 'Image' : this.props.Language === 'Spanish' ? 'Imagen' : 'Zdjecie'       }  </th>
                            <th> {this.props.Language === 'English' ? 'Name' : this.props.Language === 'Spanish' ? 'Nombre' : 'Nazwa'       } </th>
                            <th> {this.props.Language === 'English' ? 'Amount' : this.props.Language === 'Spanish' ? 'Cantidad' : 'Ilosc'       } </th>
                            <th> {this.props.Language === 'English' ? 'Calories' : this.props.Language === 'Spanish' ? 'Calorías' : 'Kalorie'       } <div> <UpArrow style={{cursor: 'pointer'}} onClick={this.sortCaloriesAscending}/>    <DownArrow style={{cursor: 'pointer'}} onClick={this.sortCaloriesDescending} /> </div>     </th>

                          </tr>



                          {this.state.IngredientList.map(Ingredient =>
                            <tr style={ this.state.activeIngredient === Ingredient.id ? styles.activeRow : null  }    >
                              <td>  <img src={Ingredient.photoURL != null || Ingredient.photoURL != undefined ? "http://52.18.248.248:3001/" + Ingredient.photoURL : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="50" width="50" />      </td>
                              <td> {Ingredient.name} </td>
                              <td> {Ingredient.Amount} </td>
                              <td> {Ingredient.Calories} </td>
                              <td> <CompareIngredient style={{cursor: 'pointer'}} onClick={this.props.addIngredientForComparison.bind(null, Ingredient)}/> </td>
                              <td> <Favourite style={{cursor: 'pointer'}} onClick={this.props.addIngredientToFavourites.bind(null, Ingredient)} /> </td>
                              <td> <See style={{cursor: 'pointer'}} onClick={this.rowClick.bind(null, Ingredient)}/> </td>
                            </tr>


                          )}
                        </table>
                      </div>


                      <div className="selectedIngredient1" style={this.state.searchMade === 1 && this.state.activeIngredient != 0 ? divStyle.activeStyle : divStyle.inactiveStyle}>
                         {this.state.activeIngredient === 0 ? null       : <ActiveIngredient goBack={this.goBack} Language={this.props.Language} Ingredient={this.state.actIngredient} Reviews={this.state.IngredientReviews} LoggedInUser={this.props.LoggedInUser} rowClick={this.rowClick} />}
                       </div>

                        <br/>






            </div>
        );
    }
}
