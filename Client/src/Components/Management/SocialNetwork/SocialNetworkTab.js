
import React from 'react'
import SearchForUsers from './SearchForUsers/SearchForUsers'
import UserProperties from './UserProperties/UserProperties'
import LastVisits from './LastVisits/LastVisits'

export default class SocialNetworkTab extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      LoggedInUser: this.props.LoggedInUser,
      ProfileOfUser: this.props.LoggedInUser,
      LastVisitorsLoaded: false,
      LastVisitors: [],
      CurrentUser: {
        Username: null,
        Height: null,
        Weight: null,
        emailAddress: null,
        Photo: null,
        RegistrationDate: null,
        FirstName: null,
        MiddleName: null,
        LastName: null,
        UserProfileProperties: {
          Ingredients: {
            CreatedIngredients: [{Name: 'testing'}],
            FavouriteIngredients: []
          }
        }
      },
      CurrentUserLoaded: false
    }
    this.setProfileOfUser = this.setProfileOfUser.bind(this);
    this.getLastVisitors = this.getLastVisitors.bind(this);
    this.loadUserProperties = this.loadUserProperties.bind(this);
    this.setPhoto = this.setPhoto.bind(this);
    this.removePhoto = this.removePhoto.bind(this);

  }

  componentWillMount() {
    this.setProfileOfUser(this.props.LoggedInUser);
  }

  setProfileOfUser(user) {
    var outer = this;
    this.setState({LastVisitorsLoaded: false, CurrentUserLoaded: false},   this.setState({ ProfileOfUser: user }, () => outer.getLastVisitors(user), outer.loadUserProperties(user)));
  }

  setPhoto(photoURL) {
    const newState = {...this.state.CurrentUser}
    newState.Photo=photoURL.response;
    this.setState({CurrentUser: newState});
  }

  removePhoto() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/updateUserPhoto2', {
        username: this.state.ProfileOfUser
      })
      .then(function (response) {
        const newState = {...outer.state.CurrentUser}
        newState.Photo=null
        outer.setState({CurrentUser: newState});
      })
      .catch(function (error) {

      });
  }

  getLastVisitors(username) {
    var outer = this;
    var axios = require('axios');
    var arr = [];

    axios.post('http://52.18.248.248:3001/getLastVisitors', {
        username: username,
        LoggedInUser: this.props.LoggedInUser
      })
      .then(function (response) {
        var s = outer;
        outer.setState({ LastVisitors: response.data.ArrayOfVisitors }, () => s.setState({LastVisitorsLoaded: true}));
      })
      .catch(function (error) {
      });
  }

  loadUserProperties(username) {
    var outer = this;
    var axios = require('axios');


    axios.post('http://52.18.248.248:3001/getUserProperties', {
        username: username
      })
      .then(function (response) {
        var s = outer;
        var obj = response.data.response;
        var obj2 = response.data.returnValue;

        outer.setState({ CurrentUser: obj, UserProfileProperties: obj2 }, () => s.setState({CurrentUserLoaded: true}));
      })
      .catch(function (error) {
      });
  }


    render() {
        return (
            <div>
              <br/>
                      <div className="parent1155">
                        <div className="UserProperties">
                          <UserProperties LoggedInUser={this.props.LoggedInUser} UserProfileProperties={this.state.CurrentUser.UserProfileProperties} OwnProfile={this.state.LoggedInUser === this.state.ProfileOfUser ? true : false } setProfileOfUser={this.setProfileOfUser} removePhoto={this.removePhoto} setPhoto={this.setPhoto} CurrentUser={this.state.CurrentUser} CurrentUserLoaded={this.state.CurrentUserLoaded} />
                        </div>

                        <div className="UserSearch">
                          <SearchForUsers setProfileOfUser={this.setProfileOfUser} />
                        </div>
                      </div>



              <br/>

              <div className="LastVisits">
                <LastVisits LastVisitors={this.state.LastVisitors} LastVisitorsLoaded={this.state.LastVisitorsLoaded} ProfileOfUser={this.state.ProfileOfUser} setProfileOfUser={this.setProfileOfUser} />
              </div>
              <br/>

            </div>
        );
    }
}
