
import React from 'react'
import Friends from './Panels/Friends/Friends';
import Ingredients from './Panels/Ingredients/Ingredients';
import Recipees from './Panels/Recipes/Recipes';
import Diets from './Panels/Diets/Diets';
import Profile from './Panels/Profile/ProfilePanel';
import DietingJourneys from './Panels/DietingJourneys/DietingJourneys'

const styles = {
  activeStyle: {
    color: 'white'
  },
  inactiveStyle: {
    color: 'black'
  }
}


export default class UserProfilePanel extends React.Component {
  constructor(props) {
  super(props);

    this.state = {
      ProfilePanelActive: false,
      FriendsPanelActive: false,
      IngredientsPanelActive: true,
      RecipeesPanelActive: false,
      DietsPanelActive: false,
      DietingJourneyActive: false
    }

    this.handleTabChange = this.handleTabChange.bind(this);
  }

  handleTabChange(value) {
    value === 1 ? this.setState({ProfilePanelActive: true}) : this.setState({ProfilePanelActive: false})
    value === 2 ? this.setState({FriendsPanelActive: true}) : this.setState({FriendsPanelActive: false})
    value === 3 ? this.setState({IngredientsPanelActive: true}) : this.setState({IngredientsPanelActive: false})
    value === 4 ? this.setState({RecipeesPanelActive: true}) : this.setState({RecipeesPanelActive: false})
    value === 5 ? this.setState({DietsPanelActive: true}) : this.setState({DietsPanelActive: false})
    value === 6 ? this.setState({DietingJourneyActive: true}) : this.setState({DietingJourneyActive: false})
  }

  componentWillMount() {
    console.log("ComponnentWillMount");
  }

    render() {
        return (
            <div>
              <ul className="UserProfileNavbar">
                <li onClick={this.handleTabChange.bind(null, 1) } > <span style={ this.state.ProfilePanelActive === true ? styles.activeStyle : styles.inactiveStyle   }> Profile </span> </li>
                <li onClick={this.handleTabChange.bind(null, 2) } > <span style={ this.state.FriendsPanelActive === true ? styles.activeStyle : styles.inactiveStyle   }> Friends </span> </li>
                <li onClick={this.handleTabChange.bind(null, 3) } > <span style={ this.state.IngredientsPanelActive === true ? styles.activeStyle : styles.inactiveStyle   }> Ingredients </span> </li>
                <li onClick={this.handleTabChange.bind(null, 4) } > <span style={ this.state.RecipeesPanelActive === true ? styles.activeStyle : styles.inactiveStyle   }> Recipees </span> </li>
                <li onClick={this.handleTabChange.bind(null, 5) } > <span style={ this.state.DietsPanelActive === true ? styles.activeStyle : styles.inactiveStyle   }> Diets </span> </li>
                <li onClick={this.handleTabChange.bind(null, 6) } > <span style={ this.state.DietingJourneyActive === true ? styles.activeStyle : styles.inactiveStyle   }> Dieting Journey </span> </li>
              </ul>


              {this.state.ProfilePanelActive === true ? <Profile CurrentUser={this.props.CurrentUser}/> : <null />}
              {this.state.FriendsPanelActive === true ? <Friends /> : <null />}
              {this.state.IngredientsPanelActive === true ? <Ingredients UserProfileProperties={this.props.UserProfileProperties} LoggedInUser={this.props.LoggedInUser}  /> : <null />}
              {this.state.RecipeesPanelActive === true ? <Recipees /> : <null />}
              {this.state.DietsPanelActive === true ? <Diets  /> : <null />}
              {this.state.DietingJourneyActive === true ? <DietingJourneys  /> : <null />}
            </div>
        );
    }
}
