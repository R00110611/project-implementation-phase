import React from 'react'

export default class Ingredients extends React.Component {
    render() {
        return (
          <div>
            <table>
              {this.props.UserProfileProperties.Ingredients.CreatedIngredients.map(Ingredient =>
                <tr>
                  <td> {Ingredient.Name} </td>
                </tr>
              )}
            </table>
          </div>
        );
    }
}
