
import React from 'react'
import DropzoneComponent from './Photo/DropzoneComponent'
import Photo from './Photo/Photo'
import UserProfilePanel from './UserProfilePanel/UserProfilePanel'

const styles = {
    activeStyle: {

    },
    inactiveStyle: {
      display: 'none'
    }
};

export default class UserProperties extends React.Component {
  constructor(props) {
  super(props);

    this.state = {
    }

    this.formatDate = this.formatDate.bind(this);
  }

  formatDate(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  }

    render() {
        return (
            <div className="UserProperties1">

            <div style={{textAlign: 'center'}}>
              <h1> Welcome to the profile of {this.props.CurrentUser.Username} <br/> {this.props.OwnProfile === true ? 'Which is your profile!' : <span onClick={this.props.setProfileOfUser.bind(null, this.props.LoggedInUser) }> <span style={{cursor: 'pointer'}}> Go to my profile </span> </span> } </h1>
            </div>

            <div style={{textAlign:'center'}}>
              <div style={{textAlign:'center'}}>
                <p> {this.props.OwnProfile === true && this.props.CurrentUser.Photo === null ? <DropzoneComponent CurrentUser={this.props.CurrentUser.Username} setPhoto={this.props.setPhoto} /> : <Photo photo={this.props.CurrentUser.Photo} removePhoto={this.props.removePhoto} OwnProfile={this.props.OwnProfile} /> } </p>
              </div>

              <br/>

              <div>
                <div  style={this.props.CurrentUser.FirstName === "null" || this.props.CurrentUser.LastName === "null" || this.props.CurrentUser.FirstName === null || this.props.CurrentUser.LastName === null ? styles.inactiveStyle : styles.activeStyle}> <p> <span style={{fontWeight: 'bold'}}> {this.props.CurrentUser.FirstName} {this.props.CurrentUser.LastName} </span> also known as <span style={{fontWeight: 'bold'}}> {this.props.CurrentUser.Username} </span> </p> </div>
                <p> <span style={{fontWeight: 'bold'}}> Registration date </span> {this.formatDate(new Date(this.props.CurrentUser.RegistrationDate))} </p>
                <div style={this.props.CurrentUser.Height === null ? styles.inactiveStyle : styles.activeStyle} > <p> <span style={{fontWeight: 'bold'}}> Height </span> {this.props.CurrentUser.Height}CM <span style={{fontWeight: 'bold'}}> Weight </span> {this.props.CurrentUser.Weight}KG </p> </div>
              </div>
            </div>






            </div>
        );
    }
}
