
import React from 'react'

export default class Photo extends React.Component {
    render() {
        return (
            <div>
              {this.props.OwnProfile === true ? <img src={"http://52.18.248.248:3001/" + this.props.photo} onClick={this.props.removePhoto} height={250} width={250} /> :
              this.props.photo === null ? <img src={"http://52.18.248.248:3001/UserImages/DefaultUserPhoto.jpg"} height={250} width={250} /> : <img src={"http://52.18.248.248:3001/" + this.props.photo} height={250} width={250} />
            }
            </div>
        );
    }
}
