
import React from 'react'
var Dropzone = require('react-dropzone');



export default class DropzoneComponent extends React.Component {
  constructor(props) {
    super(props);

    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(files) {
    console.log("you have dropped: " + files);

    var request = require('superagent');

    var photo = new FormData();
    photo.append('photo', files[0]);


    var outer = this;

    var obj = JSON.stringify(this.state);
    photo.append('username', this.props.CurrentUser);

    request.post('http://52.18.248.248:3001/updateUserPhoto')
      .send(photo)
      .end(function(err, resp) {
        if (err) { console.error(err); }

        outer.props.setPhoto(JSON.parse(resp.text));
        return resp;
      });
  }

    render() {
        return (
              <div>

              <Dropzone onDrop={this.onDrop} style={{textAlign: 'center', marginLeft: '40%', marginRight:'40%', width: '20%', height: '250px', borderStyle: 'dashed'}} activeStyle={{textAlign: 'center', marginLeft: '40%', marginRight:'40%', width: '20%', height: '250px', borderStyle: 'dashed', backgroundColor: 'red'}}>
                <div>Try dropping some photo here, or click to select the photos to upload. </div>
              </Dropzone>


            </div>
        );
    }
}
