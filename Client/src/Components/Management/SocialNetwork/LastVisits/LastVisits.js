import React from 'react'

export default class LastVisits extends React.Component {
  constructor(props) {
    super(props);

    this.formatDate = this.formatDate.bind(this);
  }

  formatDate(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
  }

    render() {
        return (
            <div>
            <h1> Last Visits {this.props.LastVisitors.length} </h1>

            <br/>
            {this.props.LastVisitorsLoaded === true ?
              <table className="LastVisitorsTable">
              {this.props.LastVisitors.map(Visitor =>
              <tr onClick={this.props.setProfileOfUser.bind(null, Visitor.Username)} style={{backgroundColor: '#39465b', cursor: 'pointer'}}>
                <td> <img src={ Visitor.Photo === undefined || Visitor.Photo === null ? "http://52.18.248.248:3001/UserImages/DefaultUserPhoto.jpg" : "http://52.18.248.248:3001/" + Visitor.Photo} height={90} width={90} /> </td>

                <div className="LastVisitorTableTd">
                  <td className="LastVisitorUsername" > {Visitor.Username} </td>
                  <br/>
                  <td className="LastVisitorTimestamp"> {this.formatDate(new Date(Visitor.Timestamp))} </td>
                </div>
              </tr>
              )}
              </table>
            :
            "LastVisitorsNotLoaded"
            }
            <br/>
            </div>
        );
    }
}
