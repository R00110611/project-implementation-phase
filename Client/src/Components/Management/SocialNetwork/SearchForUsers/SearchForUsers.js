
import React from 'react'
import TextField from 'material-ui/TextField';
import {orange500, blue500} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  labelStyle1: {
    color: 'white',
    fontSize: '150%'
  },
  underlineStyle: {
    borderColor: orange500,
  },
  inputStyle: {
    color: 'white',
  },
  style: {
    cursor: 'pointer'
  }
}


export default class SearchForUser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      User: {
        FirstName: "",
        LastName: ""
      },
      FoundUsers: []
    }
    this.SearchUser = this.SearchUser.bind(this);
    this.setFirstName = this.setFirstName.bind(this);
    this.setLastName = this.setLastName.bind(this);
  }

  SearchUser() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForUser', {
        FirstName: outer.state.User.FirstName,
        LastName: outer.state.User.LastName
      })
      .then(function (response) {
        outer.setState({UsersFound: response.data.response});
        var test = response.data.response;
        outer.setState({FoundUsers: test});
      })
      .catch(function (error) {
      });
  }

  setFirstName(e) {
    var outer = this;
    this.setState({User: {
      FirstName: e.target.value,
      LastName: outer.state.User.LastName
    } }, this.SearchUser)
  }

  setLastName(e) {
    var outer = this;
    this.setState({User: {
      FirstName: outer.state.User.FirstName,
      LastName: e.target.value
    } }, this.SearchUser)
  }

    render() {
        return (
            <div>
            <h1> Search for users </h1>
              <TextField

                floatingLabelText="First Name"
                value={this.state.User.FirstName}
                onChange={this.setFirstName}
                underlineStyle={{borderColor: orange500}}
                floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                floatingLabelShrinkStyle={{textAlign: 'center'}}
                hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                style={{textAlign: 'center', color: 'white'}}
                inputStyle={{textAlign: 'center'}}
                textFieldStyle={{textAlign: 'center'}}
                floatingLabelFocusStyle={{textAlign: 'center'}}
              />

              <br/>

              <TextField
                floatingLabelStyle={styles.labelStyle1}
                floatingLabelText="Last Name"
                underlineStyle={styles.underlineStyle}
                inputStyle={styles.inputStyle}
                value={this.state.User.LastName}
                onChange={this.setLastName}
                style={styles.style}
                floatingLabelStyle={{ textAlign: 'center', width: '100%', transformOrigin: 'center top 0px', color: 'grey'}}
                floatingLabelShrinkStyle={{textAlign: 'center'}}
                hintStyle={{textAlign: 'center', color: 'white', width: '100%'}}
                style={{textAlign: 'center', color: 'white'}}
                inputStyle={{textAlign: 'center'}}
                textFieldStyle={{textAlign: 'center'}}
                floatingLabelFocusStyle={{textAlign: 'center'}}
              />

              <br/> <br/>

              <div className="SearchUserTable">
              <table style={{width: '100%'}}>
              <tr>
                <th> Username </th>
                <th> First Name </th>
                <th> Last Name </th>
                <th> Photo </th>
              </tr>

                {this.state.FoundUsers.map(User =>
                  <tr>
                    <td style={{cursor: 'pointer'}} onClick={this.props.setProfileOfUser.bind(null, User.Username)} > {User.Username} </td>
                    <td> {User.FirstName != "null" && User.FirstName != null ? User.FirstName : ""} </td>
                    <td> {User.LastName != "null" && User.LastName != null ? User.LastName : ""} </td>
                    <td> {User.Photo === null || User.Photo === undefined ? <img src={"http://52.18.248.248:3001/UserImages/DefaultUserPhoto.jpg"} height={70} width={70} /> : <img src={"http://52.18.248.248:3001/" + User.Photo} height={70} width={70} /> }  </td>
                  </tr>
                )}
              </table>
              </div>


            </div>
        );
    }
}
