
import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete'

const style1 = {
  activeStyle: {
    fontWeight: 'bold'
  },
  inactiveStyle: {

  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class CompareIngredients extends React.Component {
    render() {
        return (
            <div>
            <div className="CompareRecipees" style={this.props.RecipeesForComparison.length == 0  ? divStyle.inactiveStyle : null} >
              <table className="CompareTable">
                { this.props.RecipeesForComparison.map(
                  Recipee =>
                  <tr className="table22"  >
                    <div className="RecipeeDescriptionComparison" style={{backgroundColor: '#525963'}}>

                    <td> <DeleteIcon className="RemoveIcon" onClick={ this.props.removeRecipeeFromComparison.bind(null, Recipee.RecipeeID) } /> </td>

                    <h2> Recipee Description </h2>
                    <td> {Recipee.RecipeePhoto === null ? <img src={"http://52.18.248.248:3001/RecipeeImages/DefaultRecipeePhoto.png"} height={90} width={90} /> : <img src={"http://52.18.248.248:3001/" + Recipee.RecipeePhoto} height={90} width={90} />} </td>
                    <br/>
                    <td> Recipee name: {Recipee.RecipeeName} </td>
                    <br/>

                    <div className="RecipeeIngredientsComparison">
                      <h2> Recipee Ingredients </h2>

                        <table className="RecipeeListTable2">


                      {Recipee.Ingredients.map(Ingredient =>
                        <div>
                        <tr className="RecipeeListTableRow1">
                          <h2> Description {console.log(Ingredient)}</h2>
                          <td> Ingredient name: {Ingredient.IngredientName} </td>
                          <br/>
                          <td> Image: <img src={Ingredient.RecipeePhoto != null || Ingredient.RecipeePhoto != undefined ? "http://52.18.248.248:3001/" + Ingredient.RecipeePhoto : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="50" width="50" /> </td>


                          <h2> Energy </h2>
                          <td> Calories: { Ingredient.Calories}</td>

                          <td> Amount: { Ingredient.Amount}</td>



                          <h2> Vitamins </h2>
                          <td> VitaminA: { Ingredient.VitaminA}</td>
                          <br/>
                          <td> VitaminB1: { Ingredient.VitaminB1}</td>
                          <br/>
                          <td> VitaminB2: { Ingredient.VitaminB2}</td>
                          <br/>
                          <td> VitaminB3: { Ingredient.VitaminB3}</td>
                                                  <br/>
                          <td> VitaminB5: { Ingredient.VitaminB5}</td>
                              <br/>
                          <td> VitaminB6: { Ingredient.VitaminB6}</td>
                              <br/>
                          <td> VitaminB12: { Ingredient.VitaminB12}</td>
                                <br/>
                          <td> VitaminC: { Ingredient.VitaminC}</td>
                                  <br/>
                          <td> VitaminD: { Ingredient.VitaminD}</td>
                                                  <br/>
                          <td> VitaminE: { Ingredient.VitaminE}</td>
                                                <br/>
                          <td> VitaminK: { Ingredient.VitaminK}</td>




                          <h2> Minerals </h2>
                          <td> Calcium: { Ingredient.Calcium}</td>
                                                <br/>
                          <td> Chromium: { Ingredient.Chromium}</td>
                                                <br/>
                          <td> Copper: { Ingredient.Copper}</td>
                                                <br/>
                          <td> Folate: { Ingredient.Folate}</td>
                                                <br/>
                          <td> Iron: { Ingredient.Iron}</td>
                                                <br/>
                          <td> Magnesium: { Ingredient.Magnesium}</td>
                                                <br/>
                          <td> Manganese: { Ingredient.Manganese}</td>
                                                <br/>
                          <td> Phosphorus: { Ingredient.Phosphorus}</td>
                                                <br/>
                          <td> Potassium: { Ingredient.Potassium}</td>
                                                <br/>
                          <td> Sodium: { Ingredient.Sodium}</td>
                                                <br/>
                          <td> Selenium: { Ingredient.Selenium}</td>
                                                                        <br/>
                          <td> Zinc: { Ingredient.Zinc}</td>



                          <h2> Fats </h2>
                          <td> MonounsaturatedFat: { Ingredient.MonounsaturatedFat}</td>
                                                <br/>
                          <td> PolyunsaturatedFat: { Ingredient.PolyunsaturatedFat}</td>
                                                  <br/>
                          <td> SaturatedFat: { Ingredient.SaturatedFat}</td>
                                                                        <br/>
                          <td> TransFat: { Ingredient.TransFat}</td>
                                                                        <br/>
                          <td> OtherFats: { Ingredient.OtherFats}</td>




                          <h2> Carbohydrates </h2>
                          <td> Fiber: { Ingredient.Fiber}</td>
                          <br/>
                          <td> Sugar: { Ingredient.Sugar}</td>
                          <br/>
                          <td> OtherCarbohydrates: { Ingredient.OtherCarbohydrates}</td>




                          <h2> Other </h2>
                          <td> Cholesterol: { Ingredient.Cholesterol}</td>
                          <br/>
                          <td> Protein: { Ingredient.Protein}</td>
                        </tr>
                      </div>
                      )}

        </table>
                    </div>


                    </div>

                  </tr>
                ) }
              </table>
            </div>
            <br/>
            </div>
        );
    }
}
