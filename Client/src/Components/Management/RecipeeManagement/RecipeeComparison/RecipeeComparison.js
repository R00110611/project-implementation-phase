
import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import Container from './Container.js'

const style1 = {
  activeStyle: {
    fontWeight: 'bold'
  },
  inactiveStyle: {

  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class RecipeeComparison extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CompareNutritionalInformation: true,
      CompareIngredients: false
    }

    this.setActive = this.setActive.bind(this);
  }

  setActive(ActiveComparison) {
    if(ActiveComparison == "CompareNutritionalInformation") {
      this.setState({CompareNutritionalInformation: true, CompareIngredients: false});
    } else {
      this.setState({CompareNutritionalInformation: false, CompareIngredients: true});
    }
  }

    render() {
        return (
            <div>
            <div className="toDoDescription" style={this.props.RecipeesForComparison.length === 0  ? divStyle.activeStyle : divStyle.inactiveStyle} >
              Here you can compare recipees, you can do that by adding recipees in the search tab.
            </div>


            <Container CompareNutritionalInformation={this.state.CompareNutritionalInformation} CompareIngredients={this.state.CompareIngredients} setActive={this.setActive} RecipeesForComparison={this.props.RecipeesForComparison} removeRecipeeFromComparison={this.props.removeRecipeeFromComparison} />



            </div>
        );
    }
}
