
import React from 'react'
import DeleteIcon from 'material-ui/svg-icons/action/delete'

const style1 = {
  activeStyle: {
    fontWeight: 'bold'
  },
  inactiveStyle: {

  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class CompareRecipeeNutritionalInformation extends React.Component {
    render() {
        return (
            <div>



                        <div className="CompareRecipees" style={this.props.RecipeesForComparison.length == 0  ? divStyle.inactiveStyle : null} >
                          <table className="CompareTable">
                            { this.props.RecipeesForComparison.map(
                              Recipee =>
                              <tr className="table1"  >
                                <div className="RemoveButton">
                                    <DeleteIcon className="RemoveIcon" onClick={ this.props.removeRecipeeFromComparison.bind(null, Recipee.RecipeeID) } />
                                </div>

                                <div className="RecipeeDescriptionInComparison">
                                <h2> Description </h2>
                                  {Recipee.RecipeePhoto === null ? <img src={"http://52.18.248.248:3001/RecipeeImages/DefaultRecipeePhoto.png"} height={90} width={90} /> : <img src={"http://52.18.248.248:3001/" + Recipee.RecipeePhoto} height={90} width={90} />}
                                  <br/>
                                  {Recipee.RecipeeName}
                                  <br/>
                                  {Recipee.RecipeeLanguage}
                                  <br/>
                                </div>

                              <div className="RecipeeNutritionalInfoInComparison" style={{textAlign: 'center'}}>

                              <div className="RecipeeNutrInfoComparison" style={{textAlign: 'center'}}>
                                <h2> Energy </h2>
                                Total Amount: {Recipee.TotalAmount}
                                <br/>
                                Total Calories: {Recipee.TotalCalories}
                                <br/>
                              </div>


                              <div className="RecipeeVitaminsComparison">
                                <h2> Vitamins </h2>
                                Total VitaminA: {Recipee.TotalVitaminA}
                                <br/>
                                 Total VitaminB1: {Recipee.TotalVitaminB1}
                                <br/>
                                 Total VitaminB2: {Recipee.TotalVitaminB2}
                                <br/>
                                 Total VitaminB3: {Recipee.TotalVitaminB3}
                                <br/>
                                 Total VitaminB5: {Recipee.TotalVitaminB5}
                                <br/>
                                 Total VitaminB6: {Recipee.TotalVitaminB6}
                                <br/>
                                 Total VitaminB9: {Recipee.TotalVitaminB9}
                                <br/>
                                 Total VitaminB12: {Recipee.TotalVitaminB12}
                                <br/>
                                 Total VitaminC: {Recipee.TotalVitaminC}
                                <br/>
                                 Total VitaminD: {Recipee.TotalVitaminD}
                                <br/>
                                 Total VitaminE: {Recipee.TotalVitaminE}
                                <br/>
                                 Total VitaminK: {Recipee.TotalVitaminK}
                                <br/>
                              </div>

                                <div className="RecipeeMineralsComparison">
                                <h2> Minerals </h2>
                                   Total Calcium: {Recipee.TotalCalcium}
                                  <br/>
                                   Total Chromium: {Recipee.TotalChromium}
                                  <br/>
                                   Total Copper: {Recipee.TotalCopper}
                                  <br/>
                                   Total Folate: {Recipee.TotalFolate}
                                  <br/>
                                   Total Iron: {Recipee.TotalIron}
                                  <br/>
                                   Total Magnesium: {Recipee.TotalMagnesium}
                                  <br/>
                                   Total Manganese: {Recipee.TotalManganese}
                                  <br/>
                                   Total Phosphorus: {Recipee.TotalPhosphorus}
                                  <br/>
                                   Total Potassium: {Recipee.TotalPotassium}
                                  <br/>
                                   Total Selenium: {Recipee.TotalSelenium}
                                  <br/>
                                   Total Zinc: {Recipee.TotalZinc}
                                  <br/>
                                   Total Sodium: {Recipee.TotalSodium}
                                  <br/>
                                </div>

                                <div className="RecipeeFatsComparison">
                                  <h2> Fats </h2>
                                   Total Saturated Fat: {Recipee.TotalSaturatedFat}
                                  <br/>
                                   Total Monounsaturated Fat: {Recipee.TotalMonounsaturatedFat}
                                  <br/>
                                   Total Other Fats: {Recipee.TotalOtherFats}
                                  <br/>
                                   Total Polyunsaturated Fat: {Recipee.TotalPolyunsaturatedFat}
                                  <br/>
                                   Total Trans Fat: {Recipee.TotalTransFat}
                                  <br/>
                                </div>

                                <div className="RecipeeCarbohydratesComparison">
                                  <h2> Carbohydrates </h2>
                                   Total Sugar: {Recipee.TotalSugar}
                                  <br/>
                                   Total Other Carbohydrates: {Recipee.TotalOtherCarbohydrates}
                                  <br/>
                                   Total Fiber: {Recipee.TotalFiber}
                                  <br/>
                                </div>


                                <div className="OtherRecComp">
                                  <h2> Other </h2>
                                   Total Cholesterol: {Recipee.TotalCholesterol}
                                  <br/>
                                   Total Protein: {Recipee.TotalProtein}
                                  <br/>
                                </div>

                          </div>


                              </tr>
                            ) }
                          </table>
                        </div>
                      <br/>
            </div>
        );
    }
}
