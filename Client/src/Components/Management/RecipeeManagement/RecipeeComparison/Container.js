
import React from 'react'
import CompareRecipeeNutritionalInformation from './CompareRecipeeNutritionalInformation'
import CompareIngredients from './CompareIngredients'

const style1 = {
  activeStyle: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle: {
    cursor: 'pointer'
  }
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {
    marginLeft: '1%'
  }
}

export default class Container extends React.Component {
    render() {
        return (
            <div>

            <div className="WhatToCompare" style={this.props.RecipeesForComparison.length == 0 ? divStyle.inactiveStyle : null}>
              What do you want to compare?
              <br/>
              <span onClick={this.props.setActive.bind(null, 'CompareNutritionalInformation')} style={this.props.CompareNutritionalInformation === true ? style1.activeStyle : style1.inactiveStyle} > Recipees Nutritional Information </span> or <span  style={this.props.CompareIngredients === true ? style1.activeStyle : style1.inactiveStyle} onClick={this.props.setActive.bind(null, 'CompareIngredients')}> Recipees Ingredients </span>
            </div>

            <div style={this.props.CompareNutritionalInformation === true ? null : divStyle.inactiveStyle } >
              <CompareRecipeeNutritionalInformation setActive={this.setActive} RecipeesForComparison={this.props.RecipeesForComparison} removeRecipeeFromComparison={this.props.removeRecipeeFromComparison}  />
            </div>

            <div style={this.props.CompareIngredients === true ? null : divStyle.inactiveStyle } >
              <CompareIngredients setActive={this.setActive} RecipeesForComparison={this.props.RecipeesForComparison} removeRecipeeFromComparison={this.props.removeRecipeeFromComparison}  />
            </div>

            </div>
        );
    }
}
