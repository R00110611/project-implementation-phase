
import React from 'react'
var Dropzone = require('react-dropzone');

export default class DropzoneComponent extends React.Component {
    render() {
        return (
            <div className="Dropzone1">
            <Dropzone onDrop={this.props.onDrop} style={{backgroundColor: 'gray', width: '100%', height: '120px', color:'black', borderStyle: 'solid'}} activeStyle={{width: '100%', height: '120px', color:'black', borderColor:'black', borderStyle: 'solid', backgroundColor: 'red'}}>
              <div>Try dropping some photo here, or click to select the photos to upload. </div>
            </Dropzone>
            </div>
        );
    }
}
