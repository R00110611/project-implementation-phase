
import React from 'react'
import IngredientSearch from './IngredientSearch/IngredientSearch.js'
var _ = require('lodash');
import TextField from 'material-ui/TextField';
import update from 'immutability-helper';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DropzoneComponent from './DropzoneComponent'
import Notifications, {notify} from 'react-notify-toast';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import DownArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';

const styles = {
  NutritionalInputStyle: {
    color: 'white',
    cursor: 'pointer',
    textAlign: 'center',
  },
  NutritionalInputStyleStyle: {
    width: '100%',
    textAlign: 'center'
  },
  FloatingLabelStyle: {
    color: 'white',
    textAlign: 'center',
    width: '100%'
  },
  floatingLabelFocusStyle: {
    textAlign: 'center',
    width: '100%'
  },
  floatingLabelShrinkStyle: {
    textAlign: 'center',
    width: '100%',
    marginLeft: '10%'
  },
  hintStyle: {
    textAlign: 'center',
    width: '100%'
  },
  textAreaFloatingLabelStyle: {
  color: 'white',
  textAlign: 'left',
  width: '100%'
},
}

const divStyle = {
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  },
  inactiveStyleCreateButton: {
    display: 'none'
  },
  activeStyleCreateButton: {

  }
}

const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}


const divStyles = {
  activeStyle: {
    backgroundColor: '#525963',
  },
  inactiveStyle: {
    display: 'none'
  },
  hoverStyle: {
    cursor: 'pointer'
  },
  inhoverStyle: {

  }
}

export default class RecipeeCreation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      RecipeeIngredients: [],
      IngredientSearchActive: true,
      RecipeeIngredientsActive: false,
      RecipeeDescriptionActive: false,
      RecipeeName: "",
      PreparationMethod: "",
      LoggedInUser: this.props.LoggedInUser,
      IngredientLanguage: "English",
      value: 1,
      IngredientPhotos: [1],
      image1: false,
      filesPhotos: [],
      photoImage: new FormData(),
      ConfirmRecipeeActive: false,
      hoverActive: 0
    }

    this.addIngredientToRecipee = this.addIngredientToRecipee.bind(this);
    this.removeIngredientFromRecipee = this.removeIngredientFromRecipee.bind(this);
    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.Continue = this.Continue.bind(this);
    this.GoBack = this.GoBack.bind(this);
    this.CreateRecipee = this.CreateRecipee.bind(this);
    this.handleIngredientLanguageChange = this.handleIngredientLanguageChange.bind(this);
    this.onDrop = this.onDrop.bind(this);
    this.removePhoto = this.removePhoto.bind(this);
    this.isFloat = this.isFloat.bind(this);
    this.validateNumber = this.validateNumber.bind(this);
    this.recipeeCreated = this.recipeeCreated.bind(this);
    this.removeIngredient = this.removeIngredient.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onHover = this.onHover.bind(this);
  }


  onHover(activeNr) {
      this.setState({hoverActive: activeNr});
  }

  recipeeCreated() {
    toastr.success('Recipee has been created!.', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

    this.setState({
      RecipeeIngredients: [],
      IngredientSearchActive: true,
      RecipeeIngredientsActive: false,
      RecipeeDescriptionActive: false,
      RecipeeName: "",
      PreparationMethod: "",
      LoggedInUser: this.props.LoggedInUser,
      IngredientLanguage: "English",
      value: 1,
      IngredientPhotos: [1],
      image1: false,
      filesPhotos: [],
      photoImage: new FormData(),
      ConfirmRecipeeActive: false,
      hoverActive: 0
    })
  }

  onClick(tabToActivate) {
    if(tabToActivate === 1) {
      if(this.state.RecipeeIngredientsActive === true) {
        this.setState({RecipeeIngredientsActive: false});
      } else {
        this.setState({ConfirmRecipeeActive: false, RecipeeDescriptionActive: false, RecipeeIngredientsActive: true});
      }
    }

    if(tabToActivate == 2) {
      if(this.state.ConfirmRecipeeActive === true) {
        this.setState({ConfirmRecipeeActive: false});
      } else {
        this.setState({RecipeeIngredientsActive: false, RecipeeDescriptionActive: false, ConfirmRecipeeActive: true});
      }
  }

  if(tabToActivate == 3) {
    if(this.state.RecipeeDescriptionActive === true) {
      this.setState({RecipeeDescriptionActive: false});
    } else {
      this.setState({ConfirmRecipeeActive: false, RecipeeIngredientsActive: false, RecipeeDescriptionActive: true});
    }
  }


}

  validateNumber(number) {
    var validated = true;


    if(this.isFloat(number) === true) {
      validated = true;
    } else {
      validated = false;
    }

    if(number === "") {
      validated = false;
    }

    if(number == 0) {
      validated = false;
    }


    return validated;
  }

  isFloat(number) {


    if(isNaN(number) === false) {
      return true;
    } else {
      return false;
    }
  }

  removeIngredient(IngredientID) {
    var arr = this.state.RecipeeIngredients;

    arr = arr.filter(function( obj ) {
        return obj.ID !== IngredientID;
    });

    this.setState({RecipeeIngredients: arr});

  }

  removePhoto(photoNr) {
    this.setState({IngredientPhotos: [1], filesPhotos: [], image1: false});
  }

  onDrop(files) {
    if(this.state.image1 === false) {
      this.state.IngredientPhotos.push(files[0].preview);
      this.setState({image1: true});
      this.setState({filesPhotos: files[0]});
    }
  }

  handleIngredientLanguageChange(event, index, val) {
    event.target.value === 'English' ? this.setState({IngredientLanguage: 'English'}) : null;
    event.target.value === 'Polish' ? this.setState({IngredientLanguage: 'Polish'}) : null;
    event.target.value === 'Spanish' ? this.setState({IngredientLanguage: 'Spanish'}) : null;

  }

  CreateRecipee() {

    if(this.state.RecipeeIngredients.length != 0) {
      var request = require('superagent');
      var outer = this;

      var photo = new FormData();
      photo.append('photo', this.state.filesPhotos);

      var obj = JSON.stringify(this.state);
      photo.append('obj', obj);

      request.post('http://52.18.248.248:3001/CreateRecipee')
        .send(photo)
        .end(function(err, resp) {
          if (err) { console.error(err); }
          outer.recipeeCreated();
          return resp;
        });
      }
  }

  Continue() {
    if(this.state.RecipeeIngredients.length != 0) {
      this.setState({ConfirmRecipeeActive: false, RecipeeIngredientsActive: false, RecipeeDescriptionActive: true});
    } else {
      toastr.error('Select ingredient(s) first!.', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }
  }

  GoBack() {
    this.setState({IngredientSearchActive: true, RecipeeIngredientsActive: true, RecipeeDescriptionActive: false});
  }

  addIngredientToRecipee(Ingredient) {
    var newA = this.state.RecipeeIngredients;

    var s = {
      photoURL: Ingredient.photoURL,
      name: Ingredient.name,
      Calories: Ingredient.Calories,
      Amount: Ingredient.Amount,
      VitaminA: Ingredient.VitaminA,
      VitaminB1: Ingredient.VitaminB1,
      VitaminB2: Ingredient.VitaminB2,
      VitaminB3: Ingredient.VitaminB3,
      VitaminB5: Ingredient.VitaminB5,
      VitaminB6: Ingredient.VitaminB6,
      VitaminB9: Ingredient.VitaminB9,
      VitaminB12: Ingredient.VitaminB12,
      VitaminC: Ingredient.VitaminC,
      VitaminD: Ingredient.VitaminD,
      VitaminE: Ingredient.VitaminE,
      VitaminK: Ingredient.VitaminK,
      Calcium: Ingredient.Calcium,
      Chromium: Ingredient.Chromium,
      Copper: Ingredient.Copper,
      Folate: Ingredient.Folate,
      Iron: Ingredient.Iron,
      Magnesium: Ingredient.Magnesium,
      Manganese: Ingredient.Manganese,
      Phosphorus: Ingredient.Phosphorus,
      Potassium: Ingredient.Potassium,
      Sodium: Ingredient.Sodium,
      Selenium: Ingredient.Selenium,
      Zinc: Ingredient.Zinc,
      MonounsaturatedFat: Ingredient.MonounsaturatedFat,
      PolyunsaturatedFat: Ingredient.PolyunsaturatedFat,
      SaturatedFat: Ingredient.SaturatedFat,
      TransFat: Ingredient.TransFat,
      OtherFats: Ingredient.OtherFats,
      Fiber: Ingredient.Fiber,
      Sugar: Ingredient.Sugar,
      OtherCarbohydrates: Ingredient.OtherCarbohydrates,
      Cholesterol: Ingredient.Cholesterol,
      Protein: Ingredient.Protein,
      ID: Ingredient.id,
      DisplayAmount: Ingredient.Amount
    };

    var existsAlready = false;
    newA.map(RecipeeIngredient => {
      if(RecipeeIngredient.ID === Ingredient.id)
        existsAlready = true;
    });

    if(existsAlready === false) {
      newA.push(s);
      toastr.success('Added ingredient ' + Ingredient.name + ' to Recipee!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
      this.setState({RecipeeIngredients: newA});
    } else {
      toastr.error('Ingredient ' + Ingredient.name + ' is already added to the recipee', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }
  }



  removeIngredientFromRecipee() {

  }

  handleAmountChange(newAmount, IngredientID) {
    var async = require('async');
    var outer = this;

    // if newAmount is proper then do waterfall else change displayAmount...


    if(this.validateNumber(newAmount) === false) {
      var newArr = outer.state.RecipeeIngredients;
      var index = _.findIndex(newArr, function(o) {
        return o.ID === IngredientID
      });
      newArr[index].DisplayAmount = newAmount;


      this.setState({RecipeeIngredients: newArr});
    } else {
    async.waterfall([
        function(callback) {
          var newArr = outer.state.RecipeeIngredients;

          var index = _.findIndex(newArr, function(o) {
            return o.ID === IngredientID
          });

          var newestAmount = newAmount;
          newArr[index].DisplayAmount = newAmount;
          var newestCalories = (newAmount * (newArr[index].Calories/newArr[index].Amount) );
          var newVitaminA = (newAmount * (newArr[index].VitaminA/newArr[index].Amount) );
          var newVitaminB1 = (newAmount * (newArr[index].VitaminB1/newArr[index].Amount) );
          var newVitaminB2 = (newAmount * (newArr[index].VitaminB2/newArr[index].Amount) );
          var newVitaminB3 = (newAmount * (newArr[index].VitaminB3/newArr[index].Amount) );
          var newVitaminB5 = (newAmount * (newArr[index].VitaminB5/newArr[index].Amount) );
          var newVitaminB6 = (newAmount * (newArr[index].VitaminB6/newArr[index].Amount) );
          var newVitaminB9 = (newAmount * (newArr[index].VitaminB9/newArr[index].Amount) );
          var newVitaminB12 = (newAmount * (newArr[index].VitaminB12/newArr[index].Amount) );
          var newVitaminC = (newAmount * (newArr[index].VitaminC/newArr[index].Amount) );
          var newVitaminD = (newAmount * (newArr[index].VitaminD/newArr[index].Amount) );
          var newVitaminE = (newAmount * (newArr[index].VitaminE/newArr[index].Amount) );
          var newVitaminK = (newAmount * (newArr[index].VitaminK/newArr[index].Amount) );
          var newCalcium = (newAmount * (newArr[index].Calcium/newArr[index].Amount) );
          var newChromium = (newAmount * (newArr[index].Chromium/newArr[index].Amount) );
          var newCopper = (newAmount * (newArr[index].Copper/newArr[index].Amount) );
          var newFolate = (newAmount * (newArr[index].Folate/newArr[index].Amount) );
          var newIron = (newAmount * (newArr[index].Iron/newArr[index].Amount) );
          var newMagnesium = (newAmount * (newArr[index].Magnesium/newArr[index].Amount) );
          var newManganese = (newAmount * (newArr[index].Manganese/newArr[index].Amount) );
          var newPhosphorus = (newAmount * (newArr[index].Phosphorus/newArr[index].Amount) );
          var newPotassium = (newAmount * (newArr[index].Potassium/newArr[index].Amount) );
          var newSodium = (newAmount * (newArr[index].Sodium/newArr[index].Amount) );
          var newSelenium = (newAmount * (newArr[index].Selenium/newArr[index].Amount) );
          var newZinc = (newAmount * (newArr[index].Zinc/newArr[index].Amount) );
          var newMonounsaturatedFat = (newAmount * (newArr[index].MonounsaturatedFat/newArr[index].Amount) );
          var newPolyunsaturatedFat = (newAmount * (newArr[index].PolyunsaturatedFat/newArr[index].Amount) );
          var newSaturatedFat = (newAmount * (newArr[index].SaturatedFat/newArr[index].Amount) );
          var newTransFat = (newAmount * (newArr[index].TransFat/newArr[index].Amount) );
          var newOtherFats = (newAmount * (newArr[index].OtherFats/newArr[index].Amount) );
          var newFiber = (newAmount * (newArr[index].Fiber/newArr[index].Amount) );
          var newSugar = (newAmount * (newArr[index].Sugar/newArr[index].Amount) );
          var newOtherCarbohydrates = (newAmount * (newArr[index].OtherCarbohydrates/newArr[index].Amount) );
          var newCholesterol = (newAmount * (newArr[index].Cholesterol/newArr[index].Amount) ) ;
          var newProtein = (newAmount * (newArr[index].Protein/newArr[index].Amount) );

          newArr[index].Amount = newestAmount;
          newArr[index].Calories = newestCalories;
          newArr[index].VitaminA = newVitaminA;
          newArr[index].VitaminB1 = newVitaminB1;
          newArr[index].VitaminB2 = newVitaminB2;
          newArr[index].VitaminB3 = newVitaminB3;
          newArr[index].VitaminB5 = newVitaminB5;
          newArr[index].VitaminB6 = newVitaminB6;
          newArr[index].VitaminB9 = newVitaminB9;
          newArr[index].VitaminB12 = newVitaminB12;
          newArr[index].VitaminC = newVitaminC;
          newArr[index].VitaminD = newVitaminD;
          newArr[index].VitaminE = newVitaminE;
          newArr[index].VitaminK = newVitaminK;
          newArr[index].Calcium =newCalcium;
          newArr[index].Chromium = newChromium;
          newArr[index].Copper =newCopper;
          newArr[index].Folate = newFolate;
          newArr[index].Iron = newIron;
          newArr[index].Magnesium =newMagnesium;
          newArr[index].Manganese = newManganese;
          newArr[index].Phosphorus = newPhosphorus;
          newArr[index].Potassium = newPotassium;
          newArr[index].Sodium = newSodium;
          newArr[index].Selenium =newSelenium;
          newArr[index].Zinc = newZinc;
          newArr[index].MonounsaturatedFat =newMonounsaturatedFat;
          newArr[index].PolyunsaturatedFat = newPolyunsaturatedFat;
          newArr[index].SaturatedFat =newSaturatedFat;
          newArr[index].TransFat = newTransFat;
          newArr[index].OtherFats = newOtherFats;
          newArr[index].Fiber =newFiber;
          newArr[index].Sugar =newSugar;
          newArr[index].OtherCarbohydrates = newOtherCarbohydrates;
          newArr[index].Cholesterol= newCholesterol;
          newArr[index].Protein = newProtein;




          callback(null, newArr);
        }
    ],

    function (err, result) {
      outer.setState({RecipeeIngredients: result});
    });
  }



  }

    render() {
        return (
            <div className="innermost" style={{marginTop: '2.5%'}}>
            <br/>
              <div className="DescriptionIC1">

              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 1)} style={this.state.hoverActive === 1 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 1)} >
                <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}} > 1. Select ingredients
                <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.RecipeeIngredientsActive === false ? <LeftArrow /> : <DownArrow /> } </span>

                </span>
                </div>

                <div className="testinh" style={this.state.RecipeeIngredientsActive === true ? divStyle.activeStyle : divStyle.inactiveStyle}>
                <IngredientSearch Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} addIngredientToRecipee={this.addIngredientToRecipee} removeIngredientFromRecipee={this.removeIngredientFromRecipee} />
                </div>
              </div>


              <br/>

      <div className="DescriptionIC1">
              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 2)} style={this.state.hoverActive === 2 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 2) }>
                  <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 2. Confirm Ingredients in Recipee
                  <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.ConfirmRecipeeActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                  </span>
            </div>
                <div className="testinh" style={this.state.ConfirmRecipeeActive === true ? divStyle.activeStyle : divStyle.inactiveStyle}>
                  <br/>



                                                <h2 style={{textAlign: 'center'}}> Ingredients In Recipee </h2>
                              <table className="RecipeeIngredientsTable">
                            <tr style={{backgroundColor: '#21262d', color: 'white'}}>
                              <th>  </th>
                              <th>  </th>
                              <th> Name </th>
                              <th> Amount </th>
                              <th> Calories  </th>
                              <th> Vitamins  </th>
                              <th> Minerals </th>
                              <th> Fats </th>
                              <th> Carbohydrates </th>
                              <th> Other </th>




                            </tr>
                              {this.state.RecipeeIngredients.map(Ingredient =>
                                <tr className="RecipeeIngredientsTableRow" style={{backgroundColor: '#38404f'}}>


                                      <td onClick={this.removeIngredient.bind(null, Ingredient.ID)} > <DeleteIcon /> </td>
                                      <td><img src={Ingredient.photoURL != null || Ingredient.photoURL != undefined ? "http://52.18.248.248:3001/" + Ingredient.photoURL : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="50" width="50" />  </td>
                                      <td>{Ingredient.name}</td>



                                    <td>

                                    <br/>
                                    <input     onChange={e => e.target.value.length < 12 ? this.handleAmountChange(e.target.value, Ingredient.ID) : null} type="text" value={Ingredient.DisplayAmount} placeholder={this.props.Language === 'English' ? 'Amount *' : this.props.Language === 'Spanish' ? 'Cantidad *' : 'Ilosc *'       } style={{textAlign: 'center', width: '70%', marginLeft: '15%', marginRight: '15%'}} />
                                    <br/><br/>


                                    </td>


                                  <td>{Ingredient.Calories}</td>

                                  <td>
                                  <tr> VitaminA {Ingredient.VitaminA}</tr>
                                  <tr> VitaminB1 {Ingredient.VitaminB1}</tr>
                                  <tr> VitaminB2 {Ingredient.VitaminB2}</tr>
                                  <tr> VitaminB3 {Ingredient.VitaminB3}</tr>
                                  <tr> VitaminB5 {Ingredient.VitaminB5}</tr>
                                  <tr> VitaminB6 {Ingredient.VitaminB6}</tr>
                                  <tr> VitaminB9 {Ingredient.VitaminB9}</tr>
                                  <tr> VitaminB12 {Ingredient.VitaminB12}</tr>
                                  <tr> VitaminC {Ingredient.VitaminC}</tr>
                                  <tr> VitaminD {Ingredient.VitaminD}</tr>
                                  <tr> VitaminE {Ingredient.VitaminE}</tr>
                                  <tr> VitaminK {Ingredient.VitaminK}</tr>
                                  </td>

                                <td>
                                  <tr> Calcium {Ingredient.Calcium}</tr>
                                  <tr> Chromium {Ingredient.Chromium}</tr>
                                  <tr> Copper {Ingredient.Copper}</tr>
                                  <tr> Folate {Ingredient.Folate}</tr>
                                  <tr> Iron {Ingredient.Iron}</tr>
                                  <tr> Magnesium {Ingredient.Magnesium}</tr>
                                  <tr> Manganese {Ingredient.Manganese}</tr>
                                  <tr> Phosphorus {Ingredient.Phosphorus}</tr>
                                  <tr> Potassium {Ingredient.Potassium}</tr>
                                  <tr> Sodium {Ingredient.Sodium}</tr>
                                  <tr> Selenium {Ingredient.Selenium}</tr>
                                  <tr> Zinc {Ingredient.Zinc}</tr>
                                  </td>

                                  <td>
                                  <tr> Monounsaturated Fat {Ingredient.MonounsaturatedFat}</tr>
                                  <tr> Polyunsaturated Fat {Ingredient.PolyunsaturatedFat}</tr>
                                  <tr> Saturated Fat {Ingredient.SaturatedFat}</tr>
                                  <tr> Trans Fat {Ingredient.TransFat}</tr>
                                  <tr> Other Fats {Ingredient.OtherFats}</tr>
                                  </td>

                                  <td>
                                  <tr> Fiber {Ingredient.Fiber}</tr>
                                  <tr> Sugar {Ingredient.Sugar}</tr>
                                  <tr> Other Carbohydrates {Ingredient.OtherCarbohydrates}</tr>
                                  </td>

                                  <td>
                                  <tr> Cholesterol {Ingredient.Cholesterol}</tr>
                                  <tr> Protein {Ingredient.Protein}</tr>
                                  </td>



                                </tr>
                              )}
                              </table>
                              <br/>



                              <h2 style={{textAlign: 'center'}}> Recipee Nutritonal Information </h2>
                              <table className="RecipeeIngredientsTable">

                            <tr style={{backgroundColor: '#21262d', color: 'white'}}>
                            <th> Amount </th>
                            <th> Calories  </th>
                            <th> Vitamins  </th>
                            <th> Minerals </th>
                            <th> Fats </th>
                            <th> Carbohydrates </th>
                            <th> Other </th>
                                            </tr>


                                              <tr className="RecipeeTotalsTableRow" style={{backgroundColor: '#38404f'}}>
                                              <td> Amount:   {  _.sumBy(this.state.RecipeeIngredients, function(o) { return parseFloat(o.Amount) }) } </td>
                                              <td> Calories:   {  _.sumBy(this.state.RecipeeIngredients, function(o) { return o.Calories }) } </td>


                                              <td>
                                                <tr> VitaminA {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminA}) } </tr>
                                                <tr> VitaminB1 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB1}) } </tr>
                                                <tr> VitaminB2 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB2}) } </tr>
                                                <tr> VitaminB3 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB3}) } </tr>
                                                <tr> VitaminB5 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB5}) } </tr>
                                                <tr> VitaminB6 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB6}) } </tr>
                                                <tr> VitaminB9 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB9}) } </tr>
                                                <tr> VitaminB12 {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminB12}) } </tr>
                                                <tr> VitaminC {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminC}) } </tr>
                                                <tr> VitaminD {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminD}) } </tr>
                                                <tr> VitaminE {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminE}) } </tr>
                                                <tr> VitaminK {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.VitaminK}) }  </tr>
                                              </td>

                                              <td>
                                                <tr> Calcium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Calcium}) } </tr>
                                                <tr> Chromium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Chromium}) } </tr>
                                                <tr> Copper {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Copper}) } </tr>
                                                <tr> Folate {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Folate}) } </tr>
                                                <tr> Iron {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Iron}) } </tr>
                                                <tr> Magnesium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Magnesium}) } </tr>
                                                <tr> Manganese {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Manganese}) } </tr>
                                                <tr> Phosphorus {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Phosphorus}) } </tr>
                                                <tr> Potassium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Potassium}) } </tr>
                                                <tr> Sodium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Sodium}) } </tr>
                                                <tr> Selenium {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Selenium}) } </tr>
                                                <tr> Zinc {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Zinc}) } </tr>
                                              </td>

                                              <td>
                                              <tr> Monounsaturated Fat {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.MonounsaturatedFat}) } </tr>
                                              <tr> Polyunsaturated Fat {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.PolyunsaturatedFat}) } </tr>
                                              <tr> Saturated Fat {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.SaturatedFat}) } </tr>
                                              <tr> Trans Fat {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.TransFat}) } </tr>
                                              <tr> Other Fats {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.OtherFats}) } </tr>
                                              </td>

                                              <td>
                                              <tr> Fiber {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Fiber}) } </tr>
                                              <tr> Sugar {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Sugar}) } </tr>
                                              <tr> Other Carbohydrates {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.OtherCarbohydrates}) } </tr>
                                              </td>

                                              <td>
                                              <tr> Cholesterol {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Cholesterol}) } </tr>
                                              <tr> Protein {  _.sumBy(this.state.RecipeeIngredients, function(o) {return o.Protein}) } </tr>
                                              </td>



                                              </tr>
                                              </table>
                                              <br/>

                                    </div>

                                            </div>


                                            <br/>

                                            <div className="DescriptionIC1">
                                              <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 3)} style={this.state.hoverActive === 3 ? divStyles.hoverStyle : divStyles.inhoverStyle} onClick={this.onClick.bind(this, 3) }>
                                                <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 3. Enter recipee description
                                                  <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.RecipeeDescriptionActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                                                </span>
                                                </div>
                                                <div className="testinh" style={this.state.RecipeeDescriptionActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >


                                                <div style={{textAlign: 'center'}} >
                                                <br/>
                                                <input onChange={e => this.setState({ RecipeeName: e.target.value })} type="text" value={this.state.RecipeeName} placeholder={this.props.Language === 'English' ? 'Name *' : this.props.Language === 'Spanish' ? 'Nombre *' : 'Nazwa *'       } style={{textAlign: 'center'}} />
                                                <br/>
                                                <span style={this.state.RecipeeName.length > 50 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.RecipeeName.length}/50 </span>

                                                <br/> <br/>


                                                <textarea value={this.state.PreparationMethod} onChange={e => this.setState({ PreparationMethod: e.target.value })} style={{resize: 'vertical', textAlign: 'center'}} placeholder={this.props.Language === 'English' ? 'Preparation Method' : this.props.Language === 'Spanish' ? 'Preparation Method' : 'Preparation Method'       }>

                                                </textarea> <br/>
                                                <span style={this.state.PreparationMethod.length > 250 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.PreparationMethod.length}/250 </span>

                                                <br/> <br/>

                                                <span style={{paddingRight: '1%'}}> Language </span>
                                                  <select onChange={this.handleIngredientLanguageChange} >
                                                    <option value="English" selected={this.state.IngredientLanguage==="English" ? true : false} > {this.props.Language === 'English' ? 'English' : this.props.Language === 'Spanish' ? 'Inglés' : 'Angielski'} </option>
                                                    <option value="Polish" selected={this.state.IngredientLanguage==="Polish" ? true : false}  > {this.props.Language === 'English' ? 'Polish' : this.props.Language === 'Spanish' ? 'Polaco' : 'Polski'} </option>
                                                    <option value="Spanish" selected={this.state.IngredientLanguage==="Spanish" ? true : false}  > {this.props.Language === 'English' ? 'Spanish' : this.props.Language === 'Spanish' ? 'Español' : 'Hiszpanski'} </option>
                                                  </select>

                                                  <br/> <br/>

                                                  <div className="img1">
                                                  {this.state.image1 === true ?
                                                    <span> Recipee Image <br/> <img onClick={this.removePhoto} src={this.state.IngredientPhotos[1]} height="100px" width="100px" className="image1" onClick={this.removePhoto.bind(this, 1)}  />  <br/> </span>
                                                    :
                                                    <DropzoneComponent onDrop={this.onDrop} /> }
                                                  <br/>
                                                  </div>

                                                  </div>
                                                </div>

                                              </div>




                                            <div className="buttonparent">
                                                <RaisedButton label={"Create"}
                                                  onClick={ this.CreateRecipee}
                                                  backgroundColor="navy"
                                                  style={{width: '60%', marginLeft: '20%', marginRight: '20%'}}
                                                  labelColor="white"
                                                  disabled={this.state.RecipeeIngredients.length === 0 || this.state.PreparationMethod.length > 250 || this.state.RecipeeName.length > 50 || this.state.RecipeeName.length == 0  ? true : false}
                                                  >
                                                </RaisedButton>

                            </div>



                            <br/>
            </div>
        );
    }
}
