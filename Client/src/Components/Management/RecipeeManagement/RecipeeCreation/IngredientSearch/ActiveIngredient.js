import React from 'react'
import IngredientReviews from './IngredientReviews'



export default class ActiveIngredient extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
		};
}


    render() {
        return (
            <div className="activeIngredient">

              <div className="IngredientInformations">
                <h1> Ingredient Information </h1>

                <br/>

                <h2> Description </h2>
                <div className="IngredientDescription">
                    <table>
                      <li>  <img src={this.props.Ingredient.photoURL != null || this.props.Ingredient.photoURL != undefined ? "http://52.18.248.248:3001/" + this.props.Ingredient.photoURL : "http://52.18.248.248:3001/IngredientImages\\DefaultIngredientPhoto.png"} height="110" width="130" />  </li>
                      <li> Ingredient name {this.props.Ingredient.name} </li>
                      <li> Creator {this.props.Ingredient.Username}  </li>
                      <li> Description {this.props.Ingredient.Description}  </li>
                      <li> Language {this.props.Ingredient.Language}  </li>
                    </table>
                  </div>
                </div>


                <br/>
                <h2> Nutritional Information </h2>
                <div className="IngredientNutritionalInformation">


                  <h3> Energy </h3>
                  <table>
                    <li> Calories {this.props.Ingredient.Calories} </li>
                    <li> Amount {this.props.Ingredient.Amount} </li>
                  </table>


                  <h3> Vitamins </h3>
                  <table>
                    <li> VitaminA {this.props.Ingredient.VitaminA}</li>
                    <li> VitaminB1 {this.props.Ingredient.VitaminB1}</li>
                    <li> VitaminB2 {this.props.Ingredient.VitaminB2}</li>
                    <li> VitaminB3 {this.props.Ingredient.VitaminB3}</li>
                    <li> VitaminB5 {this.props.Ingredient.VitaminB5}</li>
                    <li> VitaminB6 {this.props.Ingredient.VitaminB6}</li>
                    <li> VitaminB12 {this.props.Ingredient.VitaminB12}</li>
                    <li> VitaminC {this.props.Ingredient.VitaminC}</li>
                    <li> VitaminD {this.props.Ingredient.VitaminD}</li>
                    <li> VitaminE {this.props.Ingredient.VitaminE}</li>
                    <li> VitaminK {this.props.Ingredient.VitaminK}</li>
                  </table>


                  <h3> Minerals </h3>
                  <table>
                  <li> Calcium {this.props.Ingredient.Calcium}</li>
                  <li> Chromium {this.props.Ingredient.Chromium}</li>
                  <li> Copper {this.props.Ingredient.Copper}</li>
                  <li> Folate {this.props.Ingredient.Folate}</li>
                  <li> Iron {this.props.Ingredient.Iron}</li>
                  <li> Magnesium {this.props.Ingredient.Magnesium}</li>
                  <li> Mangase {this.props.Ingredient.Mangase}</li>
                  <li> Phosphorus {this.props.Ingredient.Phosphorus}</li>
                  <li> Potassium {this.props.Ingredient.Potassium}</li>
                  <li> Sodium {this.props.Ingredient.Sodium}</li>
                  <li> Selenium {this.props.Ingredient.Selenium}</li>
                  <li> Zinc {this.props.Ingredient.Zinc}</li>
                  </table>


                  <h3> Fats </h3>
                  <table>
                  <li> Monounsaturated Fat {this.props.Ingredient.MonounsaturatedFat}</li>
                  <li> Polyunsaturated Fat {this.props.Ingredient.PolyunsaturatedFat}</li>
                  <li> Saturated Fat {this.props.Ingredient.SaturatedFat}</li>
                  <li> Trans Fat {this.props.Ingredient.TransFat}</li>
                  <li> Other Fats {this.props.Ingredient.OtherFats}</li>
                  </table>


                  <h3> Carbohydrates </h3>
                  <table>
                    <li> Fiber {this.props.Ingredient.Fiber}</li>
                    <li> Sugar {this.props.Ingredient.Sugar}</li>
                    <li> Other Carbohydrates {this.props.Ingredient.OtherCarbohydrates}</li>
                  </table>



                  <h3> Other </h3>
                  <table>
                    <li> Cholesterol {this.props.Ingredient.Cholesterol}</li>
                    <li> Protein {this.props.Ingredient.Protein}</li>
                  </table>
                </div>




                <div className="Reviews">
                    <IngredientReviews LoggedInUser={this.props.LoggedInUser} Ingredient={this.props.Ingredient} Reviews={this.props.Reviews} state={this.props.state} ActiveIngredient={this.props.Ingredient} rowClick={this.props.rowClick} />
                </div>



            </div>
        );
    }
}
