import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';

const styles = {
  block: {
    maxWidth: 250,
  },
  radioButton: {
    marginBottom: 16,
  },
};

export default class IngredientReviews extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
      textArea: "",
      Recommended: 0,
      selected: "Positive"
		};

    this.addReview = this.addReview.bind(this);
}

addReview() {
  var recc = 0;

  if(this.state.selected === "Positive") {
    recc = 1;
  } else {
    recc = 0;
  }

  var outer = this;
  var axios = require('axios');

  axios.post('http://52.18.248.248:3001/AddIngredientReview', {
    IngredientID: this.props.Ingredient.id,
    Username: this.props.LoggedInUser,
    Review: this.state.textArea,
    Recommended: recc,
    })
    .then(function (response) {
      if(response.data.response === true) {
        outer.props.rowClick(outer.props.ActiveIngredient);

      }
      else {

      }

    })
    .catch(function (error) {
    });

}

    render() {
        return (
            <div className="Reviews">

              <h1> Reviews of {this.props.Ingredient.id} </h1>



              <table className="ReviewTable">
                {this.props.Reviews.map(ReviewInd =>
                  <tr className="ReviewRow">
                      <td className="ReviewTableDate"> {ReviewInd.Review} </td>
                      <td> {ReviewInd.Username} </td>
                      <td> {ReviewInd.Recommended === 0 ? "Negative" : "Positive"} </td>
                      <td> {ReviewInd.TimeAndDate} </td>
                  </tr>
                )}
              </table>

              <div className="addReview">
                <h2> Add review </h2>

                <textarea rows="4" cols="50" className="textArea" onChange={e => this.setState({ textArea: e.target.value })}   >

                </textarea>


                <RadioButtonGroup defaultSelected={"Positive"} onChange={e => this.setState({ selected: e.target.value })}      >
                      <RadioButton
                        value="Positive"
                        label="Positive"
                        style={styles.radioButton}
                      />
                      <RadioButton
                        value="Negative"
                        label="Negative"
                        style={styles.radioButton}
                      />
                </RadioButtonGroup>





                <br/>
                <RaisedButton onClick={this.addReview} label={"Add review"}/>

              </div>

            </div>
        );
    }
}
