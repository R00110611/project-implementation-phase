import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import CreateRecipee from './RecipeeCreation/RecipeeCreation';
import SearchForRecipees from './RecipeeSearch/RecipeeSearch';
import CompareRecipees from './RecipeeComparison/RecipeeComparison'
import AddIngredient from 'material-ui/svg-icons/content/add';
import SearchIngredient from 'material-ui/svg-icons/action/search';
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'

const styles = {
    activeStyle: {
      color: 'white',
      textTransform: 'none',
    },
    inactiveStyle: {
      color: '#a8adb5',
      textTransform: 'none'
    }
};

export default class RecipeeManagementTabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CreateRecipeeTabActive: true,
      SearchForRecipeesTabActive: false,
      CompareRecipeesTabActive: false,
      RecipeesForComparison: []
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.addRecipeeForComparison = this.addRecipeeForComparison.bind(this);
    this.removeRecipeeFromComparison = this.removeRecipeeFromComparison.bind(this);
    this.addRecipeeToFavourites = this.addRecipeeToFavourites.bind(this);
  }

  addRecipeeForComparison(Recipee) {
    var arr = this.state.RecipeesForComparison;

    var existsAlready = false;

    arr.map(Recipee1 => {
      if(Recipee.RecipeeID === Recipee1.RecipeeID)
        existsAlready = true;
    });

    if (existsAlready === false) {
      arr.push(Recipee);
      this.setState({RecipeesForComparison: arr});
      toastr.success('Recipee ' + Recipee.RecipeeName +  ' has been added for comparison!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }

    if(existsAlready === true) {
      toastr.error('Recipee already is in the comparison list...', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }
  }

  removeRecipeeFromComparison(Recipee) {
toastr.success('Recipee has been remove from comparison list', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

    var arr = this.state.RecipeesForComparison;

    arr = arr.filter(function( obj ) {
        return obj.RecipeeID !== Recipee;
    });

    this.setState({RecipeesForComparison: arr});

  }

  addRecipeeToFavourites(Recipee, Rec) {
toastr.success('Recipee ' + Rec.RecipeeName + ' has been added to your favourite list of recipees!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/AddRecipeeToFavourites', {
        RecipeeID: Recipee,
        LoggedInUser: this.props.LoggedInUser
      })
      .then(function (response) {
      })
      .catch(function (error) {
      });



  }


  handleTabChange(e) {
    e.props.value === 1 ? this.setState({CreateRecipeeTabActive: true}) : this.setState({CreateRecipeeTabActive: false})
    e.props.value === 2 ? this.setState({SearchForRecipeesTabActive: true}) : this.setState({SearchForRecipeesTabActive: false})
    e.props.value === 3 ? this.setState({CompareRecipeesTabActive: true}) : this.setState({CompareRecipeesTabActive: false})

  }

    render() {
        return (
          <div>
                    <div className="tabs">
            <Tabs inkBarStyle={{background: 'white'}} tabItemContainerStyle={{background: '#062b68'}}>
              <Tab label="Create a Recipee" icon={<AddIngredient />} style={this.state.CreateRecipeeTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={1}  >
                <CreateRecipee Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} />
              </Tab>

              <Tab label="Search for Recipees" icon={<SearchIngredient />} style={this.state.SearchForRecipeesTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={2} >
                <SearchForRecipees Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} addRecipeeToFavourites={this.addRecipeeToFavourites} addRecipeeForComparison={this.addRecipeeForComparison} removeRecipeeFromComparison={this.removeRecipeeFromComparison} />
              </Tab>

              <Tab label="Compare Recipees" icon={<CompareIngredient />} style={this.state.CompareRecipeesTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={3} >
                <CompareRecipees RecipeesForComparison={this.state.RecipeesForComparison} removeRecipeeFromComparison={this.removeRecipeeFromComparison}  />
              </Tab>

          </Tabs>
            </div>
          </div>
        );
    }
}
