
import React from 'react'

export default class DietComparison extends React.Component {
    render() {
        return (
            <div>
            <div className="toDoDescription">
              Here you can create ingredients which then can be used to create a recipees!

            </div>

              <table>
                { this.props.DietsForComparison.map(
                  Diet =>
                  <tr onClick={ this.props.removeDietFromComparison.bind(null, Diet) } >
                    <td> {Diet.DietID} </td>
                  </tr>
                ) }
              </table>

            </div>
        );
    }
}
