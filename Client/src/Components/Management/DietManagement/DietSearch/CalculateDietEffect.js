
import React from 'react'

export default class CalculateDietEffect extends React.Component {
  constructor(props) {
    super(props);

    this.getEffect = this.getEffect.bind(this);
  }

  getEffect() {
    var year1 =  this.props.DateOfBirth.getFullYear();
    var year2 = new Date().getFullYear();
    var age = year2 - year1;
    var BMR = 0;

    if(this.props.Gender == "Male") {
      BMR = 66 + (13.7 * this.props.weight) + (5 * this.props.height) - (6.8 * age);
    }
    if(this.props.Gender == "Female") {
      BMR = 655 + (9.6 * this.props.weight) + (1.8 * this.props.height) - (4.7 * age);
    }

    var totalCalories = 0;
    var days = [];

    for(var i=0; i<this.props.Diet.DietDays.length; i++) {
      days.push(this.props.Diet.DietDays[i].Day);
      for(var j=0; j<this.props.Diet.DietDays[i].Meals.length; j++) {
        for(var m=0; m<this.props.Diet.DietDays[i].Meals[j].Recipee.RecipeeIngredients.length; m++) {
          totalCalories = totalCalories + this.props.Diet.DietDays[i].Meals[j].Recipee.RecipeeIngredients[m].Calories;
        }
      }
    }


var _ = require('lodash');
days = _.uniq(days);
var daysInDiet = days.length;

    // BMR, days in diet, total calories

    var totalBMR = BMR * daysInDiet;

    var effect = totalCalories - totalBMR;

    var weightChange = "";


    if(effect < 0) {
      weightChange = weightChange + "-" + (Math.abs(effect)/7000).toFixed(2) + " KG";

    }
    if(effect > 0) {
      weightChange = weightChange + "+" + (Math.abs(effect)/7000).toFixed(2) + " KG";

    } else if(effect == 0){

    }



    return weightChange;
  }

    render() {
        return (
            <div>
              {this.props.DateOfBirth != null && this.props.height != null && this.props.weight != null && this.props.Gender != null ? this.getEffect() : 'Please fill details by clicking on username to see this'}
            </div>
        );
    }
}
