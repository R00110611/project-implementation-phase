import React from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActionFavorite from 'material-ui/svg-icons/action/favorite';
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border';
import TextField from 'material-ui/TextField';

const styles = {
  block: {
    maxWidth: 250,
  },
  radioButton: {
    marginBottom: 16,
    width: '90%'
  },
  textAreaFloatingLabelStyle: {
  color: 'white',
  textAlign: 'left',
  width: '100%'
}
};

const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}

export default class DietReviews extends React.Component {
  constructor(props) {
		super(props);

		this.state = {
      textArea: "",
      Recommended: 0,
      selected: "Positive"
		};

    this.addReview = this.addReview.bind(this);
    this.formatDate = this.formatDate.bind(this);
}

addReview() {
  var recc = 0;

  if(this.state.selected === "Positive") {
    recc = 1;
  } else {
    recc = 0;
  }

  var outer = this;
  var axios = require('axios');

  axios.post('http://52.18.248.248:3001/AddDietReview', {
    DietID: this.props.activeDietID,
    Username: this.props.LoggedInUser,
    Review: this.state.textArea,
    Recommended: recc,
    })
    .then(function (response) {

      if(response.data.response === true) {
        outer.props.rowClick(outer.props.ActiveDietObj);
      }
    })
    .catch(function (error) {
      console.log(error);
    });

}

formatDate(date){
  // return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  var dateFormatted = "";
  dateFormatted = date.replace("T", " ");
  dateFormatted = dateFormatted.replace("Z", "");
  dateFormatted = dateFormatted.substring(0, 19);
  return dateFormatted;
}

    render() {
        return (
            <div className="Reviews">

              <h1> Reviews of the Diet </h1>



              <div className="reviewsOfIngredient">
                <table className="ReviewTable">

                <tr>
                <th> Review </th>
                <th> Username </th>
                <th> Type of Review </th>
                <th> Time and Date </th>
                </tr>

                  {this.props.Reviews.map(ReviewInd =>
                    <tr className="ReviewRow">
                        <td className="ReviewTableDate"> {ReviewInd.Review} </td>
                        <td> {ReviewInd.Username} </td>
                        <td> {ReviewInd.Recommended === 0 ? "Negative" : "Positive"} </td>
                        <td> {this.formatDate(ReviewInd.TimeAndDate).toString()} </td>
                    </tr>
                  )}
                </table>
              </div>

              <br/>

              <div className="addReview">
                <h2> Add review </h2>


                <div>
                    <textarea value={this.state.textArea} onChange={e => this.setState({ textArea: e.target.value })} style={{resize: 'vertical', textAlign: 'center', width:'60%', marginLeft: '20%', marginRight: '20%'}} placeholder={this.props.Language === 'English' ? 'Review' : this.props.Language === 'Spanish' ? 'revisión' : 'Recenzja'       }>

                    </textarea>
                    <br/>
                    <span style={this.state.textArea.length > 250 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.textArea.length}/250 </span>

                    <br/><br/>

                </div>





                <div className="RadioButtons2">
                  <RadioButtonGroup defaultSelected={"Positive"} onChange={e => this.setState({ selected: e.target.value })}      >
                        <RadioButton
                          value="Positive"
                          label="Positive"
                          style={styles.radioButton}
                        />
                        <RadioButton
                          value="Negative"
                          label="Negative"
                          style={styles.radioButton}
                        />
                  </RadioButtonGroup>
                </div>




                <br/>
                <RaisedButton backgroundColor="#0a1c35" labelColor='white' disabled={ this.state.textArea.length > 250 ? true : false } onClick={this.addReview} label={"Add"}/>
                <br/><br/>
              </div>
              <br/>
            </div>
        );
    }
}
