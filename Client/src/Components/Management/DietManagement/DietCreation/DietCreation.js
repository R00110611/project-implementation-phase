
import React from 'react'
import ArrowPointingLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import ArrowPointingRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';
import TimePicker from './TimePicker'
import RecipeeSearch from './RecipeeSearch'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import DownArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import LeftArrow from 'material-ui/svg-icons/hardware/keyboard-arrow-left';
import Add from 'material-ui/svg-icons/content/add';
import DeleteIcon from 'material-ui/svg-icons/action/delete';

const styles = {
  customWidth: {
    width: 200,
  },
};
const textFieldStyle = {
  errorStyle: {
    color: 'red'
  },
  normalStyle: {
    color: 'black'
  }
}

const divStyle = {
  activeStyle: {

  },
  activeStyle1: {
    fontWeight: 'bold',
    cursor: 'pointer'
  },
  inactiveStyle1: {
    cursor: 'pointer'
  },
  inactiveStyle: {
    display: 'none'
  },
  hoverStyle: {
    cursor: 'pointer'
  },
  inhoverStyle: {

  },
  activeStyleCreateButton: {
    width: '50%',
    marginLeft: '25%',
    marginRight: '25%',
    color: 'black',
    marginTop: '2%',
    backgroundColor: 'red'
  },
  ActiveStyleDiv: {
    textAlign: 'center'
  },
  InactiveStyleDiv: {
    display: 'none'
  }
}

export default class DietCreation extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
        Hour: 0,
        Minute: 0,
        Second: 0,
        Day: 1,
        Diet: this.props.Diet,
        DietName: "",
        hoverActive: 0,
        SelectMealsActive: false,
        DietDescriptionActive: false,
        RecipeeSearchActive: true,
        RecipeeSummaryActive: false,
        hide: false
     }

      this.handleHourChange = this.handleHourChange.bind(this);
      this.handleMinuteChange = this.handleMinuteChange.bind(this);
      this.handleSecondChange = this.handleSecondChange.bind(this);
      this.DecreaseDay = this.DecreaseDay.bind(this);
      this.IncreaseDay = this.IncreaseDay.bind(this);
      this.DayValidation = this.DayValidation.bind(this);
      this.getTime = this.getTime.bind(this);
      this.createDiet = this.createDiet.bind(this);
      this.handleDietNameChange = this.handleDietNameChange.bind(this);
      this.onClick = this.onClick.bind(this);
      this.onHover = this.onHover.bind(this);
      this.setActive = this.setActive.bind(this);
      this.onClick1 = this.onClick1.bind(this);
      this.hide = this.hide.bind(this);
      this.initialize = this.initialize.bind(this);
  }

initialize() {
  this.setState({
    Hour: 0,
    Minute: 0,
    Second: 0,
    Day: 1,
    Diet: this.props.Diet,
    DietName: "",
    hoverActive: 0,
    SelectMealsActive: false,
    DietDescriptionActive: false,
    RecipeeSearchActive: true,
    RecipeeSummaryActive: false,
    hide: false
  })
}

  hide(nr) {
    if(nr === 1) {
      this.setState({hide: true});
    } else {
      this.setState({hide: false});
    }
  }

onClick1(tabToActivate) {
  if(tabToActivate == 1) {
    this.setState({RecipeeSearchActive: true, RecipeeSummaryActive: false});
  } else {
    this.setState({RecipeeSearchActive: false, RecipeeSummaryActive: true});
  }
}

setActive(tabToActivate) {
  if(tabToActivate === 1) {
    this.setState({RecipeeSearchActive: true, RecipeeSummaryActive: false })
  }

  if(tabToActivate === 2) {
    this.setState({RecipeeSummaryActive: true, RecipeeSearchActive: false })
  }
}


  onHover(activeNr) {
      this.setState({hoverActive: activeNr});
  }


onClick(tabToActivate) {
  if(tabToActivate == 1) {
    if(this.state.SelectMealsActive === true) {
      this.setState({SelectMealsActive: false});
    } else {
      this.setState({DietDescriptionActive: false, SelectMealsActive: true});
    }
  }

  if(tabToActivate == 2) {
    if(this.state.DietDescriptionActive === true) {
      this.setState({DietDescriptionActive: false});
    } else {
      this.setState({SelectMealsActive: false, DietDescriptionActive: true});
    }
  }
}

  createDiet() {
    var DietDays = this.state.Diet.Days.length;

    var max = Math.max.apply(null, this.state.Diet.Days.map(function(item) {
      return item.Day;
    }));

    if(max == DietDays) {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/CreateDiet', {
        username: this.props.LoggedInUser,
        Diet: this.state.Diet,
        DietName: this.state.DietName

      })
      .then(function (response) {
        if(response.data.response === true) {
          toastr.success('Diet created, head over to the Dieting Journey tabs to create a dieting diary!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    } else {
      toastr.error('Days in the diet have to be consecutive!', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }

  }

  getTime() {
    var Hour = this.state.Hour < 10 ? "0" + this.state.Hour : this.state.Hour;
    var Minute = this.state.Minute < 10 ?  "0" + this.state.Minute : this.state.Minute;
    var Second = this.state.Minute < 10 ?  "0" + this.state.Second : this.state.Second;

    return Hour + ":" + Minute + ":" + Second;
  }

  DayValidation(day) {
    var validated = true;

    if(day <= 1) {
      validated = false;
    }

    /* no meals on current day validation */


    return validated;
  }

  IncreaseDay() {
    var currentDay = this.state.Day;
    var outer = this;


        currentDay = currentDay + 1;
        this.setState({Day: currentDay});


  }

  DecreaseDay() {
    if(this.DayValidation(this.state.Day) === true) {
    var currentDay = this.state.Day;
    currentDay = currentDay - 1;
    this.setState({Day: currentDay});
  }

  }

  handleHourChange = (event, index, value) => this.setState({Hour: event.target.value});
  handleMinuteChange = (event, index, value) => this.setState({Minute: event.target.value});
  handleSecondChange = (event, index, value) => this.setState({Second: event.target.value});

  handleDietNameChange(e) {
    this.setState({DietName: e.target.value});
  }

    render() {
        return (
            <div>
                        <div className="DietCreationParent">
                        <br/>
                          <div className="SelectMeals" >
                            <div onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 1)} style={this.state.hoverActive === 1 ? divStyle.hoverStyle : divStyle.inhoverStyle} onClick={this.onClick.bind(this, 1)}>
                            <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 1. Select Meals
                              <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.SelectMealsActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                            </span>
                            </div>

                            <div className="Insides" style={this.state.SelectMealsActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >



                              <br/>

                              <div className="WhatToCompare"> <span onClick={this.onClick1.bind(this, 1)} style={this.state.RecipeeSearchActive === true ? divStyle.activeStyle1 : divStyle.inactiveStyle1 }>Recipee Search </span>  or  <span onClick={this.onClick1.bind(this, 2)} style={this.state.RecipeeSummaryActive === true ? divStyle.activeStyle1 : divStyle.inactiveStyle1 }> Diet Summary view </span> </div>

                                <div style={this.state.RecipeeSearchActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >

                                <br/>

                                <div style={{backgroundColor: '#556889', width: '50%', marginLeft: '25%', marginRight: '25%', textAlign: 'center'}}>
                                  <div style={this.state.hide === true ? divStyle.inactiveStyle : divStyle.activeStyle} >
                                  <span> Add meal at the following day & time: </span> <br/>
                                  <ArrowPointingLeft onClick={this.DecreaseDay} /> Day {this.state.Day} <ArrowPointingRight onClick={this.IncreaseDay} />
                                  <br/><br/>
                                  <TimePicker setTime={this.setTime} Hour={this.state.Hour} Minute={this.state.Minute} Second={this.state.Second} handleHourChange={this.handleHourChange} handleMinuteChange={this.handleMinuteChange} handleSecondChange={this.handleSecondChange}/>
                                  <br/>
                                  </div>
                                </div>

                                <RecipeeSearch Language={this.props.Language} hide={this.hide} Time={this.getTime()} Day={this.state.Day} LoggedInUser={this.props.LoggedInUser} addRecipee={this.props.addRecipee}  />



                                </div>

                                <div style={this.state.RecipeeSummaryActive === true ? divStyle.activeStyle : divStyle.inactiveStyle}>
                                                  <div className="DietSummary" >
                                                        <br/> <br/>




                                                        <div className="dietoverviewsearch" >
                                                                  {this.state.Diet.Days.length === 0 ? <div style={{textAlign: 'center'}}> Select meals to diet first. </div> : null}
                                                                  <div style={this.state.Diet.Days.length === 0 ? divStyle.InactiveStyleDiv : divStyle.ActiveStyleDiv }> <h3> Diet Meal Summary </h3> </div>
                                                        <table className="CompareTable" style={{listStyle: 'none'}}>

                                                        {this.state.Diet.Days.map(DietDay =>
                                                          <tr> <td style={{backgroundColor: '#546582'}}> Day: {DietDay.Day} </td>
                                                          <td style={{backgroundColor: 'white', borderStyle: 'solid', borderColor: '#055b21'}} >
                                                          {DietDay.Meals.map(Meal =>

                                                            <td style={{backgroundColor: 'red', borderStyle: 'solid', borderColor: 'yellow'}}> {Meal.Time}
                                                            <td>
                                                              {Meal.Recipee.RecipeeName}
                                                            </td>
                                                            <li>
                                                              Calories: {Meal.Recipee.TotalCalories}
                                                            </li>
                                                            <td> <DeleteIcon onClick={this.props.removeRecipee.bind(null, DietDay.Day, Meal.Time, Meal.Recipee.RecipeeID)} style={{cursor: 'pointer'}}/> </td>
                                                            </td>




                                                          )}</td>

                                                          </tr>
                                                        )}
                                                        </table>
                                                        <br/>

                                                        <div style={this.state.Diet.Days.length === 0 ? divStyle.InactiveStyleDiv : divStyle.ActiveStyleDiv } >
                                                          <div> <h3> Diet Expected Effects </h3> </div>
                                                          <div> {this.props.getEffect() === "Please fill in your details" ? "Please fill details by clicking on username to see this" : <span> The expected effect of the diet on your weight is: {this.props.getEffect()} </span> }  </div>
                                                        </div>

                                                        </div>



                                                      <br/>

                                                    </div>
                                                    <br/>
                                </div>

                            </div>
                        </div>


                          <br/>

                          <div className="DietDescription">
                            <div  onMouseOut={this.onHover.bind(this, -1)} onMouseOver={this.onHover.bind(this, 2)} style={this.state.hoverActive === 2 ? divStyle.hoverStyle : divStyle.inhoverStyle} onClick={this.onClick.bind(this, 2)}>
                            <span style={{display: 'inline-block', width: '97.5%', color: 'white', marginLeft: '2.5%', fontWeight: 'bold'}}> 2. Enter Diet Description
                              <span style={{float: 'right', marginRight: '2.5%'}}> {this.state.DietDescriptionActive === false ? <LeftArrow /> : <DownArrow /> } </span>
                            </span>
                            </div>

                            <div className="Insides" style={this.state.DietDescriptionActive === true ? divStyle.activeStyle : divStyle.inactiveStyle} >
                            <br/>
                            <div style={{textAlign: 'center'}}>
                              <input onChange={this.handleDietNameChange} type="text" value={this.state.DietName} placeholder={this.props.Language === 'English' ? 'Name *' : this.props.Language === 'Spanish' ? 'Nombre *' : 'Nazwa *'       } style={{textAlign: 'center'}} />
                              <br/>
                              <span style={this.state.DietName.length > 40 ? textFieldStyle.errorStyle : textFieldStyle.normalStyle} > {this.state.DietName.length}/40 </span>

                              <br/> <br/>
                            </div>


                            </div>

                          </div>



                          <div>
                            <RaisedButton
                              label={this.props.Language === 'English' ? 'Create' : this.props.Language === 'Spanish' ? 'Crear' : 'Stwórz'       }
                              onClick={this.createDiet}
                              backgroundColor={'navy'}
                              disabled={this.state.Diet.Days.length == 0 || this.state.DietName.length == 0 || this.state.DietName.length > 40? true : false}
                              style={divStyle.activeStyleCreateButton}
                              labelColor="white"  >
                            </RaisedButton>
                          </div>
                          <br/>
                        </div>



                        <br/>
            </div>
        );
    }
}
