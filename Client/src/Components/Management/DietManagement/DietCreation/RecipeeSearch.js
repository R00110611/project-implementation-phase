import React from 'react'
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import ActiveRecipee from './ActiveRecipee'
var _ = require('lodash');
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';
import Favourite from 'material-ui/svg-icons/action/grade';
import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'
import See from 'material-ui/svg-icons/action/zoom-in'
import Help from 'material-ui/svg-icons/action/help';
import Add from 'material-ui/svg-icons/content/add';
import TimePicker from './TimePicker'

const styles = {
  activeRow: {
    backgroundColor: 'white'
  },
  activeStyle: {

  },
  inactiveStyle: {
    display: 'none'
  }
}

export default class RecipeeSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      RecipeeName: "",
      IncludedIngredients: [],
      ExcludedIngredients: [],
      RecipeeLanguage: "All",
      onlyMineRecipees: false,
      LanguageValue: 1,
      selected: "IncludedIngredients",
      IngredientName: "",
      onlyFavouriteRecipees: false,
      RecipeesList: [],
      LoggedInUser: this.props.LoggedInUser,
      recipees: [

      ],
      activeRecipeeID: null,
      ActiveRecipee: {},
      RecipeeReviews: [],
      initialSearch: 0
    }

    this.SearchRecipee = this.SearchRecipee.bind(this);
    this.check = this.check.bind(this);
    this.handleRecipeeLanguageChange = this.handleRecipeeLanguageChange.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.rowClick = this.rowClick.bind(this);
    this.removeIngredient = this.removeIngredient.bind(this);
    this.getReviews = this.getReviews.bind(this);
    this.setList = this.setList.bind(this);
    this.goBack = this.goBack.bind(this);
 this.Help = this.Help.bind(this);
  }

  Help() {
    var Information = "";

    if(this.props.Language == "English") {
      Information = "Included Ingredients list - get recipees with only ingredients you have. \n Excluded ingredient list - get recipees without those ingredients. ";
    }
    if(this.props.Language == "Polish") {
      Information = "Lista komponentów włączonych - pobierz przepisy zawierające tylko dostępne składniki. \n Lista wykluczonych składników - uzyskać przepisy bez tych składników.";
    }
    if(this.props.Language == "Spanish") {
      Information = "Lista de Ingredientes incluidos - obtener recetas con sólo los ingredientes que tiene. \n Lista de ingredientes excluidos: obtenga recetas sin esos ingredientes.";
    }

    toastr.info(Information, 'Help', {positionClass: 'toast-top-center', preventDuplicates: true});
  }


goBack() {
  this.setState({activeRecipeeID: null}, this.props.hide(2));
}

setList(event) {

  if(event.target.value === "Included") {
    this.setState({selected: "IncludedIngredients"});
  } else {
    this.setState({selected: "ExcludedIngredients"})
  }
}

  getReviews(Recipee) {

    var outer = this;
    var axios = require('axios');



    axios.post('http://52.18.248.248:3001/RecipeeReviews', {
        RecipeeID: Recipee
      })
      .then(function (response) {

        var arr = [];
        response.data.response.map(function(name, index) {
             arr.push({
               ID: name.ID,
               RecipeeID: name.RecipeeID,
               Username: name.Username,
               UsernameID: name.UsernameID,
               Review: name.Review,
               Recommended: name.Recommended,
               TimeAndDate: name.TimeAndDate

             });
        })

        outer.setState({RecipeeReviews: arr});
      })
      .catch(function (error) {
        console.log(error);
      });


  }

  rowClick(Recipee) {
    this.getReviews(Recipee.RecipeeID);
    this.setState({ActiveRecipee: Recipee});
    this.setState({activeRecipeeID: Recipee.RecipeeID});
    this.props.hide(1);
  }

  handleRecipeeLanguageChange(event, index, val) {
    event.target.value === "All" ? this.setState({RecipeeLanguage: 'All'}, this.SearchRecipee) : null;
    event.target.value === 'English' ? this.setState({RecipeeLanguage: 'English'}, this.SearchRecipee) : null;
    event.target.value === 'Polish' ? this.setState({RecipeeLanguage: 'Polish'}, this.SearchRecipee) : null;
    event.target.value === 'Spanish' ? this.setState({RecipeeLanguage: 'Spanish'}, this.SearchRecipee) : null;
    this.setState({LanguageValue: val});
  }

  removeIngredient(IngredientName, Type) {
    var saas = Type == 1 ? "Included Ingredients" : "Excluded Ingredients";
    toastr.success('Ingredient has been removed from the ' + saas + ' list', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

    var arr = Type == 1 ? this.state.IncludedIngredients : this.state.ExcludedIngredients;

    arr = arr.filter(function( obj ) {
        return obj.Name != IngredientName;
    });

    if(Type == 1) {
    this.setState({IncludedIngredients: arr}, this.SearchRecipee);
    } else {
    this.setState({ExcludedIngredients: arr}, this.SearchRecipee);
    }

  }

  addIngredient(e, i, v) {

    var check = this.state.selected == "IncludedIngredients" ? this.state.IncludedIngredients : this.state.ExcludedIngredients;

    var outer = this;

    var index = _.findIndex(check, function(o) {
      return o.Name == outer.state.IngredientName;
    });

    if(this.state.IngredientName.length == 0) {
      toastr.error('Ingredient cannot be empty!', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }


    if(this.state.IngredientName.length != 0 || this.state.IngredientName != "" && index == -1  ) {
    var incIngr = this.state.IncludedIngredients;
    var exclIngr = this.state.ExcludedIngredients;

    if(index == -1) {
      this.state.selected === "IncludedIngredients" ? incIngr.push({Name: this.state.IngredientName}) : exclIngr.push({Name: this.state.IngredientName});
      this.state.selected === "IncludedIngredients" ? this.setState({IncludedIngredients: incIngr}, this.SearchRecipee) : this.setState({ExcludedIngredients: exclIngr}, this.SearchRecipee);
    }


    var listType = this.state.selected;

    if(listType == "IncludedIngredients" && index == -1) {
      listType = "Included Ingredients";
      toastr.success('Added ingredient to ' + listType + " list", 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }

    if(listType == "ExcludedIngredients" && index == -1) {
      listType="Excluded Ingredients"
      toastr.success('Added ingredient to ' + listType + " list", 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }

    if(index != -1) {
    toastr.error('Ingredient already exists!', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
    }

  }

  }


  check(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

  //  console.log(value, name);

    if(name === "Mine") {
      this.setState({onlyMineRecipees: value}, this.SearchRecipee);
    }

    if(name === "Favourite") {
      this.setState({onlyFavouriteRecipees: value}, this.SearchRecipee);
    }

    //name === "Mine" ? value === true ? this.setState({onlyMineRecipees: true}, this.SearchRecipee) : this.setState({onlyMineRecipees: false}, this.SearchRecipee) : value === true ? this.setState({onlyFavouriteRecipees: true}, this.SearchRecipee) : this.setState({onlyFavouriteRecipees: false}, this.SearchRecipee)
  }

  SearchRecipee() {
    var outer = this;
    var axios = require('axios');

    axios.post('http://52.18.248.248:3001/SearchForRecipee', {
        state: this.state
      })
      .then(function (response) {
         outer.setState({RecipeesList: response.data.response, initialSearch: 1});
      })
      .catch(function (error) {
        console.log(error);
      });


  }

    render() {
        return (
            <div >
            <div className="SearchForRecipees1" style={this.state.activeRecipeeID === null ? styles.activeStyle : styles.inactiveStyle }>
                                  <br/>


                                        <input onChange={e => this.setState({ RecipeeName: e.target.value }, this.SearchRecipee)} type="text" value={this.state.RecipeeName} placeholder={this.props.Language === 'English' ? 'Recipee Name' : this.props.Language === 'Spanish' ? 'Nombre' : 'Nazwa przepisu'       } style={{textAlign: 'center'}} />
                                        <br/> <br/>

                                                <div>
                                                      Only mine recipees:
                                                      <input
                                                        name="Mine"
                                                        type="checkbox"
                                                        checked={this.state.onlyMineRecipees}
                                                        onChange={this.check}
                                                      />

                                                      <br/>

                                                      Only mine favourite recipees:
                                                      <input
                                                        name="Favourite"
                                                        type="checkbox"
                                                        checked={this.state.onlyFavouriteRecipees}
                                                        onChange={this.check}
                                                      />
                                                  </div>






                                        <span style={{color: 'black'}}> {this.props.Language === 'English' ? 'Language' : this.props.Language === 'Spanish' ? 'Idioma' : 'Język'} </span>

                                        <select onChange={this.handleRecipeeLanguageChange} >
                                          <option value="All" selected={this.state.IngredientLanguage==="All" ? true : false} > {this.props.Language === 'English' ? 'All' : this.props.Language === 'Spanish' ? 'todas' : 'Wszystkie'} </option>
                                          <option value="English" selected={this.state.IngredientLanguage==="English" ? true : false} > {this.props.Language === 'English' ? 'English' : this.props.Language === 'Spanish' ? 'Inglés' : 'Angielski'} </option>
                                          <option value="Polish" selected={this.state.IngredientLanguage==="Polish" ? true : false}  > {this.props.Language === 'English' ? 'Polish' : this.props.Language === 'Spanish' ? 'Polaco' : 'Polski'} </option>
                                          <option value="Spanish" selected={this.state.IngredientLanguage==="Spanish" ? true : false}  > {this.props.Language === 'English' ? 'Spanish' : this.props.Language === 'Spanish' ? 'Español' : 'Hiszpanski'} </option>
                                        </select>





                                        <br/><br/>


                                        <div className="wrapper11">
                                        <Help onClick={this.Help} style={{cursor: 'pointer', color: 'white'}} />
                  <div>    <h2> Ingredients Filter  </h2> </div>

                          <input onChange={e => this.setState({ IngredientName: e.target.value })} type="text" value={this.state.IngredientName} placeholder={this.props.Language === 'English' ? 'Ingredient Name' : this.props.Language === 'Spanish' ? 'Nombre' : 'Nazwa skladnika'       } style={{textAlign: 'center'}} />
                            <br/> <br/>

                                <div className="addIngredient">


                                      <div onChange={this.setList.bind(this)} >
                                        <input type="radio" value="Included" name="ListType" checked={this.state.selected==="IncludedIngredients" ? true : false} /> Add to included ingredient list
                                        <br/>
                                        <input type="radio" value="Excluded" name="ListType"/> Add to excluded ingredient list
                                        </div>



                                        <br/>




                                        <FlatButton
                                        label="Add Ingredient"
                                        onClick={this.addIngredient}
                                        backgroundColor={'white'}
                                        />




                                              <div className="includedIngredients">
                                              Include Ingredients:
                                                <table>
                                                  {this.state.IncludedIngredients.map(Ingredient =>
                                                    <td style={{backgroundColor: 'white'}} onClick={this.removeIngredient.bind(null, Ingredient.Name, 1)} > {Ingredient.Name} </td>
                                                  )}
                                                </table>
                                              </div>

                                                <div className="excludedIngredients">
                                                Exclude Ingredients:
                                                  <table>
                                                    {this.state.ExcludedIngredients.map(Ingredient =>
                                                      <td style={{backgroundColor: 'white'}} onClick={this.removeIngredient.bind(null, Ingredient.Name, 2)} > {Ingredient.Name} </td>
                                                    )}
                                                  </table>
                                                </div>
                                                <br/><br/>
                                    </div>
                                  </div>
                                  <br/> <br/>
          </div>

          <div className="RecipeesList1" style={this.state.activeRecipeeID === null && this.state.initialSearch != 0 ? styles.activeStyle : styles.inactiveStyle }>
                          <table className="RecipeeListTable">
                          <tr>
                            <th> </th>
                            <th>Photo  </th>
                            <th>Name </th>
                            <th>Calories</th>
                          </tr>

                            { this.state.RecipeesList.map(Recipee =>
                              <tr className="RecipeeListTableRow"  >
                                <td> <Add onClick={this.props.addRecipee.bind(null, this.props.Day, this.props.Time, Recipee)} style={{color: 'white', cursor: 'pointer'}}/> </td>
                                <td> {Recipee.RecipeePhoto === null ? <img src={"http://52.18.248.248:3001/RecipeeImages/DefaultRecipeePhoto.png"} height={90} width={90} /> : <img src={"http://52.18.248.248:3001/" + Recipee.RecipeePhoto} height={90} width={90} />} </td>
                                <td style={{textOverflow: 'ellipsis'}}> {Recipee.RecipeeName} </td>
                                <td> {Recipee.TotalCalories} </td>
                                <td> <See style={{color:'white', cursor: 'pointer'}} onClick={this.rowClick.bind(null, Recipee)}/> </td>
                           </tr>
                         )}

                          </table>
                        </div>


                        <div style={this.state.activeRecipeeID != null ? styles.activeStyle : styles.inactiveStyle }>
                        <ActiveRecipee
                                          activeRecipeeID={this.state.activeRecipeeID }
                                          ActiveRecipee={  this.state.ActiveRecipee   }
                                          goBack={this.goBack}
                                          Language={this.props.Language}
                                          LoggedInUser={this.props.LoggedInUser}
                                          Reviews={this.state.RecipeeReviews}
                                          rowClick={this.rowClick}
                                          Language={this.props.Language}
                        />
                        </div>

      <br/>
            </div>
        );
    }
}
