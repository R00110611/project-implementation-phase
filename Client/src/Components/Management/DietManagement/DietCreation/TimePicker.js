
import React from 'react'
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

export default class TimePicker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

    }
  }

    render() {
        return (
            <div>
            Hour:
            <select onChange={this.props.handleHourChange} >
              <option selected={this.props.Hour === 0} value={0}> 00 </option>
              <option selected={this.props.Hour === 1} value={1}> 01 </option>
              <option selected={this.props.Hour === 2} value={2}> 02 </option>
              <option selected={this.props.Hour === 3} value={3}> 03 </option>
              <option selected={this.props.Hour === 4} value={4}> 04 </option>
              <option selected={this.props.Hour === 5} value={5}> 05 </option>
              <option selected={this.props.Hour === 6} value={6}> 06 </option>
              <option selected={this.props.Hour === 7} value={7}> 07 </option>
              <option selected={this.props.Hour === 8} value={8}> 08 </option>
              <option selected={this.props.Hour === 9} value={9}> 09 </option>
              <option selected={this.props.Hour === 10} value={10}> 10 </option>
              <option selected={this.props.Hour === 11} value={11}> 11 </option>
              <option selected={this.props.Hour === 12} value={12}> 12 </option>
              <option selected={this.props.Hour === 13} value={13}> 13 </option>
              <option selected={this.props.Hour === 14} value={14}> 14 </option>
              <option selected={this.props.Hour === 15} value={15}> 15 </option>
              <option selected={this.props.Hour === 16} value={16}> 16 </option>
              <option selected={this.props.Hour === 17} value={17}> 17 </option>
              <option selected={this.props.Hour === 18} value={18}> 18 </option>
              <option selected={this.props.Hour === 19} value={19}> 19 </option>
              <option selected={this.props.Hour === 20} value={20}> 20 </option>
              <option selected={this.props.Hour === 21} value={21}> 21 </option>
              <option selected={this.props.Hour === 22} value={22}> 22 </option>
              <option selected={this.props.Hour === 23} value={23}> 23 </option>
            </select>

            Minute:
            <select onChange={this.props.handleMinuteChange} >
              <option selected={this.props.Minute === 0} value={0}> 00 </option>
              <option selected={this.props.Minute === 1} value={1}> 01 </option>
              <option selected={this.props.Minute === 2} value={2}> 02 </option>
              <option selected={this.props.Minute === 3} value={3}> 03 </option>
              <option selected={this.props.Minute === 4} value={4}> 04 </option>
              <option selected={this.props.Minute === 5} value={5}> 05 </option>
              <option selected={this.props.Minute === 6} value={6}> 06 </option>
              <option selected={this.props.Minute === 7} value={7}> 07 </option>
              <option selected={this.props.Minute === 8} value={8}> 08 </option>
              <option selected={this.props.Minute === 9} value={9}> 09 </option>
              <option selected={this.props.Minute === 10} value={10}> 10 </option>
              <option selected={this.props.Minute === 11} value={11}> 11 </option>
              <option selected={this.props.Minute === 12} value={12}> 12 </option>
              <option selected={this.props.Minute === 13} value={13}> 13 </option>
              <option selected={this.props.Minute === 14} value={14}> 14 </option>
              <option selected={this.props.Minute === 15} value={15}> 15 </option>
              <option selected={this.props.Minute === 16} value={16}> 16 </option>
              <option selected={this.props.Minute === 17} value={17}> 17 </option>
              <option selected={this.props.Minute === 18} value={18}> 18 </option>
              <option selected={this.props.Minute === 19} value={19}> 19 </option>
              <option selected={this.props.Minute === 20} value={20}> 20 </option>
              <option selected={this.props.Minute === 21} value={21}> 21 </option>
              <option selected={this.props.Minute === 22} value={22}> 22 </option>
              <option selected={this.props.Minute === 23} value={23}> 23 </option>
              <option selected={this.props.Minute === 24} value={24}> 24 </option>
              <option selected={this.props.Minute === 25} value={25}> 25 </option>
              <option selected={this.props.Minute === 26} value={26}> 26 </option>
              <option selected={this.props.Minute === 27} value={27}> 27 </option>
              <option selected={this.props.Minute === 28} value={28}> 28 </option>
              <option selected={this.props.Minute === 29} value={29}> 29 </option>
              <option selected={this.props.Minute === 30} value={30}> 30 </option>
              <option selected={this.props.Minute === 31} value={31}> 31 </option>
              <option selected={this.props.Minute === 32} value={32}> 32 </option>
              <option selected={this.props.Minute === 33} value={33}> 33 </option>
              <option selected={this.props.Minute === 34} value={34}> 34 </option>
              <option selected={this.props.Minute === 35} value={35}> 35 </option>
              <option selected={this.props.Minute === 36} value={36}> 36 </option>
              <option selected={this.props.Minute === 37} value={37}> 37 </option>
              <option selected={this.props.Minute === 38} value={38}> 38 </option>
              <option selected={this.props.Minute === 39} value={39}> 39 </option>
              <option selected={this.props.Minute === 40} value={40}> 40 </option>
              <option selected={this.props.Minute === 41} value={41}> 41 </option>
              <option selected={this.props.Minute === 42} value={42}> 42 </option>
              <option selected={this.props.Minute === 43} value={43}> 43 </option>
              <option selected={this.props.Minute === 43} value={44}> 44 </option>
              <option selected={this.props.Minute === 44} value={45}> 45 </option>
              <option selected={this.props.Minute === 45} value={46}> 46 </option>
              <option selected={this.props.Minute === 46} value={47}> 47 </option>
              <option selected={this.props.Minute === 47} value={48}> 48 </option>
              <option selected={this.props.Minute === 48} value={49}> 49 </option>
              <option selected={this.props.Minute === 49} value={50}> 50 </option>
              <option selected={this.props.Minute === 50} value={51}> 51 </option>
              <option selected={this.props.Minute === 51} value={52}> 52 </option>
              <option selected={this.props.Minute === 52} value={53}> 53 </option>
              <option selected={this.props.Minute === 53} value={54}> 54 </option>
              <option selected={this.props.Minute === 54} value={55}> 55 </option>
              <option selected={this.props.Minute === 55} value={56}> 56 </option>
              <option selected={this.props.Minute === 56} value={57}> 57 </option>
              <option selected={this.props.Minute === 57} value={58}> 58 </option>
              <option selected={this.props.Minute === 58} value={59}> 59 </option>

            </select>

            Second:
            <select onChange={this.props.handleSecondChange} >
              <option selected={this.props.Second === 0} value={0}> 00 </option>
              <option selected={this.props.Second === 1} value={1}> 01 </option>
              <option selected={this.props.Second === 2} value={2}> 02 </option>
              <option selected={this.props.Second === 3} value={3}> 03 </option>
              <option selected={this.props.Second === 4} value={4}> 04 </option>
              <option selected={this.props.Second === 5} value={5}> 05 </option>
              <option selected={this.props.Second === 6} value={6}> 06 </option>
              <option selected={this.props.Second === 7} value={7}> 07 </option>
              <option selected={this.props.Second === 8} value={8}> 08 </option>
              <option selected={this.props.Second === 9} value={9}> 09 </option>
              <option selected={this.props.Second === 10} value={10}> 10 </option>
              <option selected={this.props.Second === 11} value={11}> 11 </option>
              <option selected={this.props.Second === 12} value={12}> 12 </option>
              <option selected={this.props.Second === 13} value={13}> 13 </option>
              <option selected={this.props.Second === 14} value={14}> 14 </option>
              <option selected={this.props.Second === 15} value={15}> 15 </option>
              <option selected={this.props.Second === 16} value={16}> 16 </option>
              <option selected={this.props.Second === 17} value={17}> 17 </option>
              <option selected={this.props.Second === 18} value={18}> 18 </option>
              <option selected={this.props.Second === 19} value={19}> 19 </option>
              <option selected={this.props.Second === 20} value={20}> 20 </option>
              <option selected={this.props.Second === 21} value={21}> 21 </option>
              <option selected={this.props.Second === 22} value={22}> 22 </option>
              <option selected={this.props.Second === 23} value={23}> 23 </option>
              <option selected={this.props.Second === 24} value={24}> 24 </option>
              <option selected={this.props.Second === 25} value={25}> 25 </option>
              <option selected={this.props.Second === 26} value={26}> 26 </option>
              <option selected={this.props.Second === 27} value={27}> 27 </option>
              <option selected={this.props.Second === 28} value={28}> 28 </option>
              <option selected={this.props.Second === 29} value={29}> 29 </option>
              <option selected={this.props.Second === 30} value={30}> 30 </option>
              <option selected={this.props.Second === 31} value={31}> 31 </option>
              <option selected={this.props.Second === 32} value={32}> 32 </option>
              <option selected={this.props.Second === 33} value={33}> 33 </option>
              <option selected={this.props.Second === 34} value={34}> 34 </option>
              <option selected={this.props.Second === 35} value={35}> 35 </option>
              <option selected={this.props.Second === 36} value={36}> 36 </option>
              <option selected={this.props.Second === 37} value={37}> 37 </option>
              <option selected={this.props.Second === 38} value={38}> 38 </option>
              <option selected={this.props.Second === 39} value={39}> 39 </option>
              <option selected={this.props.Second === 40} value={40}> 40 </option>
              <option selected={this.props.Second === 41} value={41}> 41 </option>
              <option selected={this.props.Second === 42} value={42}> 42 </option>
              <option selected={this.props.Second === 43} value={43}> 43 </option>
              <option selected={this.props.Second === 43} value={44}> 44 </option>
              <option selected={this.props.Second === 44} value={45}> 45 </option>
              <option selected={this.props.Second === 45} value={46}> 46 </option>
              <option selected={this.props.Second === 46} value={47}> 47 </option>
              <option selected={this.props.Second === 47} value={48}> 48 </option>
              <option selected={this.props.Second === 48} value={49}> 49 </option>
              <option selected={this.props.Second === 49} value={50}> 50 </option>
              <option selected={this.props.Second === 50} value={51}> 51 </option>
              <option selected={this.props.Second === 51} value={52}> 52 </option>
              <option selected={this.props.Second === 52} value={53}> 53 </option>
              <option selected={this.props.Second === 53} value={54}> 54 </option>
              <option selected={this.props.Second === 54} value={55}> 55 </option>
              <option selected={this.props.Second === 55} value={56}> 56 </option>
              <option selected={this.props.Second === 56} value={57}> 57 </option>
              <option selected={this.props.Second === 57} value={58}> 58 </option>
              <option selected={this.props.Second === 58} value={59}> 59 </option>

            </select>



            </div>
        );
    }
}
