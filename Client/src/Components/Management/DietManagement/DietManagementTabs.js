import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';

import CreateAnDiet from './DietCreation/DietCreation';
import CompareDiet from './DietComparison/DietComparison';
import SearchForDiet from './DietSearch/DietSearch';

import AddIngredient from 'material-ui/svg-icons/content/add';
import SearchIngredient from 'material-ui/svg-icons/action/search';
import CompareIngredient from 'material-ui/svg-icons/maps/local-library';

import toastr from 'toastr/toastr.js'
import 'toastr/package/build/toastr.min.css'

const styles = {
    activeStyle: {
      color: 'white',
      textTransform: 'none',
    },
    inactiveStyle: {
      color: '#a8adb5',
      textTransform: 'none'
    }
};

export default class DietManagementTabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      CreateDietTabActive: true,
      SearchForDietTabActive: false,
      CompareDietTabActive: false,
      index: -1,
      Diet: {
        Days: [

        ]},
      height: null,
      weight: null,
      DateOfBirth: null,
      Gender: null,
      DietsForComparison: []
    };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.addRecipee = this.addRecipee.bind(this);
    this.removeRecipee = this.removeRecipee.bind(this);
    this.addDietForComparison = this.addDietForComparison.bind(this);
    this.removeDietFromComparison = this.removeDietFromComparison.bind(this);
    this.addDietToFavourites = this.addDietToFavourites.bind(this);
    this.getEffect = this.getEffect.bind(this);
  }

  getEffect() {
    var year1 = this.state.DateOfBirth != null ? this.state.DateOfBirth.getFullYear() : null;
    var year2 = new Date().getFullYear();
    var age = year2 - year1;
    var BMR = 0;


    if(this.state.Gender == "Male") {
      BMR = 66 + (13.7 * this.state.weight) + (5 * this.state.height) - (6.8 * age);
    }
    if(this.state.Gender == "Female") {
      BMR = 655 + (9.6 * this.state.weight) + (1.8 * this.state.height) - (4.7 * age);
    }

    var totalCalories = 0;
    var days = [];

    for(var i=0; i<this.state.Diet.Days.length; i++) {
      days.push(this.state.Diet.Days[i].Day);
      for(var j=0; j<this.state.Diet.Days[i].Meals.length; j++) {
        totalCalories = totalCalories + this.state.Diet.Days[i].Meals[j].Recipee.TotalCalories;
      }
    }

  var _ = require('lodash');
  days = _.uniq(days);
  var daysInDiet = days.length;

    // BMR, days in diet, total calories

    var totalBMR = BMR * daysInDiet;

    var effect = totalCalories - totalBMR;

    var weightChange = "";


    if(effect < 0) {
      weightChange = weightChange + "-" + (Math.abs(effect)/7000).toFixed(2) + " KG";

    }
    if(effect > 0) {
      weightChange = weightChange + "+" + (Math.abs(effect)/7000).toFixed(2) + " KG";

    } else if(effect == 0){

    }



    if(this.state.DateOfBirth === null || this.state.DateOfBirth === null || this.state.Gender === null || this.state.weight === null || this.state.height === null || this.state.Diet.Days.length === 0 ) {

      weightChange = "Please fill in your details";
    }



    return weightChange;
  }


addDietForComparison(Diet1) {
  var arr = this.state.DietsForComparison;

  var existsAlready = false;

  arr.map(Diet => {
    if(Diet.DietID === Diet1.DietID)
      existsAlready = true;
  });

  if (existsAlready === false) {
    arr.push(Diet1);
    this.setState({DietsForComparison: arr});
toastr.success('Added diet to the comparison tab!.', 'Success!', {positionClass: 'toast-top-center'});
  }

  if(existsAlready === true) {
    toastr.error('Diet already is in the comparison tab', 'Error!', {positionClass: 'toast-top-center'});
  }
}

removeDietFromComparison(Diet) {
  var arr = this.state.DietsForComparison;

  arr = arr.filter(function( obj ) {
      return obj.DietID !== Diet.DietID;
  });

  this.setState({DietsForComparison: arr});

}

addDietToFavourites(Diet) {
  var outer = this;
  var axios = require('axios');

toastr.success('Added diet to favourite list!', 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

  axios.post('http://52.18.248.248:3001/AddDietToFavourites', {
      DietID: Diet.DietID,
      LoggedInUser: this.props.LoggedInUser
    })
    .then(function (response) {
    })
    .catch(function (error) {

    });

}



  handleTabChange(e) {
    e.props.value === 1 ? this.setState({CreateDietTabActive: true}) : this.setState({CreateDietTabActive: false})
    e.props.value === 2 ? this.setState({SearchForDietTabActive: true}) : this.setState({SearchForDietTabActive: false})
    e.props.value === 3 ? this.setState({CompareDietTabActive: true}) : this.setState({CompareDietTabActive: false})
  }

  addRecipee(Day, MealTime, MealRecipee) {

    /* if day not already exist add else create new one*/
    var _ = require('lodash');
    var arr = [];
    var index = _.findIndex(this.state.Diet.Days, function(o) { return o.Day == Day    });

    if(index == -1) {
      var s = {Day: Day,  Meals: [ {Time: MealTime, Recipee: MealRecipee} ] };
      arr = this.state.Diet;
      arr.Days.push(s);
        toastr.success('Meal ' + MealRecipee.RecipeeName + '  has been added to the diet on Day: '+ Day +' Time: ' + MealTime, 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});
    } else {
      var s = {Time: MealTime, Recipee: MealRecipee};
      var arr = this.state.Diet;
      var index1 = _.findIndex(arr.Days[index].Meals, function(o) { return o.Time === MealTime   });
      if(index1 == -1) {
      arr.Days[index].Meals.push(s);
        toastr.success('Meal ' + MealRecipee.RecipeeName + '  has been added to the diet on Day: '+ Day +' Time: ' + MealTime, 'Success!', {positionClass: 'toast-top-center', preventDuplicates: true});

      } else {
        toastr.error('Meal already exists at that time and day!', 'Error!', {positionClass: 'toast-top-center', preventDuplicates: true});
      }

    }

    arr.Days.sort(function(a, b) {
        return a.Day - b.Day;
    });

    for(var i=0; i<arr.Days.length; i++ ){
    arr.Days[i].Meals.sort(function(a, b) {
        return parseInt(a.Time) - parseInt(b.Time);
    });
  }

    this.setState({Diet: arr});

  }

  removeRecipee(Day, MealTime, MealRecipee) {
    var _ = require('lodash');
    var arr = this.state.Diet;


    var index = _.findIndex(arr.Days, function(o) { return o.Day == Day    });
    var index1 = _.findIndex(arr.Days[index].Meals, function(o) { return o.Recipee.RecipeeID == MealRecipee && o.Time == MealTime   });


    var mealsLength = arr.Days[index].Meals.length;

    if(mealsLength == 1) {
      // remove day
      arr.Days.splice(index, 1);


    } else {
      // remove meal
      arr.Days[index].Meals.splice(index1, 1);

    }

    arr.Days.sort(function(a, b) {
        return a.Day - b.Day;
    });

    for(var i=0; i<arr.Days.length; i++ ){
    arr.Days[i].Meals.sort(function(a, b) {
        return parseInt(a.Time) - parseInt(b.Time);
    });
  }

    this.setState({Diet: arr});

  }

  componentWillMount() {

      var outer = this;
      var axios = require('axios');

      axios.post('http://52.18.248.248:3001/getUserProperties', {
          username: this.props.LoggedInUser
        })
        .then(function (response) {

          outer.setState({
            height: response.data.response.Height,
            weight: response.data.response.Weight,
            DateOfBirth: response.data.response.DateOfBirth != null || response.data.response.DateOfBirth === undefined ? new Date(response.data.response.DateOfBirth) : null,
            Gender: response.data.response.Gender
          });

          outer.setState({open: true});
        })
        .catch(function (error) {
          console.log(error);
        });

  }

    render() {
        return (
          <div>
                    <div className="tabs">
            <Tabs inkBarStyle={{background: 'white'}}  tabItemContainerStyle={{background: '#062b68'}}>

              <Tab label="Create a Diet" icon={<AddIngredient />} style={this.state.CreateIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={1}  >
                <CreateAnDiet Language={this.props.Language} getEffect={this.getEffect} height={this.state.height} weight={this.state.weight} DateOfBirth={this.state.DateOfBirth} Gender={this.state.Gender} removeRecipee={this.removeRecipee} addRecipee={this.addRecipee} LoggedInUser={this.props.LoggedInUser} Diet={this.state.Diet} />
              </Tab>

              <Tab label="Search for Diets" icon={<SearchIngredient />} style={this.state.SearchForIngredientTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={2} >
                <SearchForDiet Language={this.props.Language} addDietToFavourites={this.addDietToFavourites} addDietForComparison={this.addDietForComparison} height={this.state.height} weight={this.state.weight} DateOfBirth={this.state.DateOfBirth} Gender={this.state.Gender} LoggedInUser={this.props.LoggedInUser} />
              </Tab>

            </Tabs>
              </div>
          </div>
        );
    }
}
