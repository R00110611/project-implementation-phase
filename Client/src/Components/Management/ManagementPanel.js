
import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import IngredientManagementTabs from './IngredientManagement/IngredientManagementTabs';
import RecipeeManagementTabs from './RecipeeManagement/RecipeeManagementTabs';
import DietManagementTabs from './DietManagement/DietManagementTabs';
import DietingJourneyManagementTabs from './DietJourneyManagement/DietingJourneyManagementTabs'
import SocialNetworkTab from './SocialNetwork/SocialNetworkTab';

var injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

const styles = {
  activeStyle: {
    color: 'white',
    textTransform: 'none',
    outline: 'solid',
    outlineColor: 'white'

  },
  inactiveStyle: {
    color: '#a8adb5',
    textTransform: 'none',
  }
}

export default class ManagementPanel extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      IngredientManagementTabActive: true,
      RecipeeManagementTabActive: false,
      DietManagementTabActive: false,
      SocialNetworkTabActive: false,
      DietingJourneyTabActive: false
    };

    this.handleTabChange = this.handleTabChange.bind(this);
  }

  handleTabChange(e) {
    e.props.value === 1 ? this.setState({IngredientManagementTabActive: true}) : this.setState({IngredientManagementTabActive: false})
    e.props.value === 2 ? this.setState({RecipeeManagementTabActive: true}) : this.setState({RecipeeManagementTabActive: false})
    e.props.value === 3 ? this.setState({DietManagementTabActive: true}) : this.setState({DietManagementTabActive: false})
    e.props.value === 4 ? this.setState({DietingJourneyTabActive: true}) : this.setState({DietingJourneyTabActive: false})
    e.props.value === 5 ? this.setState({SocialNetworkTabActive: true}) : this.setState({SocialNetworkTabActive: false})
  }

  render() {
      return (
        <div>
          <Tabs inkBarStyle={{background: 'none'}} tabItemContainerStyle={{background: '#172684'}} >
           <Tab label={this.props.Language === 'English' ? 'Ingredient Management' : this.props.Language === 'Spanish' ? 'Gestión de Ingredientes' : 'Zarzadzanie Skladnikami'       } style={this.state.IngredientManagementTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={1}> </Tab>
           <Tab label={this.props.Language === 'English' ? 'Recipee Management' : this.props.Language === 'Spanish' ? 'Gestión de recetas' : 'Zarzadzanie Przepisami'       } style={this.state.RecipeeManagementTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={2}> </Tab>
           <Tab label={this.props.Language === 'English' ? 'Diet Management' : this.props.Language === 'Spanish' ? 'Gestión de la dieta' : 'Zarzadzanie Dieta'       } style={this.state.DietManagementTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={3}> </Tab>
           <Tab label={this.props.Language === 'English' ? 'Dieting Journey Management' : this.props.Language === 'Spanish' ? 'Diario de Dieta de Gestión' : 'Zarzadzanie Dziennikiem Diety'       } style={this.state.DietingJourneyTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={4}> </Tab>
           <Tab label={this.props.Language === 'English' ? 'Social Network' : this.props.Language === 'Spanish' ? 'Redes sociales' : 'Sieć społeczna'       } style={this.state.SocialNetworkTabActive === true ? styles.activeStyle : styles.inactiveStyle} onActive={this.handleTabChange} value={5}> </Tab>
          </Tabs>

          {this.state.IngredientManagementTabActive === true ? <IngredientManagementTabs Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} /> : <null />}
          {this.state.RecipeeManagementTabActive === true ? <RecipeeManagementTabs Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} /> : <null />}
          {this.state.DietManagementTabActive === true ? <DietManagementTabs Language={this.props.Language} LoggedInUser={this.props.LoggedInUser}/> : <null />}
          {this.state.DietingJourneyTabActive === true ? <DietingJourneyManagementTabs Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} /> : <null />}
          {this.state.SocialNetworkTabActive === true ? <SocialNetworkTab Language={this.props.Language} LoggedInUser={this.props.LoggedInUser} /> : <null />}
        </div>
      );
  }
}
